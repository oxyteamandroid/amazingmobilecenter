LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

iwds-res := ../iwds-ui-res-jar

src_dirs := src $(iwds-res)/src
res_dirs := res $(iwds-res)/res

LOCAL_RESOURCE_DIR := $(addprefix $(LOCAL_PATH)/, $(res_dirs))

LOCAL_AAPT_FLAGS := \
   --auto-add-overlay \
   --extra-packages com.ingenic.iwds.ui

LOCAL_SRC_FILES := $(call all-java-files-under, $(src_dirs)) $(call all-java-files-under, src)

LOCAL_PACKAGE_NAME := AmazingMobileCenter
LOCAL_CERTIFICATE := platform

LOCAL_DEX_PREOPT := false

LOCAL_JAVA_LIBRARIES := telephony-common


LOCAL_STATIC_JAVA_LIBRARIES := iwds-ui-with-res-jar vcard android-supports-v4 iwds-jar

LOCAL_JNI_SHARED_LIBRARIES := libbt_volume_control_jni

LOCAL_REQUIRED_MODULES := libbt_volume_control_jni


LOCAL_AAPT_FLAGS += -c zz_ZZ

LOCAL_PROGUARD_ENABLED := disabled
include $(BUILD_PACKAGE)

include $(CLEAR_VARS)
LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES +=iwds-jar:libs/iwds-service-ui-jar.jar
LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES +=iwds-ui-with-res-jar:libs/iwds-ui-with-res-jar.jar
LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES +=vcard:libs/android-vcard.jar
LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES +=android-supports-v4:libs/android-support-v4.jar

include $(BUILD_MULTI_PREBUILT)
# Use the folloing include to make our test apk.
include $(call all-makefiles-under,$(LOCAL_PATH))
