package com.ingenic.mobilecenter.call;

import java.util.ArrayList;
import java.util.List;

import com.ingenic.iwds.app.AmazingDialog;
import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.iwds.datatransactor.elf.PhoneState;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.mobilecenter.MobileCenterApplication;
import com.ingenic.mobilecenter.R;
import com.ingenic.mobilecenter.TimeChangeListener;
import com.ingenic.mobilecenter.call.IncomingRemindService.AvailableChangedListener;
import com.ingenic.mobilecenter.call.IncomingRemindService.IncomingRemindStateListener;
import com.ingenic.mobilecenter.call.WatchPhoneService.CallAudioChangedListener;
import com.ingenic.mobilecenter.call.WatchPhoneService.CallStateChangedListener;
import com.ingenic.mobilecenter.call.WatchPhoneService.ConnectionStateChangedListener;
import com.ingenic.mobilecenter.utils.NumberUtils;
import com.ingenic.mobilecenter.utils.Utils;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.bluetooth.BluetoothHeadsetClient;
import android.bluetooth.BluetoothHeadsetClientCall;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.PowerManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

public class TelephoneActivity extends RightScrollActivity implements CallStateChangedListener, CallAudioChangedListener, ConnectionStateChangedListener,
        AvailableChangedListener, IncomingRemindStateListener, TimeChangeListener {

    /**
     * 方屏时间控件
     */
    private TextView mTimeTextView;

    /**
     * 蓝牙音频通话服务代理对象
     */
    public WatchPhoneService.WatchPhoneBinder mWatchPhoneBinder;

    /**
     * 来电提醒服务代理对象
     */
    public IncomingRemindService.IncomingRemindBinder mIncomingRemindBinder;

    /**
     * 跳转Activity的Action
     */
    private String action;

    /**
     * 通话人名字
     */
    public String name;

    /**
     * 通话人号码
     */
    public String number;

    /**
     * 正在显示的Fragment
     */
    private Fragment fragment;

    /**
     * 保存所有通话ID
     */
    private List<Integer> callIds = new ArrayList<Integer>();

    /**
     * 亮屏锁
     */
    private PowerManager.WakeLock mWakeLock;
    private PowerManager.WakeLock mWakeLock_;
    private PowerManager mManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.sIsConversation = true;
        if (!Utils.isRound()) {
            setContentView(R.layout.activity_telephone);
            ((MobileCenterApplication) getApplication()).registerListener(this);
        } else {
            setContentView(R.layout.activity_telephone_round);
        }
        
        
        action = getIntent().getAction();
        name = getIntent().getStringExtra(Utils.TELEPHONE_NAME);
        number = getIntent().getStringExtra(Utils.TELEPHONE_NUMBER);
        if (TextUtils.isEmpty(number)) {
            finish();
            return;
        }

        mManager = (PowerManager) getSystemService(POWER_SERVICE);

        initViews();

        Intent it = new Intent(this, WatchPhoneService.class);
        bindService(it, mWatchPhoneConn, BIND_AUTO_CREATE);
        IwdsLog.i(this, "bindService WatchPhoneService");
        
        Intent intent = new Intent(this, IncomingRemindService.class);
        bindService(intent, mIncomingRemindConn, BIND_AUTO_CREATE);
        IwdsLog.i(this, "bindService IncomingRemindService");
    }

    @Override
    protected void onDestroy() {
        onStopRing();

        unKeepScreen();
        unAcquireWakelock();

        if (mManager != null)
            mManager = null;

        if (mWatchPhoneBinder != null) {
            mWatchPhoneBinder.unRegisterCallStateChangedListener(TelephoneActivity.this);
            mWatchPhoneBinder.unRegisterCallAudioChangedListener(TelephoneActivity.this);
            mWatchPhoneBinder.unRegisterConnectionStateChangedListener(TelephoneActivity.this);
            mWatchPhoneBinder = null;
        }
        unbindService(mWatchPhoneConn);
        mWatchPhoneConn = null;
        if (mIncomingRemindBinder != null) {
            mIncomingRemindBinder.unRegisterAvailableChangedListener(TelephoneActivity.this);
            mIncomingRemindBinder.unRegisterPhoneStateListener(TelephoneActivity.this);
            mIncomingRemindBinder = null;
        }
        unbindService(mIncomingRemindConn);
        mIncomingRemindConn = null;

        Utils.sIsConversation = false;

        if (fragment != null)
            fragment = null;

        IwdsLog.e(this, "====onDestroy");
        super.onDestroy();
    }

    /**
     * 初始化控件
     */
    private void initViews() {
        if (!Utils.isRound()) {
            mTimeTextView = (TextView) findViewById(R.id.telephone_time);
            mTimeTextView.setText(Utils.getTime());
        }
    }

    /**
     * 初始化显示Fragment
     * 
     * @param intent
     */
    public void initFragment(Intent intent) {
        Utils.sFragmentName = intent.getStringExtra(Utils.TELEPHONE_FRAGMENT);
        name = intent.getStringExtra(Utils.TELEPHONE_NAME);
        number = intent.getStringExtra(Utils.TELEPHONE_NUMBER);
        name = TextUtils.isEmpty(name) ? NumberUtils.queryName(this, number) : name;
        name = TextUtils.isEmpty(name) ? number : name;

        String tag = intent.getStringExtra(Utils.TELEPHONE_TAG);
        boolean canBack = intent.getBooleanExtra(Utils.TELEPHONE_CAN_BACK, false);
        IwdsLog.i(this, "incoming name:" + name + ",number:" + number);

        if (!TextUtils.isEmpty(Utils.sFragmentName)) {
            fragment = Fragment.instantiate(this, Utils.sFragmentName);
            if (TextUtils.isEmpty(tag))
                tag = "telephone";
            showFragment(fragment, canBack, tag);
        }
    }

    /**
     * 显示Fragment
     * 
     * @param fragment
     *            显示的Fragment
     * @param canBack
     *            是否可以返回
     * @param tag
     */
    private void showFragment(Fragment fragment, boolean canBack, String tag) {

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.telephone_fragment, fragment, tag);
        if (canBack) {
            ft.addToBackStack(tag);
        }
        ft.commitAllowingStateLoss();
    }

    /**
     * 来电提醒服务连接对象
     */
    private ServiceConnection mWatchPhoneConn = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            IwdsLog.i(this, "onServiceDisconnected WatchPhoneService");
            if (mWatchPhoneBinder != null) {
                mWatchPhoneBinder.unRegisterCallStateChangedListener(TelephoneActivity.this);
                mWatchPhoneBinder.unRegisterCallAudioChangedListener(TelephoneActivity.this);
                mWatchPhoneBinder.unRegisterConnectionStateChangedListener(TelephoneActivity.this);
                mWatchPhoneBinder = null;
            }
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder binder) {
            IwdsLog.i(this, "onServiceConnected WatchPhoneService");
            mWatchPhoneBinder = (WatchPhoneService.WatchPhoneBinder) binder;
            mWatchPhoneBinder.registerCallStateChangedListener(TelephoneActivity.this);
            mWatchPhoneBinder.registerCallAudioChangedListener(TelephoneActivity.this);
            mWatchPhoneBinder.registerConnectionStateChangedListener(TelephoneActivity.this);

            initFragment(getIntent());
            if (Utils.ACTION_OUT_CALL.equals(action)) {
                if (dial(number)) {
                    keepScreen();
                    mHandler.sendEmptyMessageDelayed(MAG_0, CALL_DELAYED_TIME);
                } else {
                    showDialFailDialog();
                }
            } else if (Utils.ACTION_INCOMING_CALL.equals(action)) {
                keepScreen();
                onStartRing();
            }
        }
    };

    /**
     * 呼叫超时时间
     */
    public static final long CALL_DELAYED_TIME = 6000;
    public static final int MAG_0 = 0;
    public static final int MAG_1 = 1;

    public Handler mHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
            case MAG_0:
                if (fragment instanceof CallFargment) {
                    finish();
                }
                break;
            case MAG_1:
                Utils.sCallState = BluetoothHeadsetClientCall.CALL_STATE_TERMINATED;
                if (!isDestroyed()) {
                    if (mWatchPhoneBinder != null)
                        mWatchPhoneBinder.stopTimer();
                    finish();
                }
                break;
            default:
                break;
            }
            return false;
        }
    });

    /**
     * 拨号失败Dialog
     */
    private AmazingDialog dialFailDialog = null;

    /**
     * 显示拨号失败Dialog
     */
    private void showDialFailDialog() {
        if (dialFailDialog == null) {
            dialFailDialog = new AmazingDialog(this).setRightScrollEnable(false);
            dialFailDialog.setCancelable(false);
            dialFailDialog.setContent(R.string.dial_fail);
            dialFailDialog.setPositiveButton(R.drawable.ic_cancel, new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dialFailDialog.dismiss();
                    dialFailDialog = null;
                    finish();
                }
            });
        }
        dialFailDialog.show();
    }

    /**
     * 来电提醒服务连接对象
     */
    private ServiceConnection mIncomingRemindConn = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            IwdsLog.i(this, "onServiceDisconnected IncomingRemindService");
            if (mIncomingRemindBinder != null) {
                mIncomingRemindBinder.unRegisterAvailableChangedListener(TelephoneActivity.this);
                mIncomingRemindBinder.unRegisterPhoneStateListener(TelephoneActivity.this);
                mIncomingRemindBinder = null;
            }
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder binder) {
            IwdsLog.i(this, "onServiceConnected IncomingRemindService");
            mIncomingRemindBinder = (IncomingRemindService.IncomingRemindBinder) binder;
            mIncomingRemindBinder.registerAvailableChangedListener(TelephoneActivity.this);
            mIncomingRemindBinder.registerPhoneStateListener(TelephoneActivity.this);
            if (Utils.ACTION_INCOMING_REMIND.equals(action)) {
                initFragment(getIntent());
                keepScreen();
                onStartRing();
            }
        }
    };

    /**
     * 通话状态改变
     */
    @Override
    public void onCallStateChanged(BluetoothHeadsetClientCall call) {
        IwdsLog.i(this, "onCallStateChanged:" + call.getState());
        int state = call.getState();
        switch (state) {
        case BluetoothHeadsetClientCall.CALL_STATE_ACTIVE:// 通话
            if (Utils.mCall == null) {// 第一个通话 添加一个计时
                mWatchPhoneBinder.addTime();
                callIds.add(call.getId());

                unKeepScreen();
                acquireWakelock();
                switchFragment(PhoningFragment.class.getName(), "phoning", name, number);
            } else {
                if (!callIds.contains(call.getId())) {
                    mWatchPhoneBinder.addTime();
                    callIds.add(call.getId());

                    mWatchPhoneBinder.setIsTime(true);
                    if (fragment instanceof PhoningFragment) {
                        mWatchPhoneBinder.switchTime(callIds.indexOf(call.getId()));
                        PhoningFragment phoningFragment = (PhoningFragment) fragment;
                        phoningFragment.updateCall(call);
                    } else {
                        unKeepScreen();
                        acquireWakelock();
                        mWatchPhoneBinder.switchTime(callIds.indexOf(call.getId()));
                        switchFragment(PhoningFragment.class.getName(), "phoning", null, call.getNumber());
                    }
                } else {
                    mWatchPhoneBinder.switchTime(callIds.indexOf(call.getId()));
                    if (fragment instanceof PhoningFragment) {
                        PhoningFragment phoningFragment = (PhoningFragment) fragment;
                        phoningFragment.updateCall(call);
                    } else {
                        switchFragment(PhoningFragment.class.getName(), "phoning", null, call.getNumber());
                    }
                }
            }
            Utils.mCall = call;
            break;
        case BluetoothHeadsetClientCall.CALL_STATE_HELD:// 保留
            break;
        case BluetoothHeadsetClientCall.CALL_STATE_DIALING:// 拨号
            if (mHandler.hasMessages(MAG_0)) {
                mHandler.removeMessages(MAG_0);
            }
            break;
        case BluetoothHeadsetClientCall.CALL_STATE_ALERTING:// 呼叫
            switch (Utils.sCallState) {
            case BluetoothHeadsetClientCall.CALL_STATE_DIALING:
                if (fragment instanceof CallFargment) {
                    CallFargment callFargment = (CallFargment) fragment;
                    callFargment.delayHangUp();
                }
                break;
            default:
                break;
            }
            break;
        case BluetoothHeadsetClientCall.CALL_STATE_WAITING:// 等待
            unAcquireWakelock();
            keepScreen();
            switchFragment(IncomingFragment.class.getName(), "waiting", call.getNumber(), call.getNumber());
            break;
        case BluetoothHeadsetClientCall.CALL_STATE_TERMINATED:// 终止
            IwdsLog.d(this, "Call size:" + mWatchPhoneBinder.getCurrentCalls().size());
            if (mHandler.hasMessages(MAG_1)) {
                mHandler.removeMessages(MAG_1);
            }
            if (isHangUP(call.getId())) {
                mWatchPhoneBinder.stopTimer();
                finish();
            }
            break;
        default:
            break;
        }
        Utils.sCallState = state;
    }

    /**
     * 获取id索引
     * 
     * @param id
     * @return
     */
    public int getIdIndex(int id) {
        return callIds.indexOf(id);
    }

    /**
     * 切换Fragment
     * 
     * @param fragmentName
     * @param tag
     * @param name
     * @param number
     */
    private void switchFragment(String fragmentName, String tag, String name, String number) {
        getIntent().addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getIntent().putExtra(Utils.TELEPHONE_FRAGMENT, fragmentName);
        getIntent().putExtra(Utils.TELEPHONE_TAG, tag);
        getIntent().putExtra(Utils.TELEPHONE_NAME, name);
        getIntent().putExtra(Utils.TELEPHONE_NUMBER, number);
        initFragment(getIntent());
    }

    /**
     * 查询通话索引
     * 
     * @param call
     * @return
     */
    public int indexOf(BluetoothHeadsetClientCall call) {
        if (mWatchPhoneBinder == null)
            return -1;
        List<BluetoothHeadsetClientCall> list = mWatchPhoneBinder.getCurrentCalls();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getId() == call.getId()) {
                return i;
            }
        }
        return -1;
    }

    /**
     * 是否挂断所有通话
     * 
     * @param id
     * @return
     */
    private boolean isHangUP(int id) {
        if (mWatchPhoneBinder == null)
            return true;
        List<BluetoothHeadsetClientCall> calls = mWatchPhoneBinder.getCurrentCalls();
        for (int i = 0; i < calls.size(); i++) {
            if (calls.get(i).getId() != id) {
                return false;
            }
        }
        return true;
    }

    /**
     * 通话音频改变
     */
    @Override
    public void onCallAudioChanged(int state) {
        IwdsLog.i(this, "onCallAudioChanged:" + state);
        if (fragment instanceof PhoningFragment) {
            PhoningFragment phoningFragment = (PhoningFragment) fragment;
            phoningFragment.onCallAudioChanged(state);
        }
    }

    /**
     * 蓝牙音频连接改变
     */
    @Override
    public void onConnectionStateChanged(int state) {
        IwdsLog.i(this, "onConnectionStateChanged:" + state);
        if (fragment instanceof CallFargment) {
            if (state == BluetoothHeadsetClient.STATE_DISCONNECTED) {
                showDialDisconnectDialog();
            } else if (state == BluetoothHeadsetClient.STATE_CONNECTED) {
                dialDisconnectDialog.dismiss();
                dialDisconnectDialog = null;
                showDialConnectDialog();
            }
            return;
        }
        if (state == BluetoothHeadsetClient.STATE_DISCONNECTED) {
            finish();
        }
    }

    /**
     * 连接断开Dialog
     */
    private AmazingDialog dialDisconnectDialog = null;

    /**
     * 显示拨号连接断开Dialog
     */
    private void showDialDisconnectDialog() {
        if (dialDisconnectDialog == null) {
            dialDisconnectDialog = new AmazingDialog(this).setRightScrollEnable(false);
            dialDisconnectDialog.setCancelable(false);
            dialDisconnectDialog.setContent(R.string.dial_fail);
            dialDisconnectDialog.setPositiveButton(R.drawable.ic_cancel, new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dialDisconnectDialog.dismiss();
                    dialDisconnectDialog = null;
                    finish();
                }
            });
//            dialDisconnectDialog.setNegativeButton(R.drawable.ic_answer, new View.OnClickListener() {
//
//                @Override
//                public void onClick(View v) {
//                }
//            });
        }
        dialDisconnectDialog.show();
    }

    /**
     * 拨号连接Dialog
     */
    private AmazingDialog dialConnectDialog = null;

    /**
     * 显示拨号连接Dialog
     */
    private void showDialConnectDialog() {
        if (dialConnectDialog == null) {
            dialConnectDialog = new AmazingDialog(this).setRightScrollEnable(false);
            dialConnectDialog.setCancelable(false);
            dialConnectDialog.setContent(R.string.dial_fail);
            dialConnectDialog.setPositiveButton(R.drawable.ic_cancel, new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    dialConnectDialog.dismiss();
                    dialConnectDialog = null;
                    finish();
                }
            });
            dialConnectDialog.setNegativeButton(R.drawable.ic_answer, new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    redial();
                    dialConnectDialog.dismiss();
                    dialConnectDialog = null;
                }
            });
        }
        dialConnectDialog.show();
    }

    /**
     * 通道可用改变
     */
    @Override
    public void onAvailableChanged(boolean available) {
        IwdsLog.i(this, "onAvailableChanged:" + available);
        if (available && fragment instanceof IncomingRemindFragment) {
            finish();
        }
    }

    /**
     * 来电提醒状态改变监听
     */
    @Override
    public void onIncomingRemindStateChange(int state) {
        IwdsLog.i(this, "onIncomingRemindStateChange:" + state);
        switch (state) {
        case PhoneState.STATE_SYNC_PHONE_DISABLED:
        case PhoneState.STATE_IDLE:
        case PhoneState.STATE_OFFHOOK:
            finish();
            break;
        default:
            break;
        }
    }

    /**
     * 拨号
     * 
     * @param number
     * @return
     */
    public boolean dial(String number) {
        if (mWatchPhoneBinder == null)
            return false;
        return mWatchPhoneBinder.dial(number);
    }

    /**
     * 重拨
     * 
     * @param number
     * @return
     */
    public boolean redial() {
        if (mWatchPhoneBinder == null)
            return false;
        return mWatchPhoneBinder.redial();
    }

    /**
     * 取消呼叫,挂断通话
     * 
     * @param index
     * @return
     */
    public boolean terminateCall(int index) {
        if (mWatchPhoneBinder == null)
            return false;
        return mWatchPhoneBinder.terminateCall(index);
    }

    /**
     * 来电提醒挂断
     */
    public boolean hangup() {
        if (mIncomingRemindBinder == null)
            return false;
        mIncomingRemindBinder.hangup();
        onStopRing();
        return true;
    }

    /**
     * 接听来电
     */
    public boolean acceptCall(int flag) {
        if (mWatchPhoneBinder == null)
            return false;
        if (mWatchPhoneBinder.acceptCall(flag)) {
            onStopRing();
            return true;
        }
        return false;
    }

    /**
     * 拒绝来电
     */
    public boolean rejectCall() {
        if (mWatchPhoneBinder == null)
            return false;
        if (mWatchPhoneBinder.rejectCall()) {
            onStopRing();
            return true;
        }
        return false;
    }

    /**
     * 保留通话
     * 
     * @return
     */
    public boolean holdCall() {
        if (mWatchPhoneBinder == null)
            return false;
        if (mWatchPhoneBinder.holdCall()) {
            onStopRing();
            return true;
        }
        return false;
    }

    /**
     * 发送命令
     * 
     * @return
     */
    public boolean sendDTMF(byte code) {
        if (mWatchPhoneBinder == null)
            return false;
        if (mWatchPhoneBinder.sendDTMF(code)) {
            return true;
        }
        return false;
    }

    /**
     * 连接蓝牙音频
     * 
     * @return
     */
    public boolean connectAudio() {
        if (mWatchPhoneBinder == null)
            return false;
        if (mWatchPhoneBinder.connectAudio()) {
            return true;
        }
        return false;
    }

    /**
     * 断开蓝牙音频
     * 
     * @return
     */
    public boolean disconnectAudio() {
        if (mWatchPhoneBinder == null)
            return false;
        if (mWatchPhoneBinder.disconnectAudio()) {
            return true;
        }
        return false;
    }

    /**
     * 断开蓝牙音频
     * 
     * @return
     */
    public int getAudioState() {
        if (mWatchPhoneBinder == null)
            return BluetoothHeadsetClient.STATE_AUDIO_DISCONNECTED;
        return mWatchPhoneBinder.getAudioState();
    }

    /**
     * 短信回复挂断
     */
    public boolean receiveSMS(String sms, String number) {
        if (mIncomingRemindBinder == null)
            return false;
        mIncomingRemindBinder.receiveSMS(sms, number);
        return true;
    }

    /**
     * 停止响铃
     */
    public void onStopRing() {
        if (mIncomingRemindBinder == null)
            return;
        mIncomingRemindBinder.onStopRing();
    }

    /**
     * 响铃
     */
    public void onStartRing() {
        if (mIncomingRemindBinder == null)
            return;
        mIncomingRemindBinder.onRing();
    }

    /**
     * 持“CPU运行”锁
     */
    private void acquireWakelock() {
        if (mWakeLock == null) {
            mWakeLock = mManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "turn_screen");
        }
        if (!mWakeLock.isHeld()) {
            mWakeLock.acquire();
        }
    }

    /**
     * 释放“CPU运行”锁
     */
    private void unAcquireWakelock() {
        if (mWakeLock != null) {
            mWakeLock.release();
            mWakeLock = null;
        }
    }

    /**
     * 持“点亮并常亮”锁
     */
    @SuppressWarnings("deprecation")
    private void keepScreen() {
        if (mWakeLock_ == null) {
            mWakeLock_ = mManager.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "keep_screen");
        }
        mWakeLock_.acquire();
    }

    /**
     * 释放“点亮并常亮”锁
     */
    private void unKeepScreen() {
        if (mWakeLock_ != null) {
            mWakeLock_.release();
            mWakeLock_ = null;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Fragment fragment = getFragmentManager().findFragmentByTag("reply_sms");
            if (fragment != null && fragment.isResumed()) {
                return super.onKeyDown(keyCode, event);
            }
            fragment = getFragmentManager().findFragmentByTag("input");
            if (fragment != null && fragment.isResumed()) {
                return super.onKeyDown(keyCode, event);
            }
            fragment = getFragmentManager().findFragmentByTag("phoning");
            if (fragment != null && fragment.isResumed()) {
                Intent i = new Intent(Intent.ACTION_MAIN);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.addCategory(Intent.CATEGORY_HOME);
                startActivity(i);
                return true;
            }
            onStopRing();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onTimeChange() {
        if (mTimeTextView != null)
            mTimeTextView.setText(Utils.getTime());
    }

}
