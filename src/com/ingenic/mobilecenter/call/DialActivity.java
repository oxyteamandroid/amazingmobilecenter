package com.ingenic.mobilecenter.call;

import com.ingenic.iwds.app.AmazingDialog;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.iwds.widget.RightScrollView;
import com.ingenic.iwds.widget.RightScrollView.OnRightScrollListener;
import com.ingenic.mobilecenter.MobileCenterApplication;
import com.ingenic.mobilecenter.R;
import com.ingenic.mobilecenter.TimeChangeListener;
import com.ingenic.mobilecenter.utils.Utils;
import com.ingenic.mobilecenter.view.InputView;
import com.ingenic.mobilecenter.view.InputView.OnClickListener;
import com.ingenic.mobilecenter.view.InputViewRound;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class DialActivity extends Activity implements OnRightScrollListener, View.OnLongClickListener, OnClickListener,
        com.ingenic.mobilecenter.view.InputViewRound.OnClickListener, TimeChangeListener {

    /**
     * 右滑控件
     */
    private RightScrollView mRightScrollView;

    /**
     * 时间控件
     */
    private TextView mTimeTextView;

    /**
     * 号码输入控件
     */
    private TextView mNumberInputTextView;

    /**
     * 方屏自定义拨号控件
     */
    private InputView mInputView;

    /**
     * 圆屏自定义拨号控件
     */
    private InputViewRound mInputViewRound;

    /**
     * 圆屏0,+ 复用控件
     */
    private Button mZeroPlusButton;

    /**
     * 圆屏*,# 复用控件
     */
    private Button mAsteriskWellButton;

    /**
     * 清楚控件
     */
    private Button mDeleteButton;

    /**
     * 呼叫控件
     */
    private ImageButton mCallButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRightScrollView = new RightScrollView(this);
        mRightScrollView.setOnRightScrollListener(this);
        if (Utils.isRound()) {
            View.inflate(this, R.layout.activity_dial_round, mRightScrollView);
        } else {
            View.inflate(this, R.layout.activity_dial, mRightScrollView);
        }
        setContentView(mRightScrollView);
        initViews(mRightScrollView);
        ((MobileCenterApplication) getApplication()).registerListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * 右滑结束
     */
    @Override
    public void onRightScroll() {
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
    }

    @Override
    public void onClick(char in) {
        inputPadChar(in);
    }

    @Override
    public boolean onLongClick(View view) {
        switch (view.getId()) {
        case R.id.dial_del:
            inputPadChar('c');
            break;
        case R.id.dial_zero_plus:
            inputPadChar('+');
            break;
        case R.id.dial_asterisk_well:
            inputPadChar('*');
            break;
        default:
            break;
        }
        return true;
    }

    /**
     * 初始化控件
     */
    private void initViews(View view) {
        mNumberInputTextView = (TextView) view.findViewById(R.id.dial_number_in);

        mDeleteButton = (Button) view.findViewById(R.id.dial_del);
        mDeleteButton.setTag(R.id.numbers_key, '-');

        if (Utils.isRound()) {
            mInputViewRound = (InputViewRound) view.findViewById(R.id.dial_input_view);
            mInputViewRound.setOnClickListener(this);

            mZeroPlusButton = (Button) view.findViewById(R.id.dial_zero_plus);
            mZeroPlusButton.setTag(R.id.numbers_key, '0');
            mZeroPlusButton.setText("+" + " 0");
            mZeroPlusButton.setOnClickListener(mInputViewRound);
            mZeroPlusButton.setOnLongClickListener(this);

            mAsteriskWellButton = (Button) view.findViewById(R.id.dial_asterisk_well);
            mAsteriskWellButton.setTag(R.id.numbers_key, '#');
            mAsteriskWellButton.setText("#" + " *");
            mAsteriskWellButton.setOnClickListener(mInputViewRound);
            mAsteriskWellButton.setOnLongClickListener(this);

            mDeleteButton.setOnClickListener(mInputViewRound);
        } else {
            mTimeTextView = (TextView) view.findViewById(R.id.dial_time);
            mTimeTextView.setText(Utils.getTime());

            mInputView = (InputView) view.findViewById(R.id.dial_input_view);
            mInputView.setOnClickListener(this);

            mDeleteButton.setOnClickListener(mInputView);
        }
        mDeleteButton.setOnLongClickListener(this);
        mCallButton = (ImageButton) view.findViewById(R.id.dial_call);
        mCallButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String number = mNumberInputTextView.getText().toString().trim();
                IwdsLog.e(this, "isBtConnected:" + Utils.isBtConnected(DialActivity.this));
                if (!TextUtils.isEmpty(number)) {
                    if (Utils.isBtConnected(DialActivity.this)) {
                        Intent it = new Intent(DialActivity.this, TelephoneActivity.class); // Utils.ACTION_OUT_CALL);
                        it.setAction(Utils.ACTION_OUT_CALL);
                        it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        it.putExtra(Utils.TELEPHONE_FRAGMENT, CallFargment.class.getName());
                        it.putExtra(Utils.TELEPHONE_TAG, "call");
                        it.putExtra(Utils.TELEPHONE_NUMBER, number);
                        startActivity(it);
                    } else {
                        showBtDisconnectedDialog();
                    }
                }
            }
        });
    }

    AmazingDialog disconnected = null;

    private void showBtDisconnectedDialog() {

        if (disconnected == null) {
            disconnected = new AmazingDialog(this).setRightScrollEnable(false);
            disconnected.setCancelable(false);
            disconnected.setContent(R.string.bt_disconnect);
            disconnected.setPositiveButton(R.drawable.ic_cancel, new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    disconnected.dismiss();
                    disconnected = null;
                }
            });
        }
        disconnected.show();
    }

    /**
     * 拨号盘输入
     * 
     * @param in
     */
    private void inputPadChar(char in) {
        String next = "";
        String text = (String) mNumberInputTextView.getText();
        if (in == '-') {
            if (text != null && text.length() > 0) {
                next = text.substring(0, text.length() - 1);
            }
        } else if (in == 'c') {
            next = "";
        } else if (text == null || text.length() == 0) {
            next += in;
        } else {
            next = text + in;
        }
        mNumberInputTextView.setText(next);
        mNumberInputTextView.setEnabled(!TextUtils.isEmpty(mNumberInputTextView.getText().toString().trim()));
    }

    /**
     * 时间改变回调(单位:分钟)
     */
    @Override
    public void onTimeChange() {
        if (mTimeTextView != null)
            mTimeTextView.setText(Utils.getTime());
    }
}
