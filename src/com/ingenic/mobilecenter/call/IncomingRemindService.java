/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  YuanBanglin(Ahlin) <banglin.yuan@ingenic.com>
 *
 *  Elf/IDWS Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.mobilecenter.call;

import java.util.ArrayList;
import java.util.List;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.elf.PhoneState;
import com.ingenic.iwds.datatransactor.elf.PhoneStateTransactionModel;
import com.ingenic.iwds.datatransactor.elf.PhoneStateTransactionModel.PhoneStateCallback;
import com.ingenic.iwds.utils.IwdsAssert;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.mobilecenter.MobileCenterApplication;
import com.ingenic.mobilecenter.Ringer;
import com.ingenic.mobilecenter.utils.Utils;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;
import android.text.TextUtils;

public class IncomingRemindService extends Service implements PhoneStateCallback {

    /**
     * 通道是否可用
     */
    private boolean isAvailable = false;

    /**
     * 通道是否连接
     */
    private boolean isConnected = false;

    /**
     * 来电提醒状态监听对象
     */
    private List<IncomingRemindStateListener> phoneStateListeners = new ArrayList<IncomingRemindStateListener>();

    /**
     * 通道可用改变监听对象
     */
    private List<AvailableChangedListener> availableChangedListeners = new ArrayList<AvailableChangedListener>();

    /**
     * 来电提醒Model
     */
    private PhoneStateTransactionModel mModel;
    
    /**
     * 铃声操作
     */
    private Ringer mRinger;

    @Override
    public void onCreate() {
        super.onCreate();
        IwdsLog.i(this, "onCreate IncomingRemindService");
        mRinger = Ringer.init(this);
        if (mModel == null) {
            mModel = ((MobileCenterApplication) getApplication()).getPhoneModel(this);
        }
        mModel.start();

        Notification.Builder builder = new Notification.Builder(this);
        Notification notification = builder.build();
        startForeground(9999, notification);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        IwdsLog.i(this, "onDestroy IncomingRemindService");
        if (mModel != null) {
            mModel.stop();
        }
        sendBroadcast(new Intent(Utils.RESTART_SERVICE));
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return IncomingRemindBinder.getInstance(this);
    }

    /**
     * 设置通道是否可用并通知界面
     * 
     * @param available
     */
    private void setAvailable(boolean available) {
        if (isAvailable != available)
            isAvailable = available;

        for (AvailableChangedListener listener : availableChangedListeners) {
            listener.onAvailableChanged(available);
        }

        SharedPreferences sp = getSharedPreferences(Utils.PREFERENCE_BOND, MODE_PRIVATE);
        sp.edit().putBoolean(Utils.AVAILABLE_SYNC, available).commit();
    }

    /**
     * 注册来电提醒状态改变监听
     * 
     * @param l
     */
    private void registerPhoneStateListener(IncomingRemindStateListener l) {
        if (phoneStateListeners.contains(l)) {
            IwdsLog.w(this, "This listener is already registered!");
            return;
        }
        synchronized (phoneStateListeners) {
            phoneStateListeners.add(l);
        }
    }

    /**
     * 注销来电提醒状态改变监听
     * 
     * @param l
     */
    private void unRegisterPhoneStateListener(IncomingRemindStateListener l) {
        if (phoneStateListeners.contains(l)) {
            synchronized (phoneStateListeners) {
                phoneStateListeners.remove(l);
            }
        }
    }

    /**
     * 通道可用改变监听
     * 
     * @author Ahlin
     */
    public interface AvailableChangedListener {
        void onAvailableChanged(boolean available);
    }

    /**
     * 来电提醒状态改变监听
     * 
     * @author Ahlin
     */
    public interface IncomingRemindStateListener {
        void onIncomingRemindStateChange(int state);
    }

    /**
     * 注册通道可用改变监听
     * 
     * @param l
     */
    private void registerAvailableChangedListener(AvailableChangedListener l) {
        if (availableChangedListeners.contains(l)) {
            IwdsLog.w(this, "This listener is already registered!");
            return;
        }
        synchronized (availableChangedListeners) {
            availableChangedListeners.add(l);
        }
    }

    /**
     * 注销通道可用改变监听
     * 
     * @param l
     */
    private void unRegisterAvailableChangedListener(AvailableChangedListener l) {
        if (availableChangedListeners.contains(l)) {
            synchronized (availableChangedListeners) {
                availableChangedListeners.remove(l);
            }
        }
    }

    /**
     * 发送挂断电话命令
     */
    private void hangup() {
        PhoneState state = new PhoneState();
        state.state = PhoneState.STATE_IDLE;
        mModel.send(state);
    }

    /**
     * 发送发送短信命令
     * 
     * @param sms
     */
    private void receiveSMS(String sms, String number) {
        IwdsAssert.dieIf(this, TextUtils.isEmpty(number), "number is null");
        IwdsAssert.dieIf(this, TextUtils.isEmpty(sms), "SMS is null");
        PhoneState state = new PhoneState();
        state.state = PhoneState.STATE_SEND_SMS;
        state.number = number;
        state.name = sms;
        mModel.send(state);
    }

    /**
     * 响铃
     */
    private void onRing() {
        if (!mRinger.isRinging()) {
            mRinger.ring();
        }
    }

    /**
     * 停止响铃
     */
    private void onStopRing() {
        if (mRinger.isRinging()) {
            mRinger.stopRing();
        }
    }

    /** 命令接受 */
    @Override
    public void onObjectArrived(PhoneState state) {
        if (Utils.isBtConnected(this))
            return;

        IwdsLog.i(this, "Receive phone state:" + state);
        int callState = state.state;
        switch (state.state) {
        case PhoneState.STATE_SEND_SMS_LIST:
            SharedPreferences sp = getSharedPreferences(Utils.PREFERENCE_BOND, MODE_PRIVATE);
            sp.edit().putString(state.number, state.name).commit();
            break;

        case PhoneState.STATE_SYNC_PHONE_DISABLED:
            for (IncomingRemindStateListener listener : phoneStateListeners) {
                listener.onIncomingRemindStateChange(callState);
            }
            break;

        case PhoneState.STATE_IDLE:
            for (IncomingRemindStateListener listener : phoneStateListeners) {
                listener.onIncomingRemindStateChange(callState);
            }
            break;

        case PhoneState.STATE_INCOMING:
            // if (callState != PhoneState.STATE_IDLE) {
            // IwdsLog.w(this, "PhoneState is not IDLE!Ignore this phone call");
            // return;
            // }

            Intent it = new Intent(IncomingRemindService.this,TelephoneActivity.class);
            it.setAction(Utils.ACTION_INCOMING_CALL);
            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            it.putExtra(Utils.TELEPHONE_FRAGMENT, IncomingRemindFragment.class.getName());
            it.putExtra(Utils.TELEPHONE_TAG, "incoming_remind");
            it.putExtra(Utils.TELEPHONE_NUMBER, state.number);
            it.putExtra(Utils.TELEPHONE_NAME, state.name);
            startActivity(it);
            break;

        case PhoneState.STATE_OFFHOOK:
            if (callState != PhoneState.STATE_INCOMING)
                return;
            for (IncomingRemindStateListener listener : phoneStateListeners) {
                listener.onIncomingRemindStateChange(callState);
            }
            break;

        default:
            break;
        }
    }

    /** 通道连接改变 */
    @Override
    public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {
        if (!isConnected){
            setAvailable(false);
        }else{
            
        }
        this.isConnected = isConnected;
    }

    /** 通道可用改变 */
    @Override
    public void onChannelAvailable(boolean isAvailable) {
        setAvailable(isAvailable);
    }

    @Override
    public void onSendResult(DataTransactResult result) {
    }

    @Override
    public void onRequest() {
    }

    @Override
    public void onRequestFailed() {
    }

    /**
     * 服务代理
     * 
     * @author Ahlin
     */
    public static class IncomingRemindBinder extends Binder {
        /**
         * 服务对象
         */
        private IncomingRemindService mService;
        /**
         * 代理对象
         */
        private static IncomingRemindBinder mBinder;

        private IncomingRemindBinder(IncomingRemindService service) {
            mService = service;
        }

        public static IncomingRemindBinder getInstance(IncomingRemindService service) {
            if (mBinder == null) {
                synchronized (IncomingRemindBinder.class) {
                    if (mBinder == null)
                        mBinder = new IncomingRemindBinder(service);
                }
            }
            return mBinder;
        }

        public void registerAvailableChangedListener(AvailableChangedListener l) {
            if (mService == null)
                return;
            mService.registerAvailableChangedListener(l);
        }

        public void unRegisterAvailableChangedListener(AvailableChangedListener l) {
            if (mService == null)
                return;
            mService.unRegisterAvailableChangedListener(l);
        }

        public void registerPhoneStateListener(IncomingRemindStateListener l) {
            if (mService == null)
                return;
            mService.registerPhoneStateListener(l);
        }

        public void unRegisterPhoneStateListener(IncomingRemindStateListener l) {
            if (mService == null)
                return;
            mService.unRegisterPhoneStateListener(l);
        }

        public boolean isConnected() {
            return mService != null && mService.isConnected;
        }

        public boolean isAvailable() {
            return mService != null && mService.isAvailable;
        }

        public void hangup() {
            if (mService == null)
                return;
            mService.hangup();
        }

        public void receiveSMS(String sms, String number) {
            if (mService == null)
                return;
            mService.receiveSMS(sms, number);
        }
        
        public void onRing() {
            if (mService == null)
                return;
            mService.onRing();
        }
        
        public void onStopRing() {
            if (mService == null)
                return;
            mService.onStopRing();
        }
    }

}
