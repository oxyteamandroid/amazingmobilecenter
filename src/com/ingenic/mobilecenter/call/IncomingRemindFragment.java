package com.ingenic.mobilecenter.call;

import com.ingenic.iwds.app.AmazingDialog;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.mobilecenter.R;
import com.ingenic.mobilecenter.sms.ReplySMSFragment;
import com.ingenic.mobilecenter.utils.Utils;
import com.ingenic.mobilecenter.view.PhoneOperationView;
import com.ingenic.mobilecenter.view.PhoneOperationView.SlideListener;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class IncomingRemindFragment extends Fragment implements View.OnClickListener {
    public IncomingRemindFragment(){};
    /**
     * 来电人名字或号码控件
     */
    private TextView mIRTextView;

    /**
     * 通话状态控件
     */
    private TextView mIRStateTextView;

    /**
     * 挂断按钮
     */
    private ImageButton mHangUpImageButton;

    /**
     * 挂断按钮动画控件
     */
    private ImageView mHangUpAnimView;

    /**
     * 挂断按钮动画
     */
    private AnimationDrawable mHangUpAnimation;

    /**
     * 圆屏挂断按钮
     */
    private PhoneOperationView mHangUpViewRound;

    /**
     * 短信回复按钮
     */
    private ImageButton mSMSReplyImageButton;

    /**
     * 短信回复按钮动画控件
     */
    private ImageView mSMSReplyAnimView;

    /**
     * 短信回复按钮动画
     */
    private AnimationDrawable mSMSReplyAnimation;

    /**
     * 圆屏短信回复按钮
     */
    private PhoneOperationView mSMSFailViewRound;

    /**
     * 来电人通讯录中名字
     */
    private String mIncomingName;

    /**
     * 来电人号码
     */
    private String mIncomingNumber;

    /**
     * 所属Activity
     */
    private TelephoneActivity mActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;
        if (Utils.isRound()) {
            view = inflater.inflate(R.layout.fragment_incoming_remind_round, container, false);
        } else {
            view = inflater.inflate(R.layout.fragment_incoming_remind, container, false);
        }
        initViews(view);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (TelephoneActivity) activity;
    }

    boolean isHangUpXY;
    boolean isSMSFailXY;

    /**
     * 初始化控件
     * 
     * @param view
     */
    @SuppressLint({ "ClickableViewAccessibility", "CutPasteId" })
    private void initViews(View view) {
        mIRTextView = (TextView) view.findViewById(R.id.incoming_remind_name);
        mIRStateTextView = (TextView) view.findViewById(R.id.incoming_remind_state);
        mIncomingName = mActivity.name;
        mIncomingNumber = mActivity.number;
        mIRTextView.setText(mIncomingName);
        IwdsLog.i(this, "incoming remind name:" + mIncomingName + ",number:" + mIncomingNumber);
        if (Utils.isRound()) {
            mSMSReplyAnimView = (ImageView) view.findViewById(R.id.sms_reply_animation);
            mSMSReplyAnimView.setBackgroundResource(R.anim.sms_reply_right);
            mSMSReplyAnimation = (AnimationDrawable) mSMSReplyAnimView.getBackground();
            mSMSReplyAnimation.start();

            mHangUpAnimView = (ImageView) view.findViewById(R.id.hang_up_animation);
            mHangUpAnimView.setBackgroundResource(R.anim.hang_up_left);
            mHangUpAnimation = (AnimationDrawable) mHangUpAnimView.getBackground();
            mHangUpAnimation.start();

            mHangUpViewRound = (PhoneOperationView) view.findViewById(R.id.incoming_remind_hangup);
            mHangUpViewRound.setOrientation(PhoneOperationView.ORIENTATION_LEFT);
            ViewTreeObserver observerHangUp = mHangUpViewRound.getViewTreeObserver();
            observerHangUp.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                @Override
                public void onGlobalLayout() {
                    if (!isHangUpXY) {
                        mHangUpViewRound.setDistance((int) (mHangUpViewRound.getX() - Utils.sScreenWidth / 2 + mHangUpViewRound.getWidth() / 2));
                        isHangUpXY = true;
                    }
                }
            });
            mHangUpViewRound.setSlideListener(mSlideListener);

            mSMSFailViewRound = (PhoneOperationView) view.findViewById(R.id.incoming_remind_sms);
            mSMSFailViewRound.setOrientation(PhoneOperationView.ORIENTATION_RIGHT);
            ViewTreeObserver observerSMSFail = mSMSFailViewRound.getViewTreeObserver();
            observerSMSFail.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                @Override
                public void onGlobalLayout() {
                    if (!isSMSFailXY) {
                        mSMSFailViewRound.setDistance((int) (Utils.sScreenWidth / 2 - mSMSFailViewRound.getX() - mSMSFailViewRound.getWidth() / 2));
                        isSMSFailXY = true;
                    }
                }
            });
            mSMSFailViewRound.setSlideListener(mSlideListener);
        } else {
            mHangUpImageButton = (ImageButton) view.findViewById(R.id.incoming_remind_hangup);
            mHangUpImageButton.setOnClickListener(this);

            mSMSReplyImageButton = (ImageButton) view.findViewById(R.id.incoming_remind_sms);
            mSMSReplyImageButton.setOnClickListener(this);
        }
    }

    /**
     * 按钮滑动监听
     */
    private SlideListener mSlideListener = new SlideListener() {

        @Override
        public void slideOver(View view) {
            if (mActivity == null)
                return;
            switch (view.getId()) {
            case R.id.incoming_remind_hangup:
                if (mActivity.hangup()) {
                    mActivity.mHandler.sendEmptyMessageDelayed(TelephoneActivity.MAG_1, TelephoneActivity.CALL_DELAYED_TIME);
                    mIRStateTextView.setText(R.string.disconnecting);
                    disableView();
                }
                break;
            case R.id.incoming_remind_sms:
                toReplySMSFragment();
                break;
            default:
                break;
            }
        }

        @Override
        public void down(View view) {
            switch (view.getId()) {
            case R.id.incoming_remind_hangup:
            case R.id.incoming_remind_sms:
                mHangUpAnimView.setVisibility(View.GONE);
                mSMSReplyAnimView.setVisibility(View.GONE);
                break;
            default:
                break;
            }
        }

        @Override
        public void up(View view) {
            switch (view.getId()) {
            case R.id.incoming_remind_hangup:
            case R.id.incoming_remind_sms:
                mHangUpAnimView.setVisibility(View.VISIBLE);
                mSMSReplyAnimView.setVisibility(View.VISIBLE);
                break;
            default:
                break;
            }
        }
    };

    /**
     * 跳转回复短信界面
     */
    private void toReplySMSFragment() {
        if (!Utils.isAvailable(mActivity)) {
            showIsAvailableDialog();
            return;
        }
        Intent intent = mActivity.getIntent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(Utils.TELEPHONE_FRAGMENT, ReplySMSFragment.class.getName());
        intent.putExtra(Utils.TELEPHONE_TAG, "reply_sms");
        intent.putExtra(Utils.TELEPHONE_CAN_BACK, true);
        mActivity.initFragment(intent);
        mActivity.onStopRing();
    }

    private AmazingDialog isAvailableDialog = null;

    private void showIsAvailableDialog() {

        if (isAvailableDialog == null) {
            isAvailableDialog = new AmazingDialog(mActivity).setRightScrollEnable(false);
            isAvailableDialog.setCancelable(false);
            isAvailableDialog.setContent(R.string.connect_disconnect);
            isAvailableDialog.setPositiveButton(R.drawable.ic_cancel, new OnClickListener() {

                @Override
                public void onClick(View v) {
                    isAvailableDialog.dismiss();
                    isAvailableDialog = null;
                    mActivity.onBackPressed();
                }
            });
        }
        isAvailableDialog.show();
    }

    /**
     * 禁用按钮
     */
    private void disableView() {
        if (Utils.isRound()) {
            if (mHangUpViewRound != null && mSMSFailViewRound != null) {
                if (!mHangUpViewRound.isEnabled()) {
                    mHangUpViewRound.setEnabled(false);
                }
                if (!mSMSFailViewRound.isEnabled()) {
                    mSMSFailViewRound.setEnabled(false);
                }
            }
            if (mHangUpAnimation != null) {
                mHangUpAnimation.stop();
            }
            if (mSMSReplyAnimation != null) {
                mSMSReplyAnimation.stop();
            }
            if (mHangUpAnimView != null && mSMSReplyAnimView != null) {
                mHangUpAnimView.setVisibility(View.GONE);
                mSMSReplyAnimView.setVisibility(View.GONE);
            }
        } else {
            if (mHangUpImageButton == null || mSMSReplyImageButton == null)
                return;
            if (!mHangUpImageButton.isEnabled()) {
                mHangUpImageButton.setEnabled(false);
            }
            if (!mSMSReplyImageButton.isEnabled()) {
                mSMSReplyImageButton.setEnabled(false);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.incoming_remind_hangup:
            if (mActivity != null) {
                if (mActivity.hangup()) {
                    mActivity.mHandler.sendEmptyMessageDelayed(TelephoneActivity.MAG_1, TelephoneActivity.CALL_DELAYED_TIME);
                    mIRStateTextView.setText(R.string.disconnecting);
                    disableView();
                }
            }
            break;
        case R.id.incoming_remind_sms:
            toReplySMSFragment();
            break;
        default:
            break;
        }
    }
}
