/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  YuanBanglin(Ahlin) <banglin.yuan@ingenic.com>
 *
 *  Elf/IDWS Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.mobilecenter.call;

import java.util.ArrayList;
import java.util.List;

import com.ingenic.iwds.utils.IwdsAssert;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.mobilecenter.utils.Utils;

import android.annotation.SuppressLint;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothHeadsetClient;
import android.bluetooth.BluetoothHeadsetClientCall;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothProfile.ServiceListener;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.text.TextUtils;

public class WatchPhoneService extends Service {

    /**
     * 蓝牙耳机端操作对象
     */
    private BluetoothHeadsetClient mBluetoothHeadsetClient;
    /**
     * 远程设备
     */
    private List<BluetoothDevice> mRemoteDevices;
    /**
     * 当前操作并连接的远程设备
     */
    private BluetoothDevice mRemoteDevice;

    /**
     * 通话状态改变观察者
     */
    private List<CallStateChangedListener> mCallStateChangedListeners = new ArrayList<WatchPhoneService.CallStateChangedListener>();

    /**
     * 通话音频改变观察者
     */
    private List<CallAudioChangedListener> mAudioStateChangedListeners = new ArrayList<WatchPhoneService.CallAudioChangedListener>();

    /**
     * 蓝牙音频连接改变观察者
     */
    private List<ConnectionStateChangedListener> mConnectionStateChangedListeners = new ArrayList<WatchPhoneService.ConnectionStateChangedListener>();

    @Override
    public void onCreate() {
        super.onCreate();
        IwdsLog.i(this, "onCreate WatchPhoneService");
        // 获取蓝牙音频通话服务代理对象
        BluetoothAdapter.getDefaultAdapter().getProfileProxy(this, mServiceListener, BluetoothProfile.HEADSET_CLIENT);
        registerReceiver();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        IwdsLog.i(this, "onDestroy WatchPhoneService");
        unregisterReceiver(mReceiver);
        if (mBluetoothHeadsetClient != null)
            BluetoothAdapter.getDefaultAdapter().closeProfileProxy(BluetoothProfile.HEADSET_CLIENT, mBluetoothHeadsetClient);
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return WatchPhoneBinder.getInstance(this);
    }

    /**
     * 获取蓝牙音频通话服务代理对象监听
     */
    @SuppressLint("NewApi")
    private ServiceListener mServiceListener = new ServiceListener() {

        @Override
        public void onServiceDisconnected(int profile) {
            if (profile == BluetoothProfile.HEADSET_CLIENT) {
                IwdsLog.i(WatchPhoneService.this, "onServiceDisconnected BluetoothHeadsetClient");
                mBluetoothHeadsetClient = null;
            }
        }

        @Override
        public void onServiceConnected(int profile, BluetoothProfile proxy) {
            if (profile == BluetoothProfile.HEADSET_CLIENT) {
                IwdsLog.i(WatchPhoneService.this, "onServiceConnected BluetoothHeadsetClient");
                mBluetoothHeadsetClient = (BluetoothHeadsetClient) proxy;
                mRemoteDevices = mBluetoothHeadsetClient.getConnectedDevices();
                if (mRemoteDevices != null && mRemoteDevices.size() > 0) {
                    mRemoteDevice = mRemoteDevices.get(0);
                }
            }
        }
    };

    /**
     * 注册广播接收器
     */
    private void registerReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothHeadsetClient.ACTION_CALL_CHANGED);// 通话状态改变
        filter.addAction(BluetoothHeadsetClient.ACTION_AUDIO_STATE_CHANGED);// 通话音频改变
        filter.addAction(BluetoothHeadsetClient.ACTION_CONNECTION_STATE_CHANGED);// 蓝牙音频连接状态改变
        registerReceiver(mReceiver, filter);
    }

    /**
     * 广播接收
     */
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @SuppressLint("InlinedApi")
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            IwdsLog.i(this, "Broadcast Action:" + action);
            // 通话状态改变action
            if (BluetoothHeadsetClient.ACTION_CALL_CHANGED.equals(action)) {
                BluetoothHeadsetClientCall clientCall = (BluetoothHeadsetClientCall) intent.getExtras().get(BluetoothHeadsetClient.EXTRA_CALL);
                if (clientCall == null)
                    return;
                handlingCallStatus(clientCall);
                // 通话音频改变action
            } else if (BluetoothHeadsetClient.ACTION_AUDIO_STATE_CHANGED.equals(action)) {
                int state = intent.getIntExtra(BluetoothProfile.EXTRA_STATE, -1);
                callAudioChangedNotifyObserver(state);
                // 蓝牙音频连接改变action
            } else if (BluetoothHeadsetClient.ACTION_CONNECTION_STATE_CHANGED.equals(action)) {
                handlingConnectionStateChanged(intent);
            }
        }
    };

    /**
     * 处理通话状态
     * 
     * @param call
     */
    private void handlingCallStatus(BluetoothHeadsetClientCall call) {
        int state = call.getState();
        IwdsLog.d(this, "Call State:" + call.getState() + ",number:" + call.getNumber() + ",Utils.sCallState" + Utils.sCallState);
        switch (state) {
        case BluetoothHeadsetClientCall.CALL_STATE_ACTIVE:// 进入通话中状态
            callStateChangedNotifyObserver(call);
            break;
        case BluetoothHeadsetClientCall.CALL_STATE_HELD:// 通话保持状态,一般是保持此次通话接通新的通话
            callStateChangedNotifyObserver(call);
            break;
        case BluetoothHeadsetClientCall.CALL_STATE_DIALING://
            callStateChangedNotifyObserver(call);
            break;
        case BluetoothHeadsetClientCall.CALL_STATE_ALERTING:// 蓝牙音频端已发出呼叫,远程设备已被通知到
            callStateChangedNotifyObserver(call);
            break;
        case BluetoothHeadsetClientCall.CALL_STATE_INCOMING:// 远程设备来电
            Intent it = new Intent(this, TelephoneActivity.class);
            it.setAction(Utils.ACTION_INCOMING_CALL);
            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            it.putExtra(Utils.TELEPHONE_FRAGMENT, IncomingFragment.class.getName());
            it.putExtra(Utils.TELEPHONE_TAG, "incoming");
            it.putExtra(Utils.TELEPHONE_NUMBER, call.getNumber());
            startActivity(it);
            Utils.sCallState = state;
            break;
        case BluetoothHeadsetClientCall.CALL_STATE_WAITING:// 远程设备等待,通话过程中又一个来电
            callStateChangedNotifyObserver(call);
            break;
        case BluetoothHeadsetClientCall.CALL_STATE_HELD_BY_RESPONSE_AND_HOLD://
            callStateChangedNotifyObserver(call);
            break;
        case BluetoothHeadsetClientCall.CALL_STATE_TERMINATED:// 通话终止，空闲状态
            callStateChangedNotifyObserver(call);
            break;
        default:
            break;
        }
    }

    /**
     * 处理蓝牙耳机连接状态改变(通知监听者并保存)
     * 
     * @param state
     */
    private void handlingConnectionStateChanged(Intent intent) {
        int state = intent.getIntExtra(BluetoothProfile.EXTRA_STATE, BluetoothHeadset.STATE_DISCONNECTED);
        BluetoothDevice device = (BluetoothDevice) intent.getExtras().get(BluetoothDevice.EXTRA_DEVICE);
        IwdsLog.i(this, "Bluetooth Headset state:" + state);
        if (BluetoothHeadsetClient.STATE_CONNECTED == state) {
            IwdsLog.i(this, "Bluetooth Headset connected");
            mRemoteDevice = device;

            SharedPreferences sp = getSharedPreferences(Utils.PREFERENCE_BOND, MODE_PRIVATE);
            sp.edit().putString(Utils.BLUETOOTH_HEADSET_CONNECTION_ADDRESS, mRemoteDevice.getAddress()).commit();
        } else if (BluetoothHeadsetClient.STATE_DISCONNECTED == state) {
            IwdsLog.i(this, "Bluetooth Headset disconnected");
            mRemoteDevice = null;

            SharedPreferences sp = getSharedPreferences(Utils.PREFERENCE_BOND, MODE_PRIVATE);
            sp.edit().putString(Utils.BLUETOOTH_HEADSET_CONNECTION_ADDRESS, "").commit();
        }
        connectionStateChangedNotifyObserver(state);

        SharedPreferences sp = getSharedPreferences(Utils.PREFERENCE_BOND, MODE_PRIVATE);
        sp.edit().putInt(Utils.BLUETOOTH_HEADSET_CONNECTION_STATE, state).commit();
    }

    /**
     * 通话状态改变监听
     * 
     * @author Ahlin
     * 
     */
    public interface CallStateChangedListener {
        void onCallStateChanged(BluetoothHeadsetClientCall call);
    }

    /**
     * 注册通话状态改变监听
     * 
     * @param l
     */
    private void registerCallStateChangedListener(CallStateChangedListener l) {
        if (mCallStateChangedListeners.contains(l)) {
            IwdsLog.w(this, "This listener is already registered!");
            return;
        }
        synchronized (mCallStateChangedListeners) {
            mCallStateChangedListeners.add(l);
        }
    }

    /**
     * 注销通话状态改变监听
     * 
     * @param l
     */
    private void unRegisterCallStateChangedListener(CallStateChangedListener l) {
        if (mCallStateChangedListeners.contains(l)) {
            synchronized (mCallStateChangedListeners) {
                mCallStateChangedListeners.remove(l);
            }
        }
    }

    /**
     * 通话状态改变通知观察者
     */
    private void callStateChangedNotifyObserver(BluetoothHeadsetClientCall call) {
        for (CallStateChangedListener listener : mCallStateChangedListeners) {
            listener.onCallStateChanged(call);
        }
    }

    /**
     * 通话音频改变监听
     * 
     * @author Ahlin
     */
    public interface CallAudioChangedListener {
        void onCallAudioChanged(int state);
    }

    /**
     * 注册通话音频改变监听
     * 
     * @param l
     */
    private void registerCallAudioChangedListener(CallAudioChangedListener l) {
        if (mAudioStateChangedListeners.contains(l)) {
            IwdsLog.w(this, "This listener is already registered!");
            return;
        }
        synchronized (mAudioStateChangedListeners) {
            mAudioStateChangedListeners.add(l);
        }
    }

    /**
     * 注销通话音频改变监听
     * 
     * @param l
     */
    private void unRegisterCallAudioChangedListener(CallAudioChangedListener l) {
        if (mAudioStateChangedListeners.contains(l)) {
            synchronized (mAudioStateChangedListeners) {
                mAudioStateChangedListeners.remove(l);
            }
        }
    }

    /**
     * 通话音频改变通知观察者
     */
    private void callAudioChangedNotifyObserver(int state) {
        IwdsLog.d(this, "audioState:" + state);
        if (state == BluetoothHeadsetClient.STATE_AUDIO_CONNECTED) {
            connectAudio();
        } else if (state == BluetoothHeadsetClient.STATE_AUDIO_DISCONNECTED) {
            disconnectAudio();
        }
        // for (CallAudioChangedListener listener : mAudioStateChangedListeners)
        // {
        // listener.onCallAudioChanged(state);
        // }
    }

    /**
     * 蓝牙音频连接改变监听
     * 
     * @author Ahlin
     */
    public interface ConnectionStateChangedListener {
        void onConnectionStateChanged(int state);
    }

    /**
     * 注册蓝牙音频连接改变监听
     * 
     * @param l
     */
    private void registerConnectionStateChangedListener(ConnectionStateChangedListener l) {
        if (mConnectionStateChangedListeners.contains(l)) {
            IwdsLog.w(this, "This listener is already registered!");
            return;
        }
        synchronized (mConnectionStateChangedListeners) {
            mConnectionStateChangedListeners.add(l);
        }
    }

    /**
     * 注销蓝牙音频连接改变监听
     * 
     * @param l
     */
    private void unRegisterConnectionStateChangedListener(ConnectionStateChangedListener l) {
        if (mConnectionStateChangedListeners.contains(l)) {
            synchronized (mConnectionStateChangedListeners) {
                mConnectionStateChangedListeners.remove(l);
            }
        }
    }

    /**
     * 蓝牙音频连接改变通知观察者
     */
    private void connectionStateChangedNotifyObserver(int state) {
        if (state == BluetoothHeadsetClient.STATE_DISCONNECTED)
            Utils.sCallState = BluetoothHeadsetClientCall.CALL_STATE_TERMINATED;

        for (ConnectionStateChangedListener listener : mConnectionStateChangedListeners) {
            listener.onConnectionStateChanged(state);
        }
    }

    /**
     * 检查通道是否可用
     * 
     * @return
     */
    private boolean isAvailable() {
        if (mBluetoothHeadsetClient == null) {
            IwdsLog.e(this, "Bluetooth Headset Client is null");
            return false;
        }
        if (mRemoteDevice == null) {
            IwdsLog.e(this, "Remote device is null");
            return false;
        }
        return true;
    }

    /**
     * 拨号
     * 
     * @param number
     *            号码
     * @return true:命令成功发出 or false
     */
    private boolean dial(String number) {
        IwdsAssert.dieIf(this, TextUtils.isEmpty(number), "number is null");
        if (!isAvailable())
            return false;
        return mBluetoothHeadsetClient.dial(mRemoteDevice, number);
    }

    /**
     * 取消呼叫
     * 
     * @param index
     *            电话索引
     * @return true:命令成功发出 or false
     */
    private boolean terminateCall(int index) {
        if (index < 0) {
            IwdsLog.e(this, "Index cannot be less than 0");
            return false;
        }
        if (!isAvailable())
            return false;
        IwdsLog.i(this, "BluetoothHeadsetClient terminateCall index is"+index);
        return mBluetoothHeadsetClient.terminateCall(mRemoteDevice, index);
    }

    /**
     * 重拨
     * 
     * @return true:命令成功发出 or false
     */
    private boolean redial() {
        if (!isAvailable())
            return false;
        return mBluetoothHeadsetClient.redial(mRemoteDevice);
    }

    /**
     * 通话中发送服务命令
     * 
     * @param code
     *            命令码
     * @return true:命令成功发出 or false
     */
    private boolean sendDTMF(byte code) {
        if (!isAvailable())
            return false;
        return mBluetoothHeadsetClient.sendDTMF(mRemoteDevice, code);
    }

    /**
     * 断开音频
     * 
     * @return
     */
    private boolean disconnectAudio() {
        if (mBluetoothHeadsetClient == null) {
            IwdsLog.e(this, "Bluetooth Headset Client is null");
            return false;
        }
        return mBluetoothHeadsetClient.disconnectAudio();
    }

    /**
     * 连接音频
     * 
     * @return
     */
    private boolean connectAudio() {
        if (mBluetoothHeadsetClient == null) {
            IwdsLog.e(this, "Bluetooth Headset Client is null");
            return false;
        }
        return mBluetoothHeadsetClient.connectAudio();
    }

    /**
     * 获取当前音频状态
     * 
     * @return
     */
    private int getAudioState() {
        if (!isAvailable())
            return BluetoothHeadsetClient.STATE_AUDIO_DISCONNECTED;
        return mBluetoothHeadsetClient.getAudioState(mRemoteDevice);
    }

    /**
     * 来电挂断
     * 
     * @return
     */
    private boolean rejectCall() {
        if (!isAvailable())
            return false;
        return mBluetoothHeadsetClient.rejectCall(mRemoteDevice);
    }

    /**
     * 保留通话
     * 
     * @return
     */
    private boolean holdCall() {
        if (!isAvailable())
            return false;
        return mBluetoothHeadsetClient.holdCall(mRemoteDevice);
    }

    /**
     * 来电接听
     * 
     * @param flag
     *            action policy while accepting a call. Possible values
     *            {@link #CALL_ACCEPT_NONE}, {@link #CALL_ACCEPT_HOLD},
     *            {@link #CALL_ACCEPT_TERMINATE}
     * @return
     */
    private boolean acceptCall(int flag) {
        if (!isAvailable())
            return false;
        return mBluetoothHeadsetClient.acceptCall(mRemoteDevice, flag);
    }

    /**
     * 获取当前通话
     * 
     * @return
     */
    private List<BluetoothHeadsetClientCall> getCurrentCalls() {
        if (!isAvailable())
            return null;
        return mBluetoothHeadsetClient.getCurrentCalls(mRemoteDevice);
    }
    
    /**
     * 是否计时
     */
    private boolean isTime;
    
    /**
     * 设置是否计时
     * @param isTime
     */
    private void setIsTime(boolean isTime){
        this.isTime = isTime;
    }

    /**
     * 当前显示计时索引
     */
    private int mTimeIndex = 0;
    
    /**
     * 所有计时
     */
    int[] time = new int[0];
    
    /**
     * 切换计时
     * @param index
     */
    private void switchTime(int index){
        if(index < time.length){
            mTimeIndex = index;
            IwdsLog.i(this, "switchTime:"+mTimeIndex);
        }
    }

    /**
     * 计时监听对象
     */
    private TimeChangedListener mTimeChangedListener;

    /**
     * 计时处理
     */
    private Handler mHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            if (mTimeChangedListener != null && isTime)
                mTimeChangedListener.onTimeChanged(Utils.formatTime(time[mTimeIndex]));
            if (!mHandler.hasMessages(0)) {
                mHandler.sendEmptyMessageDelayed(0, 1000);
                for (int i = 0; i < time.length; i++) {
                    time[i]++;
                }
            }
            return false;
        }
    });

    int[] b;

    /**
     * 添加计时
     */
    private void addTime() {
        int size = getCurrentCalls().size();
        if(size <= time.length) return;
        b = new int[time.length + 1];
        for (int i = 0; i < time.length; i++) {
            b[i] = time[i];
        }
        time = b;
    }

    /**
     * 开始计时
     * 
     * @param listener
     */
    private void startTimer(TimeChangedListener listener) {
        mTimeChangedListener = listener;
        setIsTime(true);
        if (!mHandler.hasMessages(0)) {
            mHandler.sendEmptyMessage(0);
        }
    }

    /**
     * 删除计时
     * 
     * @param index
     */
    private void deleteTime(int index) {
        b = new int[time.length - 1];
        for (int i = index; i < time.length - 1; i++) {
            time[i] = time[i + 1];
        }
        for (int i = 0; i < b.length; i++) {
            b[i] = time[i];
        }
        time = b;
    }

    /**
     * 停止计时
     */
    private void stopTimer() {
        time = new int[0];
        mTimeIndex = 0;
        if (mHandler.hasMessages(0)) {
            mHandler.removeMessages(0);
        }
    }

    /**
     * 返回通话计时
     * 
     * @param index
     *            通话索引
     * @return
     */
    private String getTime(int index) {
        return Utils.formatTime(time[index]);
    }

    /**
     * 计时监听
     * 
     * @author Ahlin
     * 
     */
    interface TimeChangedListener {
        void onTimeChanged(String time);
    }

    /**
     * 服务代理
     * 
     * @author Ahlin
     * 
     */
    public static class WatchPhoneBinder extends Binder {
        /**
         * 服务对象
         */
        private WatchPhoneService mService;
        /**
         * 代理对象
         */
        private static WatchPhoneBinder mBinder;

        private WatchPhoneBinder(WatchPhoneService service) {
            mService = service;
        }

        public static WatchPhoneBinder getInstance(WatchPhoneService service) {
            if (mBinder == null) {
                synchronized (WatchPhoneBinder.class) {
                    if (mBinder == null)
                        mBinder = new WatchPhoneBinder(service);
                }
            }
            return mBinder;
        }
        
        public void setIsTime(boolean isTime) {
            if (mService == null)
                return;
            mService.setIsTime(isTime);
        }

        public void startTimer(TimeChangedListener listener) {
            if (mService == null)
                return;
            mService.startTimer(listener);
        }

        public void stopTimer() {
            if (mService == null)
                return;
            mService.stopTimer();
        }

        public String getTime(int index) {
            if (mService == null)
                return null;
            return mService.getTime(index);
        }
        
        public void switchTime(int index){
            if (mService == null)
                return;
            mService.switchTime(index);
        }
        
        public void addTime(){
            if (mService == null)
                return;
            mService.addTime();
        }
        
        public void deleteTime(int index){
            if (mService == null)
                return;
            mService.deleteTime(index);
        }

        public boolean acceptCall(int flag) {
            if (mService == null)
                return false;

            return mService.acceptCall(flag);
        }

        public boolean holdCall() {
            if (mService == null)
                return false;

            return mService.holdCall();
        }

        public boolean rejectCall() {
            if (mService == null)
                return false;

            return mService.rejectCall();
        }

        public int getAudioState() {
            if (mService == null)
                return BluetoothHeadsetClient.STATE_AUDIO_DISCONNECTED;

            return mService.getAudioState();
        }

        public boolean connectAudio() {
            if (mService == null)
                return false;

            return mService.connectAudio();
        }

        public boolean disconnectAudio() {
            if (mService == null)
                return false;

            return mService.disconnectAudio();
        }

        public boolean sendDTMF(byte code) {
            if (mService == null)
                return false;

            return mService.sendDTMF(code);
        }

        public boolean redial() {
            if (mService == null)
                return false;

            return mService.redial();
        }

        public boolean terminateCall(int index) {
            if (mService == null)
                return false;

            return mService.terminateCall(index);
        }

        public boolean dial(String number) {
            if (mService == null)
                return false;

            return mService.dial(number);
        }

        public List<BluetoothHeadsetClientCall> getCurrentCalls() {
            if (mService == null)
                return null;
            return mService.getCurrentCalls();
        }

        public void registerCallStateChangedListener(CallStateChangedListener listener) {
            if (mService == null)
                return;

            mService.registerCallStateChangedListener(listener);
        }

        public void registerCallAudioChangedListener(CallAudioChangedListener listener) {
            if (mService == null)
                return;

            mService.registerCallAudioChangedListener(listener);
        }

        public void registerConnectionStateChangedListener(ConnectionStateChangedListener listener) {
            if (mService == null)
                return;

            mService.registerConnectionStateChangedListener(listener);
        }

        public void unRegisterCallStateChangedListener(CallStateChangedListener listener) {
            if (mService == null)
                return;

            mService.unRegisterCallStateChangedListener(listener);
        }

        public void unRegisterCallAudioChangedListener(CallAudioChangedListener listener) {
            if (mService == null)
                return;

            mService.unRegisterCallAudioChangedListener(listener);
        }

        public void unRegisterConnectionStateChangedListener(ConnectionStateChangedListener listener) {
            if (mService == null)
                return;

            mService.unRegisterConnectionStateChangedListener(listener);
        }
    }
}
