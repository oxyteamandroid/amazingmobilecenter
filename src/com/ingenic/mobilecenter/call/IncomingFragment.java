package com.ingenic.mobilecenter.call;

import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.mobilecenter.R;
import com.ingenic.mobilecenter.sms.ReplySMSFragment;
import com.ingenic.mobilecenter.utils.Utils;
import com.ingenic.mobilecenter.view.PhoneOperationView;
import com.ingenic.mobilecenter.view.PhoneOperationView.SlideListener;

import android.app.Activity;
import android.app.Fragment;
import android.bluetooth.BluetoothHeadsetClient;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class IncomingFragment extends Fragment implements View.OnClickListener {
    public IncomingFragment() {
    }

    /**
     * 所属Activity
     */
    private TelephoneActivity mActivity;

    /**
     * 来电人名字或号码控件
     */
    private TextView mIncomingNmaeTextView;

    /**
     * 通话状态控件
     */
    private TextView mIncomingStateTextView;

    /**
     * 挂断按钮
     */
    private ImageButton mHangUpImageButton;

    /**
     * 接听按钮
     */
    private ImageButton mAnswerImageButton;

    /**
     * 方屏短信回复按钮
     */
    private Button mSMSReplyButton;

    /**
     * 圆屏接听按钮
     */
    private PhoneOperationView mAnswerViewRound;

    /**
     * 圆屏接听按钮动画控件
     */
    private ImageView mAnswerAnimView;

    /**
     * 圆屏接听按钮动画
     */
    private AnimationDrawable mAnswerAnimation;

    /**
     * 圆屏挂断按钮
     */
    private PhoneOperationView mHangUpViewRound;

    /**
     * 圆屏挂断按钮动画控件
     */
    private ImageView mHangUpAnimView;

    /**
     * 圆屏挂断按钮动画
     */
    private AnimationDrawable mHangUpAnimation;

    /**
     * 圆屏短信回复按钮
     */
    private PhoneOperationView mSMSReplyViewRound;

    /**
     * 圆屏短信回复按钮动画控件
     */
    private ImageView mSMSReplyAnimView;

    /**
     * 圆屏短信回复按钮动画
     */
    private AnimationDrawable mSMSReplyAnimation;

    /**
     * 来电人通讯录中名字
     */
    private String mIncomingName;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (TelephoneActivity) activity;
        mIncomingName = mActivity.name;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;
        if (Utils.isRound()) {
            view = inflater.inflate(R.layout.fragment_incoming_round, container, false);
        } else {
            view = inflater.inflate(R.layout.fragment_incoming, container, false);
        }
        initViews(view);
        return view;
    }

    /**
     * 初始化控件
     * 
     * @param view
     */
    private void initViews(View view) {
        mIncomingNmaeTextView = (TextView) view.findViewById(R.id.incoming_name);
        mIncomingNmaeTextView.setText(mIncomingName);
        mIncomingStateTextView = (TextView) view.findViewById(R.id.incoming_state);
        if (Utils.isRound()) {
            if (Utils.isAvailable(mActivity)) {
                mSMSReplyAnimView = (ImageView) view.findViewById(R.id.sms_reply_animation);
                mSMSReplyAnimView.setBackgroundResource(R.anim.sms_reply_top);
                mSMSReplyAnimation = (AnimationDrawable) mSMSReplyAnimView.getBackground();
                mSMSReplyAnimation.start();
            }

            mHangUpAnimView = (ImageView) view.findViewById(R.id.hang_up_animation);
            mHangUpAnimView.setBackgroundResource(R.anim.hang_up_left);
            mHangUpAnimation = (AnimationDrawable) mHangUpAnimView.getBackground();
            mHangUpAnimation.start();

            mAnswerAnimView = (ImageView) view.findViewById(R.id.answer_animation);
            mAnswerAnimView.setBackgroundResource(R.anim.answer_right);
            mAnswerAnimation = (AnimationDrawable) mAnswerAnimView.getBackground();
            mAnswerAnimation.start();

            mAnswerViewRound = (PhoneOperationView) view.findViewById(R.id.incoming_answer);
            mAnswerViewRound.setOrientation(PhoneOperationView.ORIENTATION_RIGHT);
            mAnswerViewRound.setSlideListener(mSlideListener);
            ViewTreeObserver observerAnswer = mAnswerViewRound.getViewTreeObserver();
            observerAnswer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                @Override
                public void onGlobalLayout() {
                    mAnswerViewRound.setDistance((int) (Utils.sScreenWidth / 2 - mAnswerViewRound.getX() - mAnswerViewRound.getWidth() / 2));
                }
            });

            mHangUpViewRound = (PhoneOperationView) view.findViewById(R.id.incoming_hangup);
            mHangUpViewRound.setOrientation(PhoneOperationView.ORIENTATION_LEFT);
            mHangUpViewRound.setSlideListener(mSlideListener);
            ViewTreeObserver observerHangUp = mHangUpViewRound.getViewTreeObserver();
            observerHangUp.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                @Override
                public void onGlobalLayout() {
                    mHangUpViewRound.setDistance((int) (mHangUpViewRound.getX() - Utils.sScreenWidth / 2 + mHangUpViewRound.getWidth() / 2));
                }
            });

            mSMSReplyViewRound = (PhoneOperationView) view.findViewById(R.id.incoming_sms_reply);
            mSMSReplyViewRound.setOrientation(PhoneOperationView.ORIENTATION_TOP);
            mSMSReplyViewRound.setSlideListener(mSlideListener);
            ViewTreeObserver observerSMSReply = mSMSReplyViewRound.getViewTreeObserver();
            observerSMSReply.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                @Override
                public void onGlobalLayout() {
                    mSMSReplyViewRound.setDistance((int) (mSMSReplyViewRound.getY()));
                    IwdsLog.d(this, "mSMSReplyViewRound.getY():" + mSMSReplyViewRound.getY());
                }
            });
            if (Utils.isAvailable(mActivity)) {
                mSMSReplyViewRound.setVisibility(View.VISIBLE);
            } else {
                mSMSReplyViewRound.setVisibility(View.GONE);
            }
        } else {
            mAnswerImageButton = (ImageButton) view.findViewById(R.id.incoming_answer);
            mAnswerImageButton.setOnClickListener(this);
            mHangUpImageButton = (ImageButton) view.findViewById(R.id.incoming_hangup);
            mHangUpImageButton.setOnClickListener(this);
            mSMSReplyButton = (Button) view.findViewById(R.id.incoming_sms_reply);
            mSMSReplyButton.setOnClickListener(this);
            if (Utils.isAvailable(mActivity)) {
                mSMSReplyButton.setVisibility(View.VISIBLE);
            } else {
                mSMSReplyButton.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.incoming_answer:
            answer();
            break;
        case R.id.incoming_hangup:
            hangUp();
            break;
        case R.id.incoming_sms_reply:
            smsReply();
            break;
        default:
            break;
        }
    }

    /**
     * 按钮滑动监听
     */
    private SlideListener mSlideListener = new SlideListener() {

        @Override
        public void slideOver(View view) {
            if (mActivity == null)
                return;
            switch (view.getId()) {
            case R.id.incoming_hangup:
                hangUp();
                break;
            case R.id.incoming_sms_reply:
                smsReply();
                break;
            case R.id.incoming_answer:
                answer();
                break;
            default:
                break;
            }
        }

        @Override
        public void down(View view) {
            switch (view.getId()) {
            case R.id.incoming_hangup:
            case R.id.incoming_sms_reply:
            case R.id.incoming_answer:
                goneAnimation();
                break;
            default:
                break;
            }
        }

        @Override
        public void up(View view) {
            switch (view.getId()) {
            case R.id.incoming_hangup:
            case R.id.incoming_sms_reply:
            case R.id.incoming_answer:
                visibleAnimation();
                break;
            default:
                break;
            }
        }
    };

    /**
     * 挂断处理
     */
    private void hangUp() {
        if (mActivity == null)
            return;
        if (mActivity.rejectCall()) {
            mActivity.mHandler.sendEmptyMessageDelayed(TelephoneActivity.MAG_1, TelephoneActivity.CALL_DELAYED_TIME);
            mIncomingStateTextView.setText(R.string.disconnecting);
            disableView();
        }
    }

    /**
     * 接听处理
     */
    private void answer() {
        if (mActivity == null)
            return;
        boolean isSuccess = false;
        int size = mActivity.mWatchPhoneBinder.getCurrentCalls().size();
        if (size > 1) {
            if (size > 2) {
                isSuccess = mActivity.acceptCall(BluetoothHeadsetClient.CALL_ACCEPT_TERMINATE);
            } else {
                isSuccess = mActivity.acceptCall(BluetoothHeadsetClient.CALL_ACCEPT_HOLD);
            }
        } else {
            isSuccess = mActivity.acceptCall(BluetoothHeadsetClient.CALL_ACCEPT_NONE);
        }
        if (isSuccess) {
            mIncomingStateTextView.setText(R.string.accepting);
            disableView();
        }
    }

    /**
     * 短信回复处理
     */
    private void smsReply() {
        toReplySMSFragment();
        disableView();
    }

    /**
     * 禁用控件
     */
    private void disableView() {
        if (Utils.isRound()) {
            disableButton();
            if (mAnswerAnimation != null) {
                mAnswerAnimation.stop();
            }
            if (mHangUpAnimation != null) {
                mHangUpAnimation.stop();
            }
            if (mSMSReplyAnimation != null) {
                mSMSReplyAnimation.stop();
            }
            goneAnimation();
        } else {
            disableButton();
        }
    }

    /**
     * 隐藏动画
     */
    private void goneAnimation() {
        if (mAnswerAnimView != null)
            mAnswerAnimView.setVisibility(View.GONE);
        if (mHangUpAnimView != null)
            mHangUpAnimView.setVisibility(View.GONE);
        if (mSMSReplyAnimView != null)
            mSMSReplyAnimView.setVisibility(View.GONE);
    }

    /**
     * 显示动画
     */
    private void visibleAnimation() {
        if (mAnswerAnimView != null)
            mAnswerAnimView.setVisibility(View.VISIBLE);
        if (mHangUpAnimView != null)
            mHangUpAnimView.setVisibility(View.VISIBLE);
        if (mSMSReplyAnimView != null)
            mSMSReplyAnimView.setVisibility(View.VISIBLE);
    }

    /**
     * 禁用按钮
     */
    private void disableButton() {
        if (mAnswerImageButton != null)
            mAnswerImageButton.setEnabled(false);
        if (mHangUpImageButton != null)
            mHangUpImageButton.setEnabled(false);
        if (mSMSReplyButton != null)
            mSMSReplyButton.setEnabled(false);
    }

    /**
     * 跳转回复短信界面
     */
    private void toReplySMSFragment() {
        Intent intent = mActivity.getIntent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(Utils.TELEPHONE_FRAGMENT, ReplySMSFragment.class.getName());
        intent.putExtra(Utils.TELEPHONE_TAG, "reply_sms");
        intent.putExtra(Utils.TELEPHONE_CAN_BACK, true);
        mActivity.initFragment(intent);
        mActivity.onStopRing();
    }
}
