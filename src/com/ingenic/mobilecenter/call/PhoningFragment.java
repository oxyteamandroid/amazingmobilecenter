package com.ingenic.mobilecenter.call;

import com.ingenic.iwds.utils.IwdsAssert;
import com.ingenic.iwds.widget.RightScrollView;
import com.ingenic.mobilecenter.R;
import com.ingenic.mobilecenter.call.WatchPhoneService.TimeChangedListener;
import com.ingenic.mobilecenter.utils.NumberUtils;
import com.ingenic.mobilecenter.utils.Utils;

import android.app.Activity;
import android.app.Fragment;
import android.bluetooth.BluetoothHeadsetClient;
import android.bluetooth.BluetoothHeadsetClientCall;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

public class PhoningFragment extends Fragment implements TimeChangedListener, View.OnClickListener {
    public PhoningFragment(){}

    /**
     * 所属Activity
     */
    private TelephoneActivity mActivity;

    /**
     * 右滑控件
     */
    private RightScrollView mRightScrollView;

    /**
     * 通话人备注控件
     */
    private TextView mNameTextView;

    /**
     * 通话人备注
     */
    private String mName;

    /**
     * 通话状态控件
     */
    private TextView mStateTextView;

    /**
     * 通话状态
     */
    // private String mState;

    /**
     * 显示手机端正在通话的控件
     */
    private TextView mPhonePhoningTextView;

    /**
     * 进入输入盘按钮
     */
    private ImageButton mInputImageButton;

    /**
     * 挂断按钮
     */
    private ImageButton mHangUpImageButton;

    /**
     * 当前通话索引
     */
    private int callIndex = 0;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (TelephoneActivity) activity;
        mActivity.mWatchPhoneBinder.startTimer(this);
        mName = mActivity.name;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;
        // if (Utils.isRound()) {
        //
        // } else {
        view = inflater.inflate(R.layout.fragment_phoning, container, false);
        // }
        initViews(view);
        return view;
    }

    /**
     * 初始化控件
     * 
     * @param view
     */
    private void initViews(View view) {
        mRightScrollView = (RightScrollView) view.findViewById(R.id.right_scroll);
        mRightScrollView.disableRightScroll();
        // if (Utils.isRound()) {
        //
        // } else {
        mNameTextView = (TextView) view.findViewById(R.id.phoning_name);
        mNameTextView.setText(mName);
        mStateTextView = (TextView) view.findViewById(R.id.phoning_state);
        if (mActivity.mWatchPhoneBinder != null)
            mStateTextView.setText(mActivity.mWatchPhoneBinder.getTime(callIndex));
        mPhonePhoningTextView = (TextView) view.findViewById(R.id.phoning_end);
        if (mActivity.getAudioState() == BluetoothHeadsetClient.STATE_AUDIO_CONNECTED) {
            mPhonePhoningTextView.setVisibility(View.INVISIBLE);
        }
        mInputImageButton = (ImageButton) view.findViewById(R.id.phoning_input);
        mInputImageButton.setOnClickListener(this);
        mHangUpImageButton = (ImageButton) view.findViewById(R.id.phoning_hangup);
        mHangUpImageButton.setOnClickListener(this);
        mInputImageButton.setEnabled(true);
        mHangUpImageButton.setEnabled(true);
        // }
    }

    /**
     * 通话音频改变
     * 
     * @param state
     */
    public void onCallAudioChanged(int state) {
        if (state == BluetoothHeadsetClient.STATE_AUDIO_CONNECTED) {
            mActivity.mWatchPhoneBinder.connectAudio();
            mPhonePhoningTextView.setVisibility(View.INVISIBLE);
        } else if (state == BluetoothHeadsetClient.STATE_AUDIO_DISCONNECTED) {
            mActivity.mWatchPhoneBinder.disconnectAudio();
            mPhonePhoningTextView.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 更换通话
     * 
     * @param call
     */
    public void updateCall(BluetoothHeadsetClientCall call) {
        IwdsAssert.dieIf(this, call == null, "Call is null");
        if (mActivity == null)
            return;
        // 切换当前通话索引
        if (mActivity.mWatchPhoneBinder != null){
            mActivity.mWatchPhoneBinder.setIsTime(true);
            callIndex = mActivity.indexOf(call);
            mActivity.mWatchPhoneBinder.switchTime(mActivity.getIdIndex(call.getId()));
        }
        String number = call.getNumber();
        String name = NumberUtils.queryName(mActivity, number);
        mName = TextUtils.isEmpty(name) ? number : name;
        mNameTextView.setText(mName);
        isHangUp = false;
        mInputImageButton.setEnabled(true);
        mHangUpImageButton.setEnabled(true);
    }

    /**
     * 计时
     */
    @Override
    public void onTimeChanged(String time) {
        if (mStateTextView == null)
            return;
        if (!isHangUp)
            mStateTextView.setText(time);
    }

    /**
     * 是否挂断
     */
    boolean isHangUp = false;

    /**
     * 按钮点击
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.phoning_input:
            toInputFragment();
            break;
        case R.id.phoning_hangup:
            hangUp();
            break;
        default:
            break;
        }
    }

    /**
     * 跳转命令输入界面
     */
    private void toInputFragment() {
        Intent intent = mActivity.getIntent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(Utils.TELEPHONE_FRAGMENT, InputFragment.class.getName());
        intent.putExtra(Utils.TELEPHONE_TAG, "input");
        intent.putExtra(Utils.TELEPHONE_CAN_BACK, true);
        mActivity.initFragment(intent);
        mActivity.onStopRing();
    }

    /**
     * 挂断处理
     */
    private void hangUp() {
        if (mActivity == null)
            return;
        if (mActivity.terminateCall(callIndex)) {
            isHangUp = true;
            mActivity.mHandler.sendEmptyMessageDelayed(TelephoneActivity.MAG_1, TelephoneActivity.CALL_DELAYED_TIME);
            mStateTextView.setText(R.string.disconnecting);
            disableView();
        }
    }

    /**
     * 禁用控件
     */
    private void disableView() {
        // if (Utils.isRound()) {
        //
        // } else {
        if (mInputImageButton != null)
            mInputImageButton.setEnabled(false);
        if (mHangUpImageButton != null)
            mHangUpImageButton.setEnabled(false);
        // }
    }

    // private OnRightScrollListener mOnRightScrollListener = new
    // OnRightScrollListener() {
    //
    // @Override
    // public void onRightScroll() {
    // if (mActivity != null)
    // mActivity.finish();
    // }
    // };
}
