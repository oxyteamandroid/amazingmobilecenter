package com.ingenic.mobilecenter.call;

import com.ingenic.mobilecenter.R;
import com.ingenic.mobilecenter.utils.Utils;
import com.ingenic.mobilecenter.view.PhoneOperationView;
import com.ingenic.mobilecenter.view.PhoneOperationView.SlideListener;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class CallFargment extends Fragment {
    public CallFargment() {
    }

    /**
     * 所属Activity
     */
    private TelephoneActivity mActivity;

    /**
     * 呼叫的号码备注
     */
    private String mIncomingName;

    /**
     * 呼叫的号码或备注控件
     */
    private TextView mCallNmaeTextView;

    /**
     * 通话状态控件
     */
    private TextView mCallStateTextView;

    /**
     * 挂断按钮
     */
    private ImageButton mHangUpImageButton;

    /**
     * 圆屏挂断按钮
     */
    private PhoneOperationView mHangUpViewRound;

    /**
     * 挂断左动画控件
     */
    private ImageView mHangUpLeftAnimView;

    /**
     * 挂断左动画
     */
    private AnimationDrawable mHangUpLeftAnimation;

    /**
     * 挂断右动画控件
     */
    private ImageView mHangUpRightAnimView;

    /**
     * 挂断右动画
     */
    private AnimationDrawable mHangUpRightAnimation;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (TelephoneActivity) activity;
        mIncomingName = mActivity.name;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;
        if (Utils.isRound()) {
            view = inflater.inflate(R.layout.fragment_call_round, container, false);
        } else {
            view = inflater.inflate(R.layout.fragment_call, container, false);
        }
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        mCallNmaeTextView = (TextView) view.findViewById(R.id.call_name);
        mCallNmaeTextView.setText(mIncomingName);
        mCallStateTextView = (TextView) view.findViewById(R.id.call_state);
        if (Utils.isRound()) {
            mHangUpLeftAnimView = (ImageView) view.findViewById(R.id.hang_up_animation_left);
            mHangUpLeftAnimView.setBackgroundResource(R.anim.hang_up_right);
            mHangUpLeftAnimation = (AnimationDrawable) mHangUpLeftAnimView.getBackground();
            mHangUpLeftAnimation.start();

            mHangUpRightAnimView = (ImageView) view.findViewById(R.id.hang_up_animation_right);
            mHangUpRightAnimView.setBackgroundResource(R.anim.hang_up_left);
            mHangUpRightAnimation = (AnimationDrawable) mHangUpRightAnimView.getBackground();
            mHangUpRightAnimation.start();

            mHangUpViewRound = (PhoneOperationView) view.findViewById(R.id.call_hang_up);
            mHangUpViewRound.setSlideListener(mSlideListener);
        } else {
            mHangUpImageButton = (ImageButton) view.findViewById(R.id.call_hang_up);
            mHangUpImageButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    hangUp();
                }
            });
        }
    }

    boolean isHangUp = false;

    /**
     * 挂断处理
     */
    private void hangUp() {
        isHangUp = true;
        mCallStateTextView.setText(R.string.disconnecting);
        if (mActivity == null)
            return;
        if (mActivity.terminateCall(0)) {
            disableView();
            isHangUp = false;
        }
    }

    /**
     * 延时挂断
     */
    public void delayHangUp() {
        if (isHangUp) {
            hangUp();
        }
    }

    /**
     * 禁用控件
     */
    private void disableView() {
        if (Utils.isRound()) {
            mHangUpViewRound.setEnabled(false);
            goneAnimView();
            onStopAnim();
        } else {
            mHangUpImageButton.setEnabled(false);
        }
    }

    /**
     * 停止动画
     */
    private void onStopAnim() {
        if (mHangUpLeftAnimation != null)
            mHangUpLeftAnimation.stop();
        if (mHangUpRightAnimation != null)
            mHangUpRightAnimation.stop();
    }

    /**
     * 恢复动画
     */
    private void onStartAnim() {
        if (mHangUpLeftAnimation != null)
            mHangUpLeftAnimation.start();
        if (mHangUpRightAnimation != null)
            mHangUpRightAnimation.start();
    }

    /**
     * 显示动画控件
     */
    private void visibleAnimView() {
        if (mHangUpLeftAnimView != null)
            mHangUpLeftAnimView.setVisibility(View.VISIBLE);
        if (mHangUpRightAnimView != null)
            mHangUpRightAnimView.setVisibility(View.VISIBLE);
    }

    /**
     * 隐藏动画控件
     */
    private void goneAnimView() {
        if (mHangUpLeftAnimView != null)
            mHangUpLeftAnimView.setVisibility(View.GONE);
        if (mHangUpRightAnimView != null)
            mHangUpRightAnimView.setVisibility(View.GONE);
    }

    /**
     * 按钮滑动监听
     */
    private SlideListener mSlideListener = new SlideListener() {

        @Override
        public void slideOver(View view) {
            if (mActivity == null)
                return;
            switch (view.getId()) {
            case R.id.call_hang_up:
                hangUp();
                break;
            default:
                break;
            }
        }

        @Override
        public void down(View view) {
            disableView();
        }

        @Override
        public void up(View view) {
            mHangUpViewRound.setEnabled(true);
            visibleAnimView();
            onStartAnim();
        }
    };

}
