package com.ingenic.mobilecenter.call;

import com.ingenic.mobilecenter.R;
import com.ingenic.mobilecenter.utils.Utils;
import com.ingenic.mobilecenter.view.InputView;
import com.ingenic.mobilecenter.view.InputViewRound;
import com.ingenic.mobilecenter.view.InputView.OnClickListener;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class InputFragment extends Fragment implements View.OnLongClickListener, OnClickListener,
        com.ingenic.mobilecenter.view.InputViewRound.OnClickListener {
    public InputFragment(){}
    /**
     * 按键显示TextView
     */
    private TextView mNumberInputTextView;

    /**
     * 方屏自定义输入盘控件
     */
    private InputView mInputView;

    /**
     * 圆屏自定义输入盘控件
     */
    private InputViewRound mInputViewRound;

    /**
     * 圆屏0,+ 复用控件
     */
    private Button mZeroPlusButton;

    /**
     * 圆屏*,# 复用控件
     */
    private Button mAsteriskWellButton;

    /**
     * 清楚控件
     */
    private Button mDeleteButton;

    /**
     * 返回按钮
     */
    private ImageButton mBackButton;

    /**
     * 所属Activity
     */
    private TelephoneActivity mActivity;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (TelephoneActivity) activity;
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = null;
        if (Utils.isRound()) {
            view = inflater.inflate(R.layout.fragment_input_round, container, false);
        } else {
            view = inflater.inflate(R.layout.fragment_input, container, false);
        }
        initViews(view);
        return view;
    }

    private void initViews(View view) {

        mNumberInputTextView = (TextView) view.findViewById(R.id.number_input);

        mDeleteButton = (Button) view.findViewById(R.id.input_del);
        mDeleteButton.setTag(R.id.numbers_key, '-');

        if (Utils.isRound()) {
            mInputViewRound = (InputViewRound) view.findViewById(R.id.input_view);
            mInputViewRound.setOnClickListener(this);

            mZeroPlusButton = (Button) view.findViewById(R.id.input_zero_plus);
            mZeroPlusButton.setTag(R.id.numbers_key, '0');
            mZeroPlusButton.setText("+" + " 0");
            mZeroPlusButton.setOnClickListener(mInputViewRound);
            mZeroPlusButton.setOnLongClickListener(this);

            mAsteriskWellButton = (Button) view.findViewById(R.id.input_asterisk_well);
            mAsteriskWellButton.setTag(R.id.numbers_key, '#');
            mAsteriskWellButton.setText("#" + " *");
            mAsteriskWellButton.setOnClickListener(mInputViewRound);
            mAsteriskWellButton.setOnLongClickListener(this);

            mDeleteButton.setOnClickListener(mInputViewRound);
        } else {
            mInputView = (InputView) view.findViewById(R.id.input_view);
            mInputView.setOnClickListener(this);

            mDeleteButton.setOnClickListener(mInputView);
        }
        mDeleteButton.setOnLongClickListener(this);
        mBackButton = (ImageButton) view.findViewById(R.id.input_back);
        mBackButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mActivity.onBackPressed();
            }
        });

    }

    @Override
    public void onClick(char in) {
        inputPadChar(in);
    }

    @Override
    public boolean onLongClick(View view) {
        switch (view.getId()) {
        case R.id.dial_del:
            inputPadChar('c');
            break;
        case R.id.dial_zero_plus:
            inputPadChar('+');
            break;
        case R.id.dial_asterisk_well:
            inputPadChar('*');
            break;
        default:
            break;
        }
        return true;
    }

    /**
     * 拨号盘输入
     * 
     * @param in
     */
    private void inputPadChar(char in) {
        String next = "";
        String text = (String) mNumberInputTextView.getText();
        if (in == '-') {
            if (text != null && text.length() > 0) {
                next = text.substring(0, text.length() - 1);
            }
        } else if (in == 'c') {
            next = "";
        } else if (text == null || text.length() == 0) {
            next += in;
        } else {
            next = text + in;
        }
        mNumberInputTextView.setText(next);
        mNumberInputTextView.setEnabled(!TextUtils.isEmpty(mNumberInputTextView.getText().toString().trim()));
        if (mActivity != null) {
            mActivity.sendDTMF((byte) in);
        }
    }
}
