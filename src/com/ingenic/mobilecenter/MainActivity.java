/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  YuanBanglin(Ahlin) <banglin.yuan@ingenic.com>
 *
 *  Elf/IDWS Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.mobilecenter;

import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.iwds.widget.RightScrollView.OnRightScrollListener;
import com.ingenic.mobilecenter.call.DialActivity;
import com.ingenic.mobilecenter.call.PhoningFragment;
import com.ingenic.mobilecenter.call.TelephoneActivity;
import com.ingenic.mobilecenter.calllog.CallLogsActivity;
import com.ingenic.mobilecenter.contacts.ContactsActivity;
import com.ingenic.mobilecenter.sms.SMSListActivity;
import com.ingenic.mobilecenter.utils.Utils;

import android.bluetooth.BluetoothHeadsetClientCall;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

public class MainActivity extends RightScrollActivity implements OnRightScrollListener,
        OnClickListener {

    /**
     * 右滑控件
     */
//    private RightScrollView mRightScrollView;
    /**
     * 拨号button
     */
    private View dialView;
    /**
     * 联系人button
     */
    private View contactsView;
    /**
     * 通话记录button
     */
    private View callsView;
    /**
     * 短信button
     */
    private View SMSView;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
//        mRightScrollView = new RightScrollView(this);
//        mRightScrollView.setOnRightScrollListener(this);
        if (Utils.isRound()) {
            setContentView(R.layout.activity_main_round);
//            View.inflate(this, R.layout.activity_main_round, mRightScrollView);
        } else {
            setContentView(R.layout.activity_main);
//            View.inflate(this, R.layout.activity_main, mRightScrollView);
        }
//        setContentView(mRightScrollView);
//        initViews(mRightScrollView);
        initViews();
    }

    @Override
    protected void onStart() {
        super.onStart();
        overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_right_in, R.anim.slide_right_out);
    }

    /**
     * 右滑回调
     */
    @Override
    public void onRightScroll() {
        finish();
    }

    /**
     * 按钮点击回调
     */
    @Override
    public void onClick(View view) {
        
        switch (view.getId()) {
        case R.id.main_dial_btn:
            if(Utils.sCallState == BluetoothHeadsetClientCall.CALL_STATE_ACTIVE){
                Intent it = new Intent(MainActivity.this, TelephoneActivity.class);
                it.setAction(Utils.ACTION_DEFAULT);
                it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                it.putExtra(Utils.TELEPHONE_FRAGMENT, PhoningFragment.class.getName());
                it.putExtra(Utils.TELEPHONE_TAG, "phoning");
                it.putExtra(Utils.TELEPHONE_NUMBER, Utils.mCall.getNumber());
                startActivity(it);
            } else {
                Intent dialIntent = new Intent(this, DialActivity.class);
                startActivity(dialIntent);
            }
            break;
        case R.id.main_contacts_btn:
            Intent contactsIntent = new Intent(MainActivity.this, ContactsActivity.class);
            contactsIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(contactsIntent);
            break;
        case R.id.main_calls_btn:
            Intent callLogsIntent = new Intent(MainActivity.this, CallLogsActivity.class);
            callLogsIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(callLogsIntent);
            break;
        case R.id.main_sms_btn:
            Intent smsIntent = new Intent(MainActivity.this, SMSListActivity.class);
            smsIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(smsIntent);
            break;
        default:
            break;
        }
    }

    /**
     * 初始化控件
     * 
     * @param rightScrollView
     */
    private void initViews() {
        dialView = findViewById(R.id.main_dial_btn);
        dialView.setOnClickListener(this);
        contactsView = findViewById(R.id.main_contacts_btn);
        contactsView.setOnClickListener(this);
        callsView = findViewById(R.id.main_calls_btn);
        callsView.setOnClickListener(this);
        SMSView = findViewById(R.id.main_sms_btn);
        SMSView.setOnClickListener(this);
    }

}
