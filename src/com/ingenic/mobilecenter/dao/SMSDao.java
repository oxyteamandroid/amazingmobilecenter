/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/AmazingSms Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.mobilecenter.dao;

import java.util.ArrayList;
import java.util.List;

import com.ingenic.iwds.datatransactor.elf.SmsInfo;
import com.ingenic.mobilecenter.sms.SmsAddress;
import com.ingenic.mobilecenter.utils.Utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class SMSDao {

    /**
     * 实例
     */
    private static SMSDao sInstance;

    /**
     * 数据库操作对象
     */
    private SQLiteHelper mHelper;

    /**
     * 构造
     * 
     * @param context
     */
    private SMSDao(Context context) {
        mHelper = new SQLiteHelper(context);
    }

    /**
     * 初始实例
     * 
     * @param context
     * @return
     */
    public synchronized static SMSDao getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new SMSDao(context);
        }
        return sInstance;
    }

    /**
     * 插入短信
     * 
     * @param info
     * @return
     */
    public long insert(SmsInfo info) {
        ContentValues values = Utils.toValues(info);

        long result = -1;
        if (info != null) {
            synchronized (mHelper) {
                SQLiteDatabase db = mHelper.getWritableDatabase();
                result = db.insert(Utils.TABLE, null, values);
                db.close();
            }
        }

        return result;
    }

    /**
     * 查询短信List<SmsInfo>
     * 
     * @return
     */
    public List<SmsInfo> getSmssWithNumber(String address) {
        List<SmsInfo> infos = new ArrayList<SmsInfo>();
        synchronized (mHelper) {
            SQLiteDatabase db = mHelper.getReadableDatabase();
            Cursor c = db.query(Utils.TABLE, new String[] { SmsColumns.BODY, SmsColumns.DATE, SmsColumns.TYPE }, SmsColumns.ADDRESS + " = ? ",
                    new String[] { address }, null, null, SmsColumns.DATE + " ASC");
            if (c == null)
                return null;

            if (c.moveToFirst()) {
                do {
                    SmsInfo info = new SmsInfo();
                    info.setBody(c.getString(0));
                    info.setDate(c.getLong(1));
                    info.setType(c.getInt(2));
                    infos.add(info);
                } while (c.moveToNext());
            }

            c.close();
            db.close();
        }
        return infos;
    }

    /**
     * 查询短信List<SmsAddress>
     * 
     * @return
     */
    public List<SmsAddress> getNumbers() {
        List<SmsAddress> addresses = new ArrayList<SmsAddress>();
        synchronized (mHelper) {
            SQLiteDatabase db = mHelper.getReadableDatabase();
            Cursor c = db.query(true, Utils.TABLE, new String[] { SmsColumns.ADDRESS }, null, null, null, null, SmsColumns.DATE + " DESC", null);
            if (c == null)
                return null;

            if (c.moveToFirst()) {
                do {
                    String addr = c.getString(0);
                    if (addr == null)
                        continue;

                    SmsAddress address = new SmsAddress(addr);
                    Cursor cc = db.query(Utils.TABLE, new String[] { SmsColumns.PERSON, SmsColumns.BODY, SmsColumns.DATE, SmsColumns.READ },
                            SmsColumns.ADDRESS + " = ? ", new String[] { addr }, null, null, SmsColumns.DATE + " DESC");
                    if (cc == null)
                        continue;

                    address.smsCount = cc.getCount();
                    if (cc.moveToFirst()) {
                        address.name = cc.getString(0);
                        address.lastSms = cc.getString(1);
                        address.date = cc.getLong(2);
                        address.hasUnread = cc.getInt(3) == 0;
                        addresses.add(address);
                    }
                    cc.close();
                } while (c.moveToNext());
            }

            c.close();
            db.close();
        }
        return addresses;
    }

    /**
     * 读短信
     * 
     * @param address
     * @return
     */
    public int readSms(String address) {
        ContentValues values = new ContentValues();
        values.put(SmsColumns.READ, 1);

        int result = -1;
        synchronized (mHelper) {
            SQLiteDatabase db = mHelper.getWritableDatabase();
            result = db.update(Utils.TABLE, values, SmsColumns.ADDRESS + " = ? ", new String[] { address });
            db.close();
        }
        return result;
    }

    /**
     * 根据ID删除短信
     * 
     * @param id
     * @return
     */
    public int delete(int id) {
        int result = -1;
        synchronized (mHelper) {
            SQLiteDatabase db = mHelper.getWritableDatabase();
            result = db.delete(Utils.TABLE, SmsColumns._ID + " = ? ", new String[] { "" + id });
            db.close();
        }
        return result;
    }

    /**
     * 删除指定短信
     * 
     * @param address
     * @return
     */
    public int deleteByAddress(String address) {
        int result = -1;
        synchronized (mHelper) {
            SQLiteDatabase db = mHelper.getWritableDatabase();
            result = db.delete(Utils.TABLE, SmsColumns.ADDRESS + " = ? ", new String[] { address });
            db.close();
        }
        return result;
    }

    /**
     * 删除所有短信
     * 
     * @return
     */
    public int deleteAll() {
        int result = -1;
        synchronized (mHelper) {
            SQLiteDatabase db = mHelper.getWritableDatabase();
            result = db.delete(Utils.TABLE, null, null);
            db.close();
        }
        return result;
    }
}