/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  YuanBanglin(Ahlin) <banglin.yuan@ingenic.com>
 *
 *  Elf/IDWS Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.mobilecenter;

import com.ingenic.mobilecenter.call.IncomingRemindService;
import com.ingenic.mobilecenter.call.WatchPhoneService;
import com.ingenic.mobilecenter.calllog.CalllogService;
import com.ingenic.mobilecenter.contacts.ContactsSyncService;
import com.ingenic.mobilecenter.sms.WatchSMSService;
import com.ingenic.mobilecenter.utils.Utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context comtext, Intent intent) {
        // 接收开机广播|重启服务广播，重启服务广播
        if (Utils.RESTART_SERVICE.equals(intent.getAction()) || Utils.CLEAR_DATA.equals(intent.getAction())) {
            comtext.startService(new Intent(comtext, WatchPhoneService.class));
            comtext.startService(new Intent(comtext, IncomingRemindService.class));
            comtext.startService(new Intent(comtext, WatchSMSService.class));
            comtext.startService(new Intent(comtext, ContactsSyncService.class));
            comtext.startService(new Intent(comtext, CalllogService.class));
        }
    }
}
