/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  YuanBanglin(Ahlin) <banglin.yuan@ingenic.com>
 *
 *  Elf/IDWS Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.mobilecenter;

import java.util.ArrayList;
import java.util.List;

import com.ingenic.iwds.HardwareList;
import com.ingenic.iwds.datatransactor.elf.PhoneStateTransactionModel;
import com.ingenic.iwds.datatransactor.elf.SmsTransactionModel;
import com.ingenic.iwds.datatransactor.elf.PhoneStateTransactionModel.PhoneStateCallback;
import com.ingenic.iwds.datatransactor.elf.SmsTransactionModel.SmsTransactionCallback;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.mobilecenter.call.IncomingRemindService;
import com.ingenic.mobilecenter.call.WatchPhoneService;
import com.ingenic.mobilecenter.calllog.CalllogService;
import com.ingenic.mobilecenter.contacts.ContactsSyncService;
import com.ingenic.mobilecenter.contacts.ContactsUtils;
import com.ingenic.mobilecenter.sms.WatchSMSService;
import com.ingenic.mobilecenter.utils.ContactUtils;
import com.ingenic.mobilecenter.utils.Utils;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.WindowManager;

public class MobileCenterApplication extends Application {

    public static class AppCtx {

        public static MobileCenterApplication application;

        public static String getPackageName() {
            if (application == null)
                return null;
            return application.getPackageName();
        }

        public static ContentResolver getContentResolver() {
            if (application == null)
                return null;
            return application.getContentResolver();
        }

        public static void sendBroadcast(Intent intent) {
            if (application == null)
                return;
            application.sendBroadcast(intent);
        }

        public static String getString(int res) {
            if (application == null)
                return "";
            return application.getString(res);
        }
    }

    /**
     * 来电提醒Model
     */
    private static PhoneStateTransactionModel sPhoneModel;

    /**
     * 短信Model
     */
    private static SmsTransactionModel sSmsModel;

    /**
     * 时间改变监听者
     */
    private List<TimeChangeListener> listeners = new ArrayList<TimeChangeListener>();

    @Override
    public void onCreate() {
        super.onCreate();
        AppCtx.application = this;

        ContactUtils.init();

        int syncState = ContactsUtils.getContactsSyncstate(this);
        IwdsLog.i(this, "syncState:" + syncState);
        if (syncState == 0)
            ContactUtils.query();

        startService(new Intent(this, WatchPhoneService.class));
        startService(new Intent(this, IncomingRemindService.class));
        startService(new Intent(this, WatchSMSService.class));
        startService(new Intent(this, ContactsSyncService.class));
        startService(new Intent(this, CalllogService.class));
        Utils.setIsRound(HardwareList.IsCircularScreen());
        getXY();

        // 注册时间改变广播接收器
        IntentFilter IF = new IntentFilter();
        IF.addAction(Intent.ACTION_TIME_TICK);
        registerReceiver(timeReceiver, IF);

        IntentFilter filter = new IntentFilter();
        filter.addAction(ContactsUtils.ACTION_CONTACTS_SYNC_FINISHED);
        registerReceiver(mReceiver, filter);
    }

    /**
     * 第一次绑定手表同步数据完成广播
     */
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            IwdsLog.i(this, "Contacts sync finished");
            ContactUtils.query();
        }
    };

    /**
     * 时间改变广播接收器
     */
    private BroadcastReceiver timeReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            for (TimeChangeListener listener : listeners) {
                listener.onTimeChange();
            }
        }
    };

    /**
     * 注册时间改变监听
     * 
     * @param listener
     */
    public void registerListener(TimeChangeListener listener) {
        if (!listeners.contains(listener)) {
            synchronized (listeners) {
                if (!listeners.contains(listener))
                    listeners.add(listener);
            }
        }
    }

    /**
     * 反注册时间改变监听
     * 
     * @param listener
     */
    public void unregisterListener(TimeChangeListener listener) {
        if (listeners.contains(listener)) {
            synchronized (listeners) {
                if (listeners.contains(listener))
                    listeners.remove(listener);
            }
        }
    }

    /**
     * 获取来电提醒Model
     * 
     * @param psc
     * @return
     */
    public PhoneStateTransactionModel getPhoneModel(PhoneStateCallback psc) {
        if (sPhoneModel == null) {
            sPhoneModel = new PhoneStateTransactionModel(this, psc, Utils.UUID_PHONE);
        }
        return sPhoneModel;
    }

    /**
     * 获取屏幕 宽 高
     */
    @SuppressWarnings("deprecation")
    private void getXY() {
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Utils.sScreenWidth = wm.getDefaultDisplay().getWidth();
        Utils.sScreenHeight = wm.getDefaultDisplay().getHeight();
    }

    /**
     * 获取短信Model
     * 
     * @param stc
     * @return
     */
    public SmsTransactionModel getSmsModel(SmsTransactionCallback stc) {
        if (sSmsModel == null) {
            sSmsModel = new SmsTransactionModel(this, stc, Utils.UUID_SMS);
        }
        return sSmsModel;
    }
}
