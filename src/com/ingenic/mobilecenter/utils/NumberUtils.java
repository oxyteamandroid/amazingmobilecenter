package com.ingenic.mobilecenter.utils;

import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.mobilecenter.R;

import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;

public class NumberUtils {

    private static final String TAG = "NumberUtils";

    private static PhoneType mType = PhoneType.MOBILE;

    public static PhoneType getNumberType(String number) {
        if (number == null) {
            return PhoneType.PUBLIC;
        }
        if (number.startsWith("+")) {
            return PhoneType.INTERNATIONAL;
        } else if (number.length() == 11 && (number.startsWith("1"))) {
            return PhoneType.MOBILE;
        } else if (number.length() == 7 || number.length() == 8 || number.startsWith("0")) {
            return PhoneType.PHONE;
        } else {
            return PhoneType.PUBLIC;
        }
    }

    private static String configNumber(String num) {
        if (num == null) {
            return num;
        }
        String number = num.replaceAll("[^0-9]", "");
        switch (mType) {
        case PHONE:
            if (number.length() > 8) {
                if (number.startsWith("01") || number.startsWith("02")) {
                    number = number.substring(3);
                } else {
                    number = number.substring(4);
                }
            }
            break;
        case INTERNATIONAL:
            if (number.length() > 11) {
                number = number.substring(number.length() - 11);
            }
            break;
        case MOBILE:
        case PUBLIC:
        default:
            break;
        }
        return number;
    }

    public static String queryName(Context context, String number_) {
        mType = getNumberType(number_);
        String number = configNumber(number_);
        if (number == null) {
            return context.getResources().getString(R.string.unknow);
        }
        IwdsLog.d(TAG, "getName:" + number);
        String[] projection = { ContactsContract.CommonDataKinds.Phone.CONTACT_ID, ContactsContract.CommonDataKinds.Phone.NUMBER };
        String[] projection2 = { ContactsContract.Contacts._ID, ContactsContract.Contacts.DISPLAY_NAME };
        StringBuffer sb = new StringBuffer(number);
        if (number.length() > 4) {
            for (int i = number.length() - 4; i > 0; i--) {
                sb = sb.insert(i, "%");
            }
            if (mType == PhoneType.PHONE || mType == PhoneType.INTERNATIONAL) {
                sb = sb.insert(0, "%");
            }
        }
        IwdsLog.d(TAG, "number:" + number + "====sb:" + sb.toString());
        Cursor cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection,
                " data1 like '" + sb.toString() + "'", null, null);

        if (cursor == null) {
            IwdsLog.e(TAG, "getContactId null");
            return null;
        }
        int contactId = -1;
        if (cursor.moveToFirst()) {
            do {
                String num = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                num = num.replaceAll("[^0-9]", "");
                IwdsLog.i(TAG, "num:" + num);
                if (num.equals(number)) {
                    int columnIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID);
                    contactId = cursor.getInt(columnIndex);
                    break;
                } else if (mType == PhoneType.PHONE || mType == PhoneType.INTERNATIONAL) {
                    if ((num.length() > number.length() && num.endsWith(number)) || (number.length() > num.length() && number.endsWith(num))) {
                        int columnIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID);
                        contactId = cursor.getInt(columnIndex);
                        break;
                    }
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        if (contactId == -1) {
            IwdsLog.e(TAG, "id not got");
            return null;
        }
        Cursor cursor2 = context.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, projection2, " _id = '" + contactId + "'", null,
                null);
        if (cursor2 == null) {
            IwdsLog.e(TAG, "getName null");
            return null;
        }
        String name = null;
        if (cursor2.moveToFirst()) {
            int columnIndex = cursor2.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
            name = cursor2.getString(columnIndex);
        }
        cursor2.close();
        return name;
    }

    public enum PhoneType {
        PHONE, MOBILE, PUBLIC, INTERNATIONAL
    }
}
