/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  YuanBanglin(Ahlin) <banglin.yuan@ingenic.com>
 *
 *  Elf/IDWS Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.mobilecenter.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.ingenic.iwds.datatransactor.elf.SmsInfo;
import com.ingenic.mobilecenter.dao.SmsColumns;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothHeadsetClient;
import android.bluetooth.BluetoothHeadsetClientCall;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;

public class Utils {

    public static final String UUID_PHONE = "3e164e8e-57c9-0e70-5635-04059ed9c5ee";

    /**
     * SharedPreferences存储key
     */
    public static final String PREFERENCE_BOND = "bond_preference";

    /**
     * 存储通道是否可用的key
     */
    public static final String AVAILABLE_SYNC = "available";

    /**
     * 通道连接设备地址key
     */
    public static final String AVAILABLE_CONNECTION = "available_connection";

    /**
     * 蓝牙耳机连接状态的key
     */
    public static final String BLUETOOTH_HEADSET_CONNECTION_STATE = "bluetooth_headset_connection_state";

    /**
     * 蓝牙耳机连接设备地址key
     */
    public static final String BLUETOOTH_HEADSET_CONNECTION_ADDRESS = "bluetooth_headset_connection_address";

    /**
     * 通话状态fragment切换是参数key
     */
    public static final String TELEPHONE_FRAGMENT = "telephone_fragment";
    public static final String TELEPHONE_TAG = "telephone_tag";
    public static final String TELEPHONE_CAN_BACK = "telephone_can_back";
    public static final String TELEPHONE_NAME = "telephone_name";
    public static final String TELEPHONE_NUMBER = "telephone_number";

    /**
     * 短信参数传递Key
     */
    public static final String SMS_NUMBER = "address";

    /**
     * 通话界面跳转ACTION
     */
    public static final String ACTION_DEFAULT = "com.ingenic.mobilecenter.ACTION_DEFAULT";
    public static final String ACTION_OUT_CALL = "com.ingenic.mobilecenter.ACTION_OUT_CALL";
    public static final String ACTION_INCOMING_CALL = "com.ingenic.mobilecenter.ACTION_INCOMING_CALL";
    public static final String ACTION_INCOMING_REMIND = "com.ingenic.mobilecenter.ACTION_INCOMING_REMIND";

    /**
     * 开机广播ACTION
     */
    public static final String BOOT_COMPLETED = "android.intent.action.BOOT_COMPLETED";

    /**
     * 重启服务ACTION
     */
    public static final String RESTART_SERVICE = "android.intent.action.RESTART_SERVICE";

    /**
     * 清除数据ACTION
     */
    public static final String CLEAR_DATA = "ingenic.intent.action.com.ingenic.mobilecenter";

    /**
     * 发送短信ACTION
     */
    public static final String INENT_SEND_MESSAGE_REQUEST = "com.ingenic.amazingSms.send";

    /**
     * 是否是圆屏
     */
    public static boolean sIsRound = false;

    /**
     * 屏幕宽度
     */
    public static int sScreenWidth;

    /**
     * 屏幕高度
     */
    public static int sScreenHeight;

    /**
     * 正在显示的Fragment
     */
    public static String sFragmentName;

    /**
     * 是否正在通话
     */
    public static boolean sIsConversation = false;

    /**
     * 通话状态，默认状态 终止
     */
    public static int sCallState = BluetoothHeadsetClientCall.CALL_STATE_TERMINATED;

    /**
     * 正在进行的通话
     */
    public static BluetoothHeadsetClientCall mCall;

    /**
     * 判断是否是圆屏
     * 
     * @return
     */
    public static boolean isRound() {
        return sIsRound;
    }

    /**
     * 设置是否圆屏
     * 
     * @param sIsRound
     */
    public static void setIsRound(boolean sIsRound) {
        Utils.sIsRound = sIsRound;
    }

    /**
     * 获取来电提醒通道是否可用
     * 
     * @param context
     * @return
     */
    public static boolean isAvailable(Context context) {
        SharedPreferences sp = context.getSharedPreferences(PREFERENCE_BOND, Context.MODE_PRIVATE);
        return sp.getBoolean(AVAILABLE_SYNC, false);
    }

    /**
     * 获取通道连接设备地址
     * 
     * @param context
     * @return
     */
    public static String getAvailableConnection(Context context) {
        SharedPreferences sp = context.getSharedPreferences(PREFERENCE_BOND, Context.MODE_PRIVATE);
        String address = sp.getString(AVAILABLE_CONNECTION, "");
        return address;
    }

    /**
     * 获取蓝牙耳机是否连接
     * 
     * @param context
     * @return
     */
    public static boolean isBtConnected(Context context) {
        SharedPreferences sp = context.getSharedPreferences(Utils.PREFERENCE_BOND, Context.MODE_PRIVATE);
        int state = sp.getInt(BLUETOOTH_HEADSET_CONNECTION_STATE, -1);
        return state == BluetoothHeadsetClient.STATE_CONNECTED;
    }

    /**
     * 获取蓝牙耳机连接设备地址
     * 
     * @param context
     * @return
     */
    public static String getBtConnectedAddress(Context context) {
        SharedPreferences sp = context.getSharedPreferences(Utils.PREFERENCE_BOND, Context.MODE_PRIVATE);
        String address = sp.getString(BLUETOOTH_HEADSET_CONNECTION_ADDRESS, "");
        return address;
    }

    /**
     * 时间转换
     * 
     * @param time
     * @return
     */
    @SuppressLint("SimpleDateFormat")
    public static String transformationTime(long time) {
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
        Date date = new Date(time);
        return formatter.format(date);
    }

    /**
     * 获取系统当前时间
     * 
     * @return
     */
    public static String getTime() {
        return transformationTime(System.currentTimeMillis());
    }

    /**
     * 格式化计时
     * 
     * @param time
     * @return
     */
    public static String formatTime(int time) {
        if (time < 0) {
            time = 0;
        }
        int h = time / 3600;
        int m = time / 60;
        int s = time % 60;
        return h == 0 ? (m < 10 ? "0" + m : m) + ":" + (s < 10 ? "0" + s : s) : (h < 10 ? "0" + h : h) + ":" + (m < 10 ? "0" + m : m) + ":"
                + (s < 10 ? "0" + s : s);
    }

    public static final String UUID_SMS = "fcb3bc51-f75a-eb06-ee1a-2907f966e1a5";

    /**
     * 一天毫秒数
     */
    private static final long ONE_DAY_MILLS = 24 * 60 * 60 * 1000;

    /**
     * 一分钟毫秒数
     */
    private static final long ONE_MINUTE_MILLS = 60 * 1000;

    /**
     * 数据库表名
     */
    public static final String TABLE = "sms";

    /**
     * 短信通道是否可用的key
     */
    public static final String AVAILABLE_SMS = "available_sms";

    public static ContentValues toValues(SmsInfo info) {
        if (info == null)
            return null;

        ContentValues values = new ContentValues();
        values.put(SmsColumns._ID, info.getId());
        values.put(SmsColumns.THREAD_ID, info.getThreadId());
        values.put(SmsColumns.ADDRESS, info.getAddress());
        values.put(SmsColumns.PERSON, info.getPerson());
        values.put(SmsColumns.BODY, info.getBody());
        values.put(SmsColumns.TYPE, info.getType());
        values.put(SmsColumns.READ, info.getRead());
        values.put(SmsColumns.PROTOCOL, info.getProtocol());
        values.put(SmsColumns.DATE, info.getDate());
        return values;
    }

    /**
     * 判断time是否是今天
     * 
     * @param time
     * @return
     */
    public static boolean isToday(long time) {
        return time >= getDayMinTimeMillis(0) && time <= getDayMaxTimeMillis(0);
    }

    /**
     * 判断time是否是昨天
     * 
     * @param time
     * @return
     */
    public static boolean isYesterday(long time) {
        return time >= getDayMinTimeMillis(-1) && time <= getDayMaxTimeMillis(-1);
    }

    /**
     * 获取某天开始的时间戳
     * 
     * @param dayDelta
     *            某天到今天的天数 （如昨天 -1）
     * @return
     */
    private static long getDayMinTimeMillis(int dayDelta) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(System.currentTimeMillis());

        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        c.set(year, month, day, 0, 0, 0);
        long min = c.getTimeInMillis() + ONE_DAY_MILLS * dayDelta;

        return min;
    }

    /**
     * 获取某天结束的时间戳
     * 
     * @param dayDelta
     *            某天到今天的天数 （如昨天 -1）
     * @return
     */
    private static long getDayMaxTimeMillis(int dayDelta) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(System.currentTimeMillis());

        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        c.set(year, month, day, 23, 59, 59);
        long max = c.getTimeInMillis() + ONE_DAY_MILLS * dayDelta;

        return max;
    }

    /**
     * 比较两个时间是否在60秒之内
     * 
     * @param time1
     * @param time2
     * @return
     */
    public static boolean less1Min(long time1, long time2) {
        return Math.abs(time1 - time2) <= ONE_MINUTE_MILLS;
    }

    /**
     * 获取短信通道是否可用
     * 
     * @param context
     * @return
     */
    public static boolean isAvailableSms(Context context) {
        SharedPreferences sp = context.getSharedPreferences(PREFERENCE_BOND, Context.MODE_PRIVATE);
        return sp.getBoolean(AVAILABLE_SMS, false);
    }
}
