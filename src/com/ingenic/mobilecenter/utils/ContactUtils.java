package com.ingenic.mobilecenter.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.CallLog.Calls;
import android.provider.ContactsContract.CommonDataKinds.Phone;

import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.mobilecenter.R;
import com.ingenic.mobilecenter.calllog.CallLog;
import com.ingenic.mobilecenter.calllog.CallLog.Log;
import com.ingenic.mobilecenter.contacts.Contact;

import static com.ingenic.mobilecenter.MobileCenterApplication.AppCtx;

/**
 * 通讯录工具类
 * 
 * @author hjt
 * 
 */
public class ContactUtils {

    private static final String TAG = "ContactUtils";

    /**
     * 联系人初始化完毕，可以获取了
     */
    public static final String ACTION_CONTACTS_COMPLETE = AppCtx.getPackageName() + ".ContactsComplete";

    /**
     * 通话记录初始化完毕，可以获取了
     */
    public static final String ACTION_LOGS_COMPLETE = AppCtx.getPackageName() + ".LogsComplete";

    /**
     * 获取到的联系人对象缓存
     */
    private static List<Contact> CONTACTS = new ArrayList<Contact>();
    /**
     * 通话记录缓存对象
     */
    private static List<CallLog> CALL_LOGS = new ArrayList<CallLog>();

    /**
     * 当前是否正在查询
     */
    private static boolean Quering = false;
    
    /**
     * 是否再次查询
     */
    private static boolean isQuery = false;
    /**
     * 在程序开始的时候调用，初始化操作，设置数据库改变监听之类的
     */
    public static void init() {
        Quering = false;
        ContentResolver cr = AppCtx.getContentResolver();
        ContentObserver co = new ContentObserver(new Handler()) {

            @Override
            public void onChange(boolean selfChange) {
                // 清空缓存数据重新查询
                CALL_LOGS.clear();
                CONTACTS.clear();
                query(); // 重新查询一遍数据
                super.onChange(selfChange);
            }
        }; // 通讯录，通话记录监听，一项改变刷新所有一遍
        cr.registerContentObserver(Phone.CONTENT_URI, true, co); // 观察通讯录
        cr.registerContentObserver(Calls.CONTENT_URI, true, co); // 观察通话记录
    }

    /**
     * 提前进行此操作可以减少主页面显示等待时间,进行查询数据：联系人和通话记录获取，获取完成后将会广播通知,
     * {@link #ACTION_CONTACTS_COMPLETE}和{@link #ACTION_LOGS_COMPLETE}
     */
    public static void query() {
        if (!CALL_LOGS.isEmpty() && !CONTACTS.isEmpty()) // 两个缓存数据都不为空，不查询
            return;
        if (Quering){ // 正在查询中
            isQuery = true;
            return;
        }
        isQuery = false;
        Quering = true;
        new Thread() {

            @SuppressLint("SimpleDateFormat")
            @Override
            public void run() {
                // 先查询联系人,后获取通话记录
                if (CALL_LOGS.isEmpty()) // 通话记录为空
                    queryCallLogs();
                boolean contactsEmpty = CONTACTS.isEmpty();
                if (contactsEmpty) // 通讯录为空
                    queryContacts();

                // 填充通话记录联系人的名字
                int logSize = CALL_LOGS.size(), contactSize = CONTACTS.size();
                for (int i = 0; i < logSize; i++) { // 外循环通话记录
                    CallLog log = CALL_LOGS.get(i);
                    for (int j = 0; j < contactSize; j++) { // 内循环联系人
                        boolean isHave = false;
                        Contact contact = CONTACTS.get(j);
                        List<com.ingenic.mobilecenter.contacts.Contact.Phone> phones = contact.getPhones();
                        for (int k = 0; k < phones.size(); k++) {// 内循环联系人的所有号码
                            com.ingenic.mobilecenter.contacts.Contact.Phone phone = phones.get(k);
                            if (phone.getNumber().contains(log.getPhone())) { // 此联系人存在这个通话记录号码
                                log.setName(contact.getName());
                                log.setNumType(phone.getType());
                                isHave = true;
                                break; // 遍历下一个通话记录
                            }
                        }
                        if(isHave)
                            break;
                    }
                    IwdsLog.i(TAG, "Call Logs"+log);
                }
                AppCtx.sendBroadcast(new Intent(ACTION_LOGS_COMPLETE)); // 通话记录刷新

                AppCtx.sendBroadcast(new Intent(ACTION_CONTACTS_COMPLETE)); // 通讯录数据刷新

                Quering = false; // 查询结束

                if (CALL_LOGS.isEmpty() || CONTACTS.isEmpty()) { // 其中有一种没有数据
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                    }
                    query(); // 等待一S再次获取数据,确保在第一次调用，用户一定需要手动授权操作
                }
                
                if(isQuery){
                    CALL_LOGS.clear();
                    CONTACTS.clear();
                    query();
                }
                super.run();
            }
        }.start();
    }

    /**
     * 查询通话记录
     */
    @SuppressLint("SimpleDateFormat")
    private static void queryCallLogs() {
        IwdsLog.e(TAG, "CallLogs count");
        // 后获取通话记录，需要匹配通讯录名字
        Cursor logCursor = AppCtx.getContentResolver().query(Calls.CONTENT_URI, new String[] { Calls.NUMBER, Calls.TYPE, Calls.DATE ,Calls.DURATION},
                Calls.NUMBER + "!='-1' AND " + Calls.NUMBER + "!=''", null, Calls.DATE + " DESC LIMIT 300"); // 查询最近三百条有效数据
        List<CallLog> logs = new ArrayList<CallLog>();
        if (logCursor != null) {
            IwdsLog.e(TAG, "queryCallLogs:" + logCursor.getCount());
            while (logCursor.moveToNext()) {
                String phone = logCursor.getString(logCursor.getColumnIndex(Calls.NUMBER)); // 号码
                int type = logCursor.getInt(logCursor.getColumnIndex(Calls.TYPE)); // 呼叫类型
                int size = logs.size(); // 获取当前size大小
                phone = filterPhone(phone);
                Log log = new Log();
                long timeMS = logCursor.getLong(logCursor.getColumnIndex(Calls.DATE)); // 时间毫秒数
                int duration = logCursor.getInt(logCursor.getColumnIndex(Calls.DURATION));
                // 解析时间
                String time = ""; // 时间
                SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                int pass = Integer.valueOf(sdf.format(new Date())) - Integer.valueOf(sdf.format(new Date(timeMS)));
                if (pass == 0) { // 今天
                    time = new SimpleDateFormat("HH:mm").format(new Date(timeMS));
                } else if (pass >= 1 && pass <= 2) { // 昨天之内
                    time = AppCtx.getString(R.string.yesterday) + " " + new SimpleDateFormat("HH:mm").format(new Date(timeMS));
                } else { // 其它时间
                    time = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date(timeMS));
                }
                log.setTime(time);
                log.setDuration(duration);
                if (size > 0 && logs.get(size - 1).getPhone().equals(phone) && logs.get(size - 1).getType() == type) { // 存在Phone相等、type相等，并且邻近
                    CallLog callLog = logs.get(size - 1);
                    callLog.getLogs().add(log);
                } else {
                    List<Log> log_s = new ArrayList<Log>();
                    log_s.add(log);
                    logs.add(new CallLog("", phone, log_s, type));
                }
                IwdsLog.d("queryCallLogs", "CallLog: phone:" + phone + ",type:" + type);
            }
            logCursor.close();
        }
        CALL_LOGS = logs; // 直接覆盖
        // AppCtx.sendBroadcast(new Intent(ACTION_LOGS_COMPLETE)); //
        // 发送通话记录获取完成通知
    }

    /**
     * 查询联系人
     */
    private static void queryContacts() {
        // 先获取联系人，后获取通话记录
        Cursor contactCursor = AppCtx.getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                new String[] { ContactsContract.CommonDataKinds.Phone.CONTACT_ID, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                        ContactsContract.CommonDataKinds.Phone.DATA1, ContactsContract.CommonDataKinds.Phone.TYPE }, null, null,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
        List<Contact> contacts = new ArrayList<Contact>();
        IwdsLog.e(TAG, "contactCursor:" + contactCursor.getCount());
        if (contactCursor != null) {
            while (contactCursor.moveToNext()) {
                // 获取数据
                String name = contactCursor.getString(contactCursor.getColumnIndex(Phone.DISPLAY_NAME)); // 通讯录姓名
                String id = contactCursor.getString(contactCursor.getColumnIndex(Phone.CONTACT_ID)); // 获取通讯录ID
                String phone = contactCursor.getString(contactCursor.getColumnIndex(Phone.DATA1));
                int phoneType = contactCursor.getInt(contactCursor.getColumnIndex(Phone.TYPE));
                Contact.Phone phone_ = new Contact.Phone();
                IwdsLog.d(TAG, "Contacts:  id:" + id + ",name:" + name + ",phone:" + phone + ",phoneType:" + phoneType);
                phone = filterPhone(phone);

                boolean have = false; // 不存在
                int size = contacts.size();
                for (int i = 0; i < size; i++) {
                    Contact contact = contacts.get(i);
                    if (contact.getId().equals(id)) { // 已经存在
                        phone_.setNumber(phone);
                        phone_.setType(phoneType);
                        contact.getPhones().add(phone_);
                        have = true; // 存在
                        break; // 跳出
                    }
                }

                if (!have) { // 不存在
                    List<com.ingenic.mobilecenter.contacts.Contact.Phone> phones = new ArrayList<com.ingenic.mobilecenter.contacts.Contact.Phone>(); // 确保不会出现null情况
                    phone_.setNumber(phone);
                    phone_.setType(phoneType);
                    phones.add(phone_);
                    contacts.add(new Contact(id, name, phones)); // 添加联系人
                }
            }
            contactCursor.close();
        }
        // Collections.sort(contacts, ContactComparator); // 进行排序一遍
        CONTACTS = contacts; // 直接替换掉
//        AppCtx.sendBroadcast(new Intent(ACTION_CONTACTS_COMPLETE)); // 发送通讯录获取完成通知
    }

    /**
     * 通讯录排序
     */
    // private static Comparator<Contact> ContactComparator = new
    // Comparator<Contact>() {
    //
    // private char start = SideBar.KEYS[0]; // 开始字符
    // private char end = SideBar.KEYS[SideBar.KEYS.length - 1]; // 结束字符
    //
    // @Override
    // public int compare(Contact lhs, Contact rhs) {
    // if (lhs.getSort() == start || rhs.getSort() == end)
    // return -1;
    // else if (lhs.getSort() == end || rhs.getSort() == start)
    // return 1;
    // else
    // return lhs.getSort() - rhs.getSort();
    // }
    // };

    /**
     * 过滤电话号码中的特殊部分
     * 
     * @param phone
     *            电话号码
     * @return 过滤后的电话号码
     */
    private static String filterPhone(String phone) {
        if (phone != null) {
            phone = phone.replaceAll(" ", "");
            phone = phone.replaceAll("-", "");
            phone = phone.replace("+86", "");
        }
        return phone;
    }

    /**
     * 获取缓存的联系人列表，只可复制，不用于直接UI操作
     * 
     * @return 联系人列表
     */
    public static List<Contact> loadContacts() {
        return CONTACTS;
    }

    /**
     * 获取缓存的通话记录列表，只可复制，不用于直接UI操作
     * 
     * @return 通话记录列表
     */
    public static List<CallLog> loadCallLogs() {
        return CALL_LOGS;
    }

    /**
     * 获取是否正在查询
     * 
     * @return 是否正在查询
     */
    public static boolean getQuering() {
        return Quering;
    }
}
