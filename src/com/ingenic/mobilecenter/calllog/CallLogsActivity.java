package com.ingenic.mobilecenter.calllog;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ingenic.iwds.app.AmazingDialog;
import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.mobilecenter.MobileCenterApplication;
import com.ingenic.mobilecenter.R;
import com.ingenic.mobilecenter.TimeChangeListener;
import com.ingenic.mobilecenter.call.CallFargment;
import com.ingenic.mobilecenter.call.TelephoneActivity;
import com.ingenic.mobilecenter.contacts.ContactsUtils;
import com.ingenic.mobilecenter.utils.ContactUtils;
import com.ingenic.mobilecenter.utils.Utils;

public class CallLogsActivity extends RightScrollActivity implements TimeChangeListener, OnItemClickListener {

    /**
     * 通话记录列表控件
     */
    private ListView mCallLogsListView;

    /**
     * 适配圆屏
     */
    private LinearLayout headerLayout;

    /**
     * list空提示控件
     */
    private TextView mEmptyTextView;

    /**
     * 时间控件
     */
    private TextView mTimeTextView;

    /**
     * back控件
     */
    private TextView mBackTextView;

    /**
     * 通话记录列表适配器
     */
    private CallLogsAdapter mLogsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Utils.isRound()) {
            setContentView(R.layout.activity_calllogs_round);
        } else {
            setContentView(R.layout.activity_calllogs);
        }

        initViews();
        
        ((MobileCenterApplication) getApplication()).registerListener(this);
        
        int state = ContactsUtils.getContactsSyncstate(this);
        if (state == 0) {// 同步完成
            if (!ContactUtils.getQuering()) {
                mLogsAdapter.addAll(ContactUtils.loadCallLogs());
                mLogsAdapter.notifyDataSetChanged();
            } else {
                mEmptyTextView.setText(R.string.load);
            }
        } else if (state == 2) {// 同步失败
            mEmptyTextView.setText(R.string.sync_failure);
        } else {
            mEmptyTextView.setText(R.string.are_sync);
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(ContactsUtils.ACTION_CONTACTS_SYNC_FAILED);
        filter.addAction(ContactUtils.ACTION_LOGS_COMPLETE);
        registerReceiver(mReceiver, filter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
    }

    /**
     * 初始化控件
     */
    private void initViews() {
        mCallLogsListView = (ListView) findViewById(R.id.calllogs_list);
        mCallLogsListView.setEmptyView(findViewById(android.R.id.empty));
        if (Utils.isRound()) {
            mCallLogsListView.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {
                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    for (int i = 0; i < mCallLogsListView.getChildCount(); i++) {
                        mCallLogsListView.getChildAt(i).invalidate();
                    }
                }
            });
            headerLayout = (LinearLayout) findViewById(R.id.header);
            headerLayout.getLayoutParams().height = Utils.sScreenHeight / 3;
            mCallLogsListView.setDivider(new ColorDrawable(getResources().getColor(R.color.color_grey_text)));
            mCallLogsListView.setDividerHeight(1);
            mCallLogsListView.setSelector(R.drawable.btn_activity_main_round);
            mLogsAdapter = new CallLogsAdapter(this, R.layout.item_calllog_round);

            TextView headerView = new TextView(this);
            headerView.setHeight(Utils.sScreenHeight / 3);
            mCallLogsListView.addHeaderView(headerView);
            TextView footerView = new TextView(this);
            footerView.setHeight(Utils.sScreenHeight / 3);
            mCallLogsListView.addFooterView(footerView);
        } else {
            mCallLogsListView.setDividerHeight(10);
            mCallLogsListView.setSelector(R.drawable.btn_round_angle);
            mLogsAdapter = new CallLogsAdapter(this, R.layout.item_calllog);
        }
        mEmptyTextView = (TextView) findViewById(android.R.id.empty);

        mCallLogsListView.setAdapter(mLogsAdapter);
        mCallLogsListView.setOnItemClickListener(this);

        mBackTextView = (TextView) findViewById(R.id.calllogs_back);
        mBackTextView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mTimeTextView = (TextView) findViewById(R.id.calllogs_time);
        mTimeTextView.setText(Utils.getTime());

    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (ContactUtils.ACTION_LOGS_COMPLETE.equals(action)) {
                if (ContactUtils.loadCallLogs().isEmpty())
                    return;
                mLogsAdapter.clear();
                mLogsAdapter.addAll(ContactUtils.loadCallLogs());
                mLogsAdapter.notifyDataSetChanged();
            } else if (ContactsUtils.ACTION_CONTACTS_SYNC_FAILED.equals(action)) {
                mEmptyTextView.setText(R.string.sync_failure);
            }
        }
    };

    public static class CallLogsAdapter extends ArrayAdapter<CallLog> {

        private Context mContext;

        private int mResource;

        public CallLogsAdapter(Context context, int resource) {
            super(context, resource);
            this.mContext = context;
            this.mResource = resource;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = LayoutInflater.from(mContext).inflate(mResource, parent, false);
                holder.number = (TextView) convertView.findViewById(R.id.calllog_number);
                holder.size = (TextView) convertView.findViewById(R.id.calllog_size);
                holder.type = (ImageView) convertView.findViewById(R.id.calllog_type);
                holder.numberType = (TextView) convertView.findViewById(R.id.calllog_number_type);
                holder.time = (TextView) convertView.findViewById(R.id.calllog_time);
                holder.details = (ImageButton) convertView.findViewById(R.id.calllog_details);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            final CallLog log = getItem(position);
            String name = log.getName();
            if(name == null || name.isEmpty()){
                holder.number.setText(log.getPhone());
            } else {
                holder.number.setText(name);
            }
            int size = log.getLogs().size();
            if (size > 1) {
                holder.size.setText("(" + size + ")");
            } else {
                holder.size.setText("");
            }
            if (log.getType() == 3) {
                holder.number.setTextColor(Color.RED);
                holder.size.setTextColor(Color.RED);
            } else {
                holder.number.setTextColor(Color.WHITE);
                holder.size.setTextColor(Color.WHITE);
            }
            holder.type.setImageResource(getRes(log.getType()));
            holder.numberType.setText(getString(log.getNumType()));
            holder.time.setText(log.getLogs().get(0).getTime());
            holder.details.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent smsIntent = new Intent(mContext, CallLogDetailActivity.class);
                    smsIntent.putExtra("calllog", log);
                    mContext.startActivity(smsIntent);
                }
            });
            if (Utils.isRound())
                convertView.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, Utils.sScreenHeight / 3));
            return convertView;
        }

        private static class ViewHolder {
            TextView number;
            TextView size;
            ImageView type;
            TextView numberType;
            TextView time;
            ImageButton details;
        }

        /**
         * 获取号码在联系人中编辑的类型图片资源
         * 
         * @param type
         * @return
         */
        private int getRes(int type) {
            if (type == 3) {
                return R.drawable.ic_not_call;
            } else if(type == 1){
                return R.drawable.ic_call;
            }else if(type == 2){
               return R.drawable.ic_dial;
            }else{
                return -1;
            }
        }

        /**
         * 获取号码在联系人中编辑的类型
         * 
         * @param type
         * @return
         */
        private String getString(int type) {
            if (type == 2) {
                return mContext.getString(R.string.mobile);
            } else if (type == -1) {
                return "";
            } else {
                return mContext.getString(R.string.phone);
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (Utils.isBtConnected(this)) {
            Intent it = new Intent(this, TelephoneActivity.class); // Utils.ACTION_OUT_CALL);
            it.setAction(Utils.ACTION_OUT_CALL);
            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            it.putExtra(Utils.TELEPHONE_FRAGMENT, CallFargment.class.getName());
            it.putExtra(Utils.TELEPHONE_TAG, "call");
            if (Utils.isRound()) {
                it.putExtra(Utils.TELEPHONE_NUMBER, mLogsAdapter.getItem(position - 1).getPhone());
            } else {
                it.putExtra(Utils.TELEPHONE_NUMBER, mLogsAdapter.getItem(position).getPhone());
            }
            startActivity(it);
        }else{
            showBtDisconnectedDialog();
        }
    }

    @Override
    public void onTimeChange() {
        if (mTimeTextView != null)
            mTimeTextView.setText(Utils.getTime());
    }
    
    private AmazingDialog disconnected = null;

    private void showBtDisconnectedDialog() {

        if (disconnected == null) {
            disconnected = new AmazingDialog(this).setRightScrollEnable(false);
            disconnected.setCancelable(false);
            disconnected.setContent(R.string.bt_disconnect);
            disconnected.setPositiveButton(R.drawable.ic_cancel, new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    disconnected.dismiss();
                    disconnected = null;
                }
            });
        }
        disconnected.show();
    }
}
