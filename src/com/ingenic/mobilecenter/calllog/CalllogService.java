/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * nongjiabao<jiabao.nong@ingenic.com>
 * 
 * Elf/IDWS Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.ingenic.mobilecenter.calllog;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.elf.CalllogInfo;
import com.ingenic.iwds.datatransactor.elf.CalllogTransactionModel;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.mobilecenter.contacts.ContactsUtils;

import android.app.Service;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.provider.CallLog;
import android.provider.CallLog.Calls;

/**
 * CalllogService 通话记录服务
 * @author jbnong
 *
 */
public class CalllogService extends Service implements CalllogTransactionModel.CalllogTransactionCallback {
    private static final String TAG = "CalllogService";
    public static final int MSG_DATAARRIVED = 0;
    private static final int SYNC_CALLLOG_START = -1;
    private static final int SYNC_CALLLOG_END = -2;
    private CalllogTransactionModel mModel;
    private Handler mHandler = null;
    private Context mContext;

    /**
     * 同步通话记录UUID
     */
    private static final String CALLLOG = "90cc8a7a-fb8e-11e4-a9c3-5404a6abe086";

    class MyThread implements Runnable{
        @Override
        public void run() {
            Looper.prepare();
            mHandler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    switch (msg.what) {
                        case MSG_DATAARRIVED:
                            CalllogInfo info =(CalllogInfo)(msg.obj);
                            if (info==null)
                                break;
                            /**假如收到的info._id == -1,删除calllog数据*/
                            if(info.get_id() == SYNC_CALLLOG_START){
                                IwdsLog.d(TAG, "calllog sync start");
                                ContactsUtils.setCalllogSyncstate(mContext ,1);
                                clearCalllog();
                                break;
                            }
                            if (info.get_id() == SYNC_CALLLOG_END){
                                IwdsLog.d(TAG, "calllog sync finished");
                                ContactsUtils.setCalllogSyncstate(mContext, 0);
                                /*同步完成，通知界面*/
                                Intent it = new Intent(ContactsUtils.ACTION_CALLLOG_SYNC_FINISHED);
                                mContext.sendBroadcast(it);
                                break;
                            }

                            boolean exist = queryCalllog(info.get_id());
                            if (exist){
                                deleteCalllog(info.get_id());
                            }
                            insertCalllog(info);
                            break;
                        default:
                            break;
                    }
                }
            };
            Looper.loop();
        }
    };

    /**判断该通话记录是否已经存在手表中
     * @param call_Id 通话记录的 _Id 和 手机端的通话记录_Id一一对应
     * @return
     */

    private boolean queryCalllog(int call_Id){
        Cursor csr = getContentResolver().query(Calls.CONTENT_URI, null, "_id==?", new String[] { "" + call_Id },null);
        if (csr==null){
            return false;
        }
        if (csr.getCount()==0){
            csr.close();
            return false;
        }
        csr.close();
        return true;
    }

    /**插入通话记录
     * @param info
     */
    private void insertCalllog(CalllogInfo info) {
        ContentValues values = new ContentValues();
        ContentResolver contentResolver = getContentResolver();
        values.clear();
        values.put(CallLog.Calls._ID, info.get_id());
        values.put(CallLog.Calls.NUMBER, info.getNumber());
        values.put(CallLog.Calls.CACHED_NAME, info.getName());
        values.put(CallLog.Calls.DATE, info.getDate());
        values.put(CallLog.Calls.DURATION, info.getDuration());
        values.put(CallLog.Calls.IS_READ, info.getIs_read());
        values.put(CallLog.Calls.TYPE, info.getType());
        values.put(CallLog.Calls.NEW, info.getNewflag());
        contentResolver.insert(CallLog.Calls.CONTENT_URI, values);
        values = null;
    }
    /**
     * 清除Calllog数据库
     **/
    public void clearCalllog(){
        getContentResolver().delete(Calls.CONTENT_URI, null, null);
    }

    /**
     * 删除Calllog 记录
     **/
    public void deleteCalllog( int _id){
        IwdsLog.i(TAG,"deleteCalllog by _id = " + _id);
        getContentResolver().delete(CallLog.Calls.CONTENT_URI, "_id==?", new String[] { "" + _id });
    }

    public void onCreate() {
        IwdsLog.i(TAG, "+++onCreate. ");
        super.onCreate();
        mContext = getBaseContext();
//        ContactsUtils.setCalllogSyncstate(mContext, -1);
        MyThread myThread = new MyThread();
        new Thread(myThread).start();
        if (mModel == null) {
            mModel = new CalllogTransactionModel(this, this, CALLLOG);
        }
        mModel.start();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        IwdsLog.i(TAG, "+++onStartCommand. ");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        if (mModel != null) {
            mModel.stop();
        }
        super.onDestroy();
    }

    @Override
    public void onRequest() {}

    @Override
    public void onRequestFailed() {}

    @Override
    public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {}

    @Override
    public void onChannelAvailable(boolean isAvailable) {
    }

    @Override
    public void onSendResult(DataTransactResult result) {}

    @Override
    public void onObjectArrived(CalllogInfo object) {
        mHandler.obtainMessage(MSG_DATAARRIVED, object).sendToTarget();
    }
}
