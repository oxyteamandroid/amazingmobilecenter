package com.ingenic.mobilecenter.calllog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ingenic.iwds.app.AmazingDialog;
import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.mobilecenter.MobileCenterApplication;
import com.ingenic.mobilecenter.R;
import com.ingenic.mobilecenter.TimeChangeListener;
import com.ingenic.mobilecenter.call.CallFargment;
import com.ingenic.mobilecenter.call.TelephoneActivity;
import com.ingenic.mobilecenter.calllog.CallLog.Log;
import com.ingenic.mobilecenter.sms.SmsAddress;
import com.ingenic.mobilecenter.sms.SmsDetailActivity;
import com.ingenic.mobilecenter.utils.Utils;

public class CallLogDetailActivity extends RightScrollActivity implements OnClickListener, TimeChangeListener, OnItemClickListener {

    /**
     * 通话记录号码
     */
    private CallLog sCallLog;

    /**
     * back控件(方屏)
     */
    private View mBackView;

    /**
     * back控件(圆屏)
     */
    private LinearLayout mBackLayout;

    /**
     * 时间控件
     */
    private TextView mTimeTextView;

    /**
     * 名字控件
     */
    private TextView mNameTextView;

    /**
     * 号码控件
     */
    private TextView mNumberTextView;

    /**
     * 发送短信控件
     */
    private ImageButton mSmsImageButton;

    /**
     * 呼叫item(方屏)
     */
    private LinearLayout mCallLayout;

    /**
     * 某个号码的通话记录列表控件
     */
    private ListView mLogsListView;

    /**
     * 适配圆屏
     */
    private LinearLayout headerLayout;

    /**
     * 适配器
     */
    private CallLogDetailAdapter mCallLogDetailAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sCallLog = (CallLog) getIntent().getSerializableExtra("calllog");
        if (sCallLog == null) {
            finish();
        }
        if (Utils.isRound()) {
            setContentView(R.layout.activity_calllog_detail_round);
        } else {
            setContentView(R.layout.activity_calllog_detail);
        }

        initView();

        ((MobileCenterApplication) getApplication()).registerListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @SuppressLint("CutPasteId")
    private void initView() {
        mLogsListView = (ListView) findViewById(R.id.calllog_list);
        mLogsListView.setDivider(new ColorDrawable(getResources().getColor(R.color.color_grey_text)));
        mLogsListView.setDividerHeight(1);
        mLogsListView.setSelector(R.drawable.btn_activity_main_round);

        mNumberTextView = (TextView) findViewById(R.id.calllog_number);

        if (Utils.isRound()) {
            mLogsListView.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {
                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    for (int i = 0; i < mLogsListView.getChildCount(); i++) {
                        mLogsListView.getChildAt(i).invalidate();
                    }
                }
            });
            headerLayout = (LinearLayout) findViewById(R.id.header);
            headerLayout.getLayoutParams().height = Utils.sScreenHeight / 3;

            TextView headerView = new TextView(this);
            headerView.setHeight(Utils.sScreenHeight / 3);
            mLogsListView.addHeaderView(headerView);

            mNameTextView = (TextView) findViewById(R.id.calllog_name);
            mNameTextView.setText((sCallLog.getName() == null || sCallLog.getName().isEmpty()) ? sCallLog.getPhone() : sCallLog.getName());

            if ((sCallLog.getName() == null || sCallLog.getName().isEmpty())) {
                mNumberTextView.setVisibility(View.GONE);
            } else {
                mNumberTextView.setVisibility(View.VISIBLE);
                mNumberTextView.setText(sCallLog.getPhone());
            }

            mBackLayout = (LinearLayout) findViewById(R.id.calllog_back);
            mBackLayout.setOnClickListener(this);

            mCallLogDetailAdapter = new CallLogDetailAdapter(this, R.layout.item_calllog_detail_round);
        } else {
            mBackView = (TextView) findViewById(R.id.calllog_back);
            ((TextView) mBackView).setText((sCallLog.getName() == null || sCallLog.getName().isEmpty()) ? sCallLog.getPhone() : sCallLog.getName());
            mBackView.setOnClickListener(this);

            mNumberTextView.setText(sCallLog.getPhone());

            mCallLayout = (LinearLayout) findViewById(R.id.calllog_call);
            mCallLayout.setOnClickListener(this);

            mCallLogDetailAdapter = new CallLogDetailAdapter(this, R.layout.item_calllog_detail);
        }
        mSmsImageButton = (ImageButton) findViewById(R.id.calllog_sms);
        mSmsImageButton.setOnClickListener(this);

        mTimeTextView = (TextView) findViewById(R.id.calllog_time);
        mTimeTextView.setText(Utils.getTime());

        mLogsListView.setOnItemClickListener(this);
        mLogsListView.setAdapter(mCallLogDetailAdapter);
        mCallLogDetailAdapter.addAll(sCallLog.getLogs());
        mCallLogDetailAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.calllog_back:
            onBackPressed();
            break;
        case R.id.calllog_call:
            if (Utils.isBtConnected(this)) {
                Intent callIntent = new Intent(this, TelephoneActivity.class);
                callIntent.setAction(Utils.ACTION_OUT_CALL);
                callIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                callIntent.putExtra(Utils.TELEPHONE_FRAGMENT, CallFargment.class.getName());
                callIntent.putExtra(Utils.TELEPHONE_TAG, "call");
                callIntent.putExtra(Utils.TELEPHONE_NUMBER, sCallLog.getPhone());
                startActivity(callIntent);
            } else {
                showBtDisconnectedDialog();
            }
            break;
        case R.id.calllog_sms:
            SmsAddress address = new SmsAddress(sCallLog.getPhone());
            address.name = sCallLog.getName();
            Intent smsIntent = new Intent(this, SmsDetailActivity.class);
            smsIntent.putExtra("address", address);
            startActivity(smsIntent);
            break;
        default:
            break;
        }
    }

    private class CallLogDetailAdapter extends ArrayAdapter<Log> {

        private Context mContext;

        private int mResource;

        public CallLogDetailAdapter(Context context, int resource) {
            super(context, resource);
            this.mContext = context;
            this.mResource = resource;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = LayoutInflater.from(mContext).inflate(mResource, parent, false);
                holder.time = (TextView) convertView.findViewById(R.id.time);
                holder.type = (TextView) convertView.findViewById(R.id.type);
                holder.duration = (TextView) convertView.findViewById(R.id.duration);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            final Log log = getItem(position);
            holder.time.setText(log.getTime());
            int type = sCallLog.getType();
            holder.type.setText(getString(type));
            if (type == 3) {
                holder.type.setTextColor(Color.RED);
                holder.duration.setVisibility(View.GONE);
            } else {
                holder.type.setTextColor(Color.GRAY);
                holder.duration.setVisibility(View.VISIBLE);
            }
            int duration = (int) log.getDuration();
            int h = duration / 3600;
            int m = duration % 3600 / 60;
            int s = duration % 60;
            String durationText = "";
            if (h != 0) {
                durationText = h + mContext.getString(R.string.hours);
            }
            durationText = durationText + m + mContext.getString(R.string.minutes) + s + mContext.getString(R.string.seconds);
            holder.duration.setText(durationText);
            if (Utils.isRound())
                convertView.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, Utils.sScreenHeight / 3));
            return convertView;
        }

        private class ViewHolder {
            TextView time;
            TextView type;
            TextView duration;
        }

        /**
         * 获取号码在联系人中编辑的类型
         * 
         * @param type
         * @return
         */
        private String getString(int type) {
            if (type == 3) {
                return mContext.getString(R.string.not_call);
            } else if (type == 2) {
                return mContext.getString(R.string.dial);
            } else if (type == 1) {
                return mContext.getString(R.string.call);
            } else {
                return "";
            }
        }
    }

    @Override
    public void onTimeChange() {
        if (mTimeTextView != null)
            mTimeTextView.setText(Utils.getTime());
    }

    private AmazingDialog disconnected = null;

    private void showBtDisconnectedDialog() {

        if (disconnected == null) {
            disconnected = new AmazingDialog(this).setRightScrollEnable(false);
            disconnected.setCancelable(false);
            disconnected.setContent(R.string.bt_disconnect);
            disconnected.setPositiveButton(R.drawable.ic_cancel, new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    disconnected.dismiss();
                    disconnected = null;
                }
            });
        }
        disconnected.show();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (Utils.isBtConnected(this)) {
            Intent callIntent = new Intent(this, TelephoneActivity.class);
            callIntent.setAction(Utils.ACTION_OUT_CALL);
            callIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            callIntent.putExtra(Utils.TELEPHONE_FRAGMENT, CallFargment.class.getName());
            callIntent.putExtra(Utils.TELEPHONE_TAG, "call");
            callIntent.putExtra(Utils.TELEPHONE_NUMBER, sCallLog.getPhone());
            startActivity(callIntent);
        } else {
            showBtDisconnectedDialog();
        }
    }
}
