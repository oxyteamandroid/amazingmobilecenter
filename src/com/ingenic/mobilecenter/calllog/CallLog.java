package com.ingenic.mobilecenter.calllog;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CallLog implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public CallLog(String id, String phone, List<Log> time, int type) {
        this.id = id;
        this.phone = phone;
        this.logs = time;
        this.type = type;
        this.numType = -1;
    }

    private String id;

    private String name;

    private String phone;

    private List<Log> logs = new ArrayList<Log>();

    private int type;

    private int numType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<Log> getLogs() {
        return logs;
    }

    public void setLogs(List<Log> logs) {
        this.logs = logs;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getNumType() {
        return numType;
    }

    public void setNumType(int numType) {
        this.numType = numType;
    }

    @Override
    public String toString() {
        return "id:" + id + ",name:" + name + ",phone:" + phone + ",type:" + type + ",numTypt:" + numType + ",time:" + logs;
    }

    public static class Log implements Serializable {
        /**
         * 
         */
        private static final long serialVersionUID = 1L;

        String time;
        int duration;

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public int getDuration() {
            return duration;
        }

        public void setDuration(int duration) {
            this.duration = duration;
        }
    }
}
