package com.ingenic.mobilecenter.contacts;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Contact implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public Contact(String id, String name, List<Phone> phones) {
        this.id = id;
        this.name = name;
        this.phones = phones;
    }

    private String id;

    private String name;

    private List<Phone> phones = new ArrayList<Phone>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public static class Phone implements Serializable {
        /**
         * 
         */
        private static final long serialVersionUID = 1L;
        private String number;
        private int type;

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }
    }
}
