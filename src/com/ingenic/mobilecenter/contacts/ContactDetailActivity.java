package com.ingenic.mobilecenter.contacts;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ingenic.iwds.app.AmazingDialog;
import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.mobilecenter.MobileCenterApplication;
import com.ingenic.mobilecenter.R;
import com.ingenic.mobilecenter.TimeChangeListener;
import com.ingenic.mobilecenter.call.CallFargment;
import com.ingenic.mobilecenter.call.TelephoneActivity;
import com.ingenic.mobilecenter.contacts.Contact.Phone;
import com.ingenic.mobilecenter.sms.SmsAddress;
import com.ingenic.mobilecenter.sms.SmsDetailActivity;
import com.ingenic.mobilecenter.utils.Utils;

public class ContactDetailActivity extends RightScrollActivity implements TimeChangeListener, OnItemClickListener {

    /**
     * back控件
     */
    private TextView mBackTextView;

    /**
     * 时间控件
     */
    private TextView mTimeTextView;

    /**
     * 联系人列表控件
     */
    private ListView mPhoneListView;

    /**
     * 联系人适配器
     */
    private ContactDetailAdapter mContactDetailAdapter;

    private LinearLayout headerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Utils.isRound()) {
            setContentView(R.layout.activity_contact_detail_round);
        } else {
            setContentView(R.layout.activity_contact_detail);
        }
        ((MobileCenterApplication) getApplication()).registerListener(this);

        Contact contact = (Contact) getIntent().getSerializableExtra("contact");

        initViews();

        mBackTextView.setText(contact.getName());

        mContactDetailAdapter.addAll(contact.getPhones());
        mContactDetailAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    private void initViews() {
        mPhoneListView = (ListView) findViewById(R.id.contact_detail_list);
        mPhoneListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                for (int i = 0; i < mPhoneListView.getChildCount(); i++) {
                    mPhoneListView.getChildAt(i).invalidate();
                }
            }
        });
        mPhoneListView.setEmptyView(findViewById(android.R.id.empty));
        if (Utils.isRound()) {
            headerLayout = (LinearLayout) findViewById(R.id.header);
            headerLayout.getLayoutParams().height = Utils.sScreenHeight / 3;
            mPhoneListView.setDivider(new ColorDrawable(getResources().getColor(R.color.color_grey_text)));
            mPhoneListView.setDividerHeight(1);
            mPhoneListView.setSelector(R.drawable.btn_activity_main_round);
            mContactDetailAdapter = new ContactDetailAdapter(this, R.layout.item_contact_phone_round);

            TextView headerView = new TextView(this);
            headerView.setHeight(Utils.sScreenHeight / 3);
            mPhoneListView.addHeaderView(headerView);
            TextView footerView = new TextView(this);
            footerView.setHeight(Utils.sScreenHeight / 3);
            mPhoneListView.addFooterView(footerView);
        } else {
            mPhoneListView.setDividerHeight(20);
            mPhoneListView.setSelector(R.drawable.btn_round_angle);
            mContactDetailAdapter = new ContactDetailAdapter(this, R.layout.item_contact_phone);
        }

        mPhoneListView.setAdapter(mContactDetailAdapter);
        mPhoneListView.setOnItemClickListener(this);

        mBackTextView = (TextView) findViewById(R.id.contact_detail_back);
        mBackTextView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mTimeTextView = (TextView) findViewById(R.id.contact_detail_time);
        mTimeTextView.setText(Utils.getTime());

    }

    private static class ContactDetailAdapter extends ArrayAdapter<Phone> {

        private Context mContext;

        private int mResource;

        public ContactDetailAdapter(Context context, int resource) {
            super(context, resource);
            this.mContext = context;
            this.mResource = resource;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = LayoutInflater.from(mContext).inflate(mResource, parent, false);
                holder.type = (TextView) convertView.findViewById(R.id.phone_type);
                holder.number = (TextView) convertView.findViewById(R.id.phone_numer);
                holder.sms = (ImageButton) convertView.findViewById(R.id.phone_sms);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            final Phone phone = getItem(position);
            holder.type.setText(getString(phone.getType()));
            holder.number.setText(phone.getNumber());
            holder.sms.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    SmsAddress address = new SmsAddress(phone.getNumber());
                    Intent it = new Intent(mContext, SmsDetailActivity.class);
                    it.putExtra("address", address);
                    mContext.startActivity(it);
                }
            });
            if (Utils.isRound())
                convertView.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, Utils.sScreenHeight / 3));
            return convertView;
        }

        /**
         * 获取号码在联系人中编辑的类型
         * 
         * @param type
         * @return
         */
        private String getString(int type) {
            if (type == 2) {
                return mContext.getString(R.string.mobile);
            } else {
                return mContext.getString(R.string.phone);
            }
        }

        private static class ViewHolder {
            TextView type;
            TextView number;
            ImageButton sms;
        }
    }

    @Override
    public void onTimeChange() {
        mTimeTextView.setText(Utils.getTime());
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (Utils.isBtConnected(this)) {
            Intent it = new Intent(this, TelephoneActivity.class); // Utils.ACTION_OUT_CALL);
            it.setAction(Utils.ACTION_OUT_CALL);
            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            it.putExtra(Utils.TELEPHONE_FRAGMENT, CallFargment.class.getName());
            it.putExtra(Utils.TELEPHONE_TAG, "call");
            if (Utils.isRound()) {
                it.putExtra(Utils.TELEPHONE_NUMBER, mContactDetailAdapter.getItem(position - 1).getNumber().replace(" ", ""));
            } else {
                it.putExtra(Utils.TELEPHONE_NUMBER, mContactDetailAdapter.getItem(position).getNumber().replace(" ", ""));
            }
            startActivity(it);
        } else {
            showBtDisconnectedDialog();
        }
    }

    private AmazingDialog disconnected = null;

    private void showBtDisconnectedDialog() {

        if (disconnected == null) {
            disconnected = new AmazingDialog(this).setRightScrollEnable(false);
            disconnected.setCancelable(false);
            disconnected.setContent(R.string.bt_disconnect);
            disconnected.setPositiveButton(R.drawable.ic_cancel, new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    disconnected.dismiss();
                    disconnected = null;
                }
            });
        }
        disconnected.show();
    }
}
