/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * nongjiabao<jiabao.nong@ingenic.com>
 *
 * Elf/IDWS Project
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.ingenic.mobilecenter.contacts;

import android.content.Context;
import android.content.SharedPreferences;

import com.ingenic.iwds.HardwareList;

/**
 * 电话本不同工具类
 * @author jbnong
 *
 */
public class ContactsUtils {
    public static final String ACTION_CONTACTS_SYNC_FINISHED = "com.ingenic.contacts.syncfinished";
    public static final String ACTION_CONTACTS_SYNC_FAILED = "com.ingenic.calllog.syncfailed";
    public static final String ACTION_CALLLOG_SYNC_FINISHED = "com.ingenic.calllog.syncfinished";
    public static final String SHARE_PREFERENCE = "AmazingMobileCenter";
    public static final String CONTACTS_SYNC_STATE = "contacts_sync_state";
    public static final String CONTACTS_SYNC_CALLLOG_STATE = "contacts_sync_calllog_state";
    public static final String PHONE_ADDRESS = "phone-address";
    public static final String PHONE_ADDRESS_DEFUALT = "00:00:00:00:00:00";

    public static final String CMD_VCARD_EMPTY ="vcard-empty";
    public static final String CMD_SYNC_FAILED ="sync-failed";

    public static final int STATE_SUCCESSED = 0;
    public static final int STATE_SYNCING   = 1;
    public static final int STATE_FAILED    = 2;
    public static final int STATE_UNKNOWN   = -1;

    /**
     * @param context
     * @param state 1 正在同步， 0 同步完成， -1 不知状态
     */
    public static void setContactsSyncstate(Context context, int state) {
        SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCE,
                Context.MODE_PRIVATE);
        sp.edit().putInt(CONTACTS_SYNC_CALLLOG_STATE, state).commit();
    }

    public static int getContactsSyncstate(Context context) {
        SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCE,
                Context.MODE_PRIVATE);
        return sp.getInt(CONTACTS_SYNC_CALLLOG_STATE, -1);
    }

    /**
     * @param context
     * @param state 1 正在同步， 0 同步完成， -1 不知状态
     */
    public static void setCalllogSyncstate(Context context, int state) {
        SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCE,
                Context.MODE_PRIVATE);
        sp.edit().putInt(CONTACTS_SYNC_STATE, state).commit();
    }

    public static int getCalllogSyncstate(Context context) {
        SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCE,
                Context.MODE_PRIVATE);
        return sp.getInt(CONTACTS_SYNC_STATE, -1);
    }

    /**
     * 绑定连接后，保存 phone 的 mac 地址
     * 
     * @param context
     * @param phone_address
     */
    public static void setPhoneMacAddress(Context context, String phone_address) {
        SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCE, Context.MODE_PRIVATE);
        sp.edit().putString(PHONE_ADDRESS, phone_address).commit();
    }

    /**
     * 获取 phone 的 mac 地址
     * 
     * @param context
     * @return
     */
    public static String getPhoneMacAddress(Context context) {
        SharedPreferences sp = context.getSharedPreferences(SHARE_PREFERENCE, Context.MODE_PRIVATE);
        return sp.getString(PHONE_ADDRESS, PHONE_ADDRESS_DEFUALT);
    }

    /**
     * 判断当前的屏是圆还是方形
     * @return
     */
    public static boolean IsCircularScreen(){
        return HardwareList.IsCircularScreen();
    }
}
