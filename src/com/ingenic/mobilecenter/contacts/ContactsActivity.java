package com.ingenic.mobilecenter.contacts;

import com.ingenic.iwds.app.AmazingDialog;
import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.mobilecenter.MobileCenterApplication;
import com.ingenic.mobilecenter.R;
import com.ingenic.mobilecenter.TimeChangeListener;
import com.ingenic.mobilecenter.call.CallFargment;
import com.ingenic.mobilecenter.call.TelephoneActivity;
import com.ingenic.mobilecenter.utils.ContactUtils;
import com.ingenic.mobilecenter.utils.Utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class ContactsActivity extends RightScrollActivity implements TimeChangeListener, OnItemClickListener {

    /**
     * list空提示控件
     */
    private TextView mEmptyTextView;

    /**
     * back控件
     */
    private TextView mBackTextView;

    /**
     * 时间控件
     */
    private TextView mTimeTextView;

    /**
     * 联系人列表控件
     */
    private ListView mContactsListView;

    /**
     * 联系人列表适配器
     */
    private ContactsAdapter mContactsAdapter;

    /**
     * 适配圆屏
     */
    private LinearLayout headerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Utils.isRound()) {
            setContentView(R.layout.activity_contacts_round);
        } else {
            setContentView(R.layout.activity_contacts);
        }
        ((MobileCenterApplication) getApplication()).registerListener(this);

        initViews();

        int state = ContactsUtils.getContactsSyncstate(this);
        if (state == 0) {// 联系人同步完成
            if (!ContactUtils.getQuering()) {
                mContactsAdapter.addAll(ContactUtils.loadContacts());
                mContactsAdapter.notifyDataSetChanged();
            } else {
                mEmptyTextView.setText(R.string.load);
            }
        } else if (state == 2) {// 联系人同步失败
            mEmptyTextView.setText(R.string.sync_failure);
        } else {
            mEmptyTextView.setText(R.string.are_sync);
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(ContactsUtils.ACTION_CONTACTS_SYNC_FAILED);
        filter.addAction(ContactUtils.ACTION_CONTACTS_COMPLETE);
        registerReceiver(mReceiver, filter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
    }

    private void initViews() {
        mContactsListView = (ListView) findViewById(R.id.contacts_list);
        mContactsListView.setEmptyView(findViewById(android.R.id.empty));
        if (Utils.isRound()) {
            mContactsListView.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {
                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    for (int i = 0; i < mContactsListView.getChildCount(); i++) {
                        mContactsListView.getChildAt(i).invalidate();
                    }
                }
            });
            headerLayout = (LinearLayout) findViewById(R.id.header);
            headerLayout.getLayoutParams().height = Utils.sScreenHeight / 3;
            mContactsListView.setDivider(new ColorDrawable(getResources().getColor(R.color.color_grey_text)));
            mContactsListView.setDividerHeight(1);
            mContactsListView.setSelector(R.drawable.btn_activity_main_round);
            mContactsAdapter = new ContactsAdapter(this, R.layout.item_contact_round);

            TextView headerView = new TextView(this);
            headerView.setHeight(Utils.sScreenHeight / 3);
            mContactsListView.addHeaderView(headerView);
            TextView footerView = new TextView(this);
            footerView.setHeight(Utils.sScreenHeight / 3);
            mContactsListView.addFooterView(footerView);
        } else {
            mContactsListView.setDividerHeight(10);
            mContactsListView.setSelector(R.drawable.btn_round_angle);
            mContactsAdapter = new ContactsAdapter(this, R.layout.item_contact);
        }
        mEmptyTextView = (TextView) findViewById(android.R.id.empty);

        mContactsListView.setAdapter(mContactsAdapter);
        mContactsListView.setOnItemClickListener(this);

        mBackTextView = (TextView) findViewById(R.id.contacts_back);
        mBackTextView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mTimeTextView = (TextView) findViewById(R.id.contacts_time);
        mTimeTextView.setText(Utils.getTime());

    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (ContactUtils.ACTION_CONTACTS_COMPLETE.equals(action)) {
                if (ContactUtils.loadContacts().isEmpty())
                    return;
                mContactsAdapter.clear();
                mContactsAdapter.addAll(ContactUtils.loadContacts());
                mContactsAdapter.notifyDataSetChanged();
            } else if (ContactsUtils.ACTION_CONTACTS_SYNC_FAILED.equals(action)) {
                mEmptyTextView.setText(R.string.sync_failure);
            }
        }
    };

    private class ContactsAdapter extends ArrayAdapter<Contact> {

        private Context mContext;
        private int mResource;

        public ContactsAdapter(Context context, int resource) {
            super(context, resource);
            this.mContext = context;
            this.mResource = resource;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(mResource, parent, false);

                holder = new ViewHolder();
                holder.name = (TextView) convertView.findViewById(R.id.contact_name);
                holder.dial = (ImageButton) convertView.findViewById(R.id.contact_dial);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            final Contact contact = getItem(position);

            holder.name.setText(contact.getName());
            holder.dial.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (Utils.isBtConnected(mContext)) {
                        if (!contact.getPhones().isEmpty()) {
                            Intent it = new Intent(mContext, TelephoneActivity.class);
                            it.setAction(Utils.ACTION_OUT_CALL);
                            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            it.putExtra(Utils.TELEPHONE_FRAGMENT, CallFargment.class.getName());
                            it.putExtra(Utils.TELEPHONE_TAG, "call");
                            it.putExtra(Utils.TELEPHONE_NUMBER, contact.getPhones().get(0).getNumber());
                            mContext.startActivity(it);
                        }
                    } else {
                        showBtDisconnectedDialog();
                    }
                }
            });
            if (Utils.isRound())
                convertView.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, Utils.sScreenHeight / 3));
            return convertView;
        }

        private class ViewHolder {
            TextView name;
            ImageButton dial;
        }
    }

    @Override
    public void onTimeChange() {
        if (mTimeTextView != null)
            mTimeTextView.setText(Utils.getTime());
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Contact contact;
        if (Utils.isRound()) {
            contact = mContactsAdapter.getItem(position - 1);
        } else {
            contact = mContactsAdapter.getItem(position);
        }
        Intent it = new Intent(this, ContactDetailActivity.class);
        it.putExtra("contact", contact);
        startActivity(it);
    }

    private AmazingDialog disconnected = null;

    private void showBtDisconnectedDialog() {

        if (disconnected == null) {
            disconnected = new AmazingDialog(this).setRightScrollEnable(false);
            disconnected.setCancelable(false);
            disconnected.setContent(R.string.bt_disconnect);
            disconnected.setPositiveButton(R.drawable.ic_cancel, new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    disconnected.dismiss();
                    disconnected = null;
                }
            });
        }
        disconnected.show();
    }
}
