package com.ingenic.mobilecenter.contacts;

import java.util.ArrayList;
import java.util.List;

import com.ingenic.iwds.datatransactor.elf.ContactInfo;

public class ContentControl {

    private static ContentControl mInstance = null;

    private List<ContactInfo> mInfos = null;

    public List<ContactInfo> getInfos() {
        return mInfos;
    }

    public void setInfos(List<ContactInfo> infos) {
        this.mInfos.clear();
        this.mInfos.addAll(infos);
    }

    private ContentControl() {
        mInfos = new ArrayList<ContactInfo>();
    }

    /** 获取实例 */
    public static ContentControl getInstance() {
        if (mInstance == null) {
            mInstance = new ContentControl();
        }
        return mInstance;
    }

}
