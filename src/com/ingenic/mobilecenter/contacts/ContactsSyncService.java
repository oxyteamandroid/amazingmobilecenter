/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * nongjiabao<jiabao.nong@ingenic.com>
 *
 * Elf/IDWS Project
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

package com.ingenic.mobilecenter.contacts;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.FileInfo;
import com.ingenic.iwds.datatransactor.FileTransactionModel;
import com.ingenic.iwds.datatransactor.FileTransactionModel.FileTransactionModelCallback;
import com.ingenic.iwds.datatransactor.elf.ContactInfo;
import com.ingenic.iwds.utils.IwdsLog;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;

/**
 * 同步联系人服务
 */
public class ContactsSyncService extends Service {
    private static String TAG = "ContactsSyncService";
    private static final int MSG_REPONE_CMD = 1;
    private static final int MSG_REQUESTSENDFILE = 2;
    private static final int MSG_CHANNEL_UNAVAILABLE = 3;
    private static final int MSG_CHANNEL_AVAILABLE = 31;
    private static final int MSG_FILEARRIVD = 4;
    private static final int MSG_DISCONNECT = 5;
    private static final int MSG_LINK_CONNECTED = 51;
    private static final int MSG_SET_PROGRESS = 6;
    private static final int MSG_DATA_ARRIVED = 7;
    private static final int MSG_VCARD_EMPTY = 8;
    private static final int MSG_SYNC_FAILED = 9;
    /**
     * 同步联系人UUID
     */
    private String UUID_CONTACTS = "c654f802-de98-11e4-8feb-5404a6abe086";
    private static ContactsFiletransModel mFiletransModel;
    private File mVcard_file = null;
    private static boolean mIsAvailable = false;
    private static boolean mPhoneisChange = false;
    private ContactHandler mContacthandler;
    private Context mContext;
    private RestoreReceiver mRestoreReceiver;
    private Handler mInsertHandler;
    private static boolean mFirstStart = false;

    private class RestoreReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ContactsUtils.ACTION_CONTACTS_SYNC_FINISHED)) {
                IwdsLog.i(TAG, "action:" + ContactsUtils.ACTION_CONTACTS_SYNC_FINISHED);
                mFiletransModel.sendCmd2Phone("contacts_sync_success");
            }
        }
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public void onCreate() {

        mContext = getBaseContext();
        String MAC = ContactsUtils.getPhoneMacAddress(mContext);
        IwdsLog.i(TAG,"ContactsSyncService start...MAC " +  MAC);
        if (MAC.equals(ContactsUtils.PHONE_ADDRESS_DEFUALT)){
            IwdsLog.i(TAG,"ContactsSyncService first run...");
            mFirstStart = true;
        }
        mRestoreReceiver = new RestoreReceiver();
        IntentFilter it = new IntentFilter(ContactsUtils.ACTION_CONTACTS_SYNC_FINISHED);
        mContext.registerReceiver(mRestoreReceiver, it);

        MyThread myThread = new MyThread();
        new Thread(myThread).start();

        mContacthandler = ContactHandler.getInstance(mContext);
        if (mContacthandler.isContactsfileExist()) {
            mContacthandler.StartRestoreContactsThread();
        }
        if (mFiletransModel == null) {
            mFiletransModel = new ContactsFiletransModel(mContext, new ContactsFileTransfer(), UUID_CONTACTS);
        }
        mFiletransModel.start();
        IwdsLog.e(this, "mFiletransModel start");
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mFiletransModel != null) {
            mFiletransModel.stop();
            IwdsLog.e(this, "mFiletransModel stop");
        }
        mContext.unregisterReceiver(mRestoreReceiver);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    private class ContactsFiletransModel extends FileTransactionModel {

        public ContactsFiletransModel(Context context, FileTransactionModelCallback callback, String uuid) {
            super(context, callback, uuid);
        }

        public void sendCmd2Phone(String cmd) {
            m_transactor.send(cmd);
        }

        @Override
        public void onDataArrived(Object object) {
            super.onDataArrived(object);
            if (object instanceof ContactInfo) {
                mInsertHandler.obtainMessage(MSG_DATA_ARRIVED, object).sendToTarget();
            } else if (object instanceof String) {
                String cmd = (String)object;
                if (cmd.equals(ContactsUtils.CMD_VCARD_EMPTY)){
                    mInsertHandler.obtainMessage(MSG_VCARD_EMPTY).sendToTarget();
                }
                if (cmd.equals(ContactsUtils.CMD_SYNC_FAILED)){
                    mInsertHandler.obtainMessage(MSG_SYNC_FAILED).sendToTarget();
                }
            }
        }
    }

    private class ContactsFileTransfer implements FileTransactionModelCallback {
        @Override
        public void onCancelForReceiveFile() {
        }

        @Override
        public void onChannelAvailable(boolean arg0) {
            mIsAvailable = arg0;
            IwdsLog.i(TAG, "+++++onChannelAvailable: " + arg0);
            if (!arg0) {
                mHandler.obtainMessage(MSG_CHANNEL_UNAVAILABLE).sendToTarget();
            } else {
                mHandler.obtainMessage(MSG_CHANNEL_AVAILABLE).sendToTarget();
            }
        }

        @Override
        public void onConfirmForReceiveFile() {
            IwdsLog.d(TAG, "+++++ContactsFileTransfer onConfirmForReceiveFile");
        }

        @Override
        public void onFileArrived(File arg0) {
            IwdsLog.d(TAG, "+++++ContactsFileTransfer onFileArrived :" + arg0.getAbsolutePath());
            mVcard_file = arg0;
            mHandler.obtainMessage(MSG_FILEARRIVD).sendToTarget();
        }

        @Override
        public void onLinkConnected(DeviceDescriptor arg0, boolean arg1) {
            IwdsLog.d(TAG, "+++++ContactsFileTransfer onLinkConnected " + arg0.toString() + " isConnected: " + arg1);
            if (!arg1) {
                mHandler.obtainMessage(MSG_DISCONNECT).sendToTarget();
            } else {
                mHandler.obtainMessage(MSG_LINK_CONNECTED, arg0).sendToTarget();
            }
        }

        @Override
        public void onRecvFileProgress(int arg0) {
            mHandler.obtainMessage(MSG_SET_PROGRESS, arg0, 0).sendToTarget();
        }

        @Override
        public void onRequestSendFile(FileInfo arg0) {
            IwdsLog.d(TAG, "+++++onRequestSendFile");
            mHandler.obtainMessage(MSG_REQUESTSENDFILE).sendToTarget();
        }

        @Override
        public void onSendFileProgress(int arg0) {

        }

        @Override
        public void onSendResult(DataTransactResult arg0) {

        }

        @Override
        public void onFileTransferError(int arg0) {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void onRecvFileInterrupted(int arg0) {
            // TODO Auto-generated method stub
            
        }

        @Override
        public void onSendFileInterrupted(int arg0) {
            // TODO Auto-generated method stub
            
        }

    }

    private static boolean copyfile(String name) {
        final String vcf = Environment.getExternalStorageDirectory() + "/contacts.vcf";
        try {
            File oldfile = new File(name);
            File newfile = new File(vcf);
            if (newfile.exists())
                newfile.delete();
            FileInputStream in = new FileInputStream(oldfile);
            FileOutputStream out = new FileOutputStream(newfile);
            FileChannel inc = in.getChannel();
            FileChannel outc = out.getChannel();
            int len = 2097152;
            ByteBuffer b = null;
            while (true) {
                if (inc.position() == inc.size()) {
                    in.close();
                    out.close();
                    // oldfile.delete(); // remove the contacts.vcf
                    return true;
                }
                if (inc.size() - inc.position() < len) {
                    len = (int) (inc.size() - inc.position());
                } else {
                    len = 2097152;
                }
                b = ByteBuffer.allocateDirect(len);
                inc.read(b);
                b.flip();
                outc.write(b);
                outc.force(false);
            }
        } catch (IOException e) {
            return false;
        }
    }

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MSG_LINK_CONNECTED:
                DeviceDescriptor descriptor = (DeviceDescriptor) msg.obj;
                String MAC = ContactsUtils.getPhoneMacAddress(mContext);
                if (MAC.equals(ContactsUtils.PHONE_ADDRESS_DEFUALT)) {
                    ContactsUtils.setPhoneMacAddress(mContext, descriptor.devAddress);
                    break;
                }

                if (MAC.equals(descriptor.devAddress)) {
                    mPhoneisChange = false;
                } else {
                    IwdsLog.d(TAG, "+++++phone MAC is change to :" + descriptor.devAddress);
                    mPhoneisChange = true;
                    ContactsUtils.setContactsSyncstate(mContext, ContactsUtils.STATE_UNKNOWN);
                }
                ContactsUtils.setPhoneMacAddress(mContext, descriptor.devAddress);
                break;

            case MSG_REPONE_CMD:
                break;

            case MSG_REQUESTSENDFILE:
                mFiletransModel.notifyConfirmForReceiveFile();
                break;

            case MSG_CHANNEL_AVAILABLE:

                if (mFirstStart) {
                    mFirstStart = false;
                    mFiletransModel.sendCmd2Phone("first_run");
                }

                if (mPhoneisChange && mIsAvailable) {
                    IwdsLog.d(TAG, "+++++send mac change cmd to phone");
                    mFiletransModel.sendCmd2Phone("phone_mac_ischange");
                    mPhoneisChange = false;
                }
                break;

            case MSG_CHANNEL_UNAVAILABLE:
            case MSG_DISCONNECT:
                break;

            case MSG_FILEARRIVD:
                if (copyfile(mVcard_file.getAbsolutePath())) {
                    IwdsLog.d(TAG, "+++++start restore contacts ...");
                    mContacthandler.StartRestoreContactsThread();
                }
                break;

            case MSG_SET_PROGRESS:
                break;

            }
            super.handleMessage(msg);
        }
    };

    class MyThread implements Runnable {
        @Override
        public void run() {
            Looper.prepare();
            mInsertHandler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    switch (msg.what) {
                    case MSG_DATA_ARRIVED:
                        ContactInfo info = (ContactInfo) msg.obj;
                        mContacthandler.deleteContacts(info.raw_id);
                        if (info.operation != ContactInfo.OPT_DEL) {
                            IwdsLog.d(TAG, "insert contacts: " + info.toString());
                            mContacthandler.addContacts(info);
                        }
                        break;

                    case MSG_VCARD_EMPTY:
                        IwdsLog.i(TAG, "vcard file is empty");
                        mContacthandler.DeleteRawContacts();
                        ContactsUtils.setContactsSyncstate(mContext, ContactsUtils.STATE_SUCCESSED);
                        Intent it = new Intent(ContactsUtils.ACTION_CONTACTS_SYNC_FINISHED);
                        mContext.sendBroadcast(it);
                        break;
                    case MSG_SYNC_FAILED:
                        IwdsLog.i(TAG, "sync contacts file failed");
                        ContactsUtils.setContactsSyncstate(mContext, ContactsUtils.STATE_FAILED);
                        Intent it1 = new Intent(ContactsUtils.ACTION_CONTACTS_SYNC_FAILED);
                        mContext.sendBroadcast(it1);
                        break;
                    default:
                        break;
                    }
                }
            };
            Looper.loop();
        }
    };
}
