package com.ingenic.mobilecenter.view;

import com.ingenic.mobilecenter.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;

public class InputViewRound extends LinearLayout implements OnClickListener {

    private Button[] mNumbers = new Button[9];
    private OnClickListener mClickListener;

    public InputViewRound(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_input_round, this);
    }

    public InputViewRound(Context context) {
        this(context, null);
    }

    @SuppressLint("DefaultLocale")
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        View v1 = findViewById(R.id.num_line1);
        View v2 = findViewById(R.id.num_line2);
        View v3 = findViewById(R.id.num_line3);
        mNumbers[0] = (Button) v1.findViewById(R.id.key1);
        mNumbers[1] = (Button) v1.findViewById(R.id.key2);
        mNumbers[2] = (Button) v1.findViewById(R.id.key3);

        mNumbers[3] = (Button) v2.findViewById(R.id.key1);
        mNumbers[4] = (Button) v2.findViewById(R.id.key2);
        mNumbers[5] = (Button) v2.findViewById(R.id.key3);

        mNumbers[6] = (Button) v3.findViewById(R.id.key1);
        mNumbers[7] = (Button) v3.findViewById(R.id.key2);
        mNumbers[8] = (Button) v3.findViewById(R.id.key3);

        for (int i = 0; i < mNumbers.length; i++) {
            mNumbers[i].setOnClickListener(this);
            String number = String.format("%d", i + 1);
            mNumbers[i].setText(number);
            mNumbers[i].setTag(R.id.numbers_key, (char) ((i + 1) + '0'));
        }
    }


    @Override
    public void onClick(View view) {
        char val = (Character) view.getTag(R.id.numbers_key);
        if (val != 0 && mClickListener != null) {
            mClickListener.onClick(val);
        }
    }

    public void setOnClickListener(OnClickListener l) {
        mClickListener = l;
    }

    public interface OnClickListener {
        void onClick(char in);
    }

}
