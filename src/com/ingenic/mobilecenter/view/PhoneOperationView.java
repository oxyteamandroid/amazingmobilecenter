package com.ingenic.mobilecenter.view;

import com.ingenic.iwds.utils.IwdsLog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.OverScroller;

public class PhoneOperationView extends ImageView {

    public static final int ORIENTATION_VERTICAL = 0;
    public static final int ORIENTATION_HORIZONTAL = 1;
    public static final int ORIENTATION_LEFT = 2;
    public static final int ORIENTATION_RIGHT = 3;
    public static final int ORIENTATION_BELOW = 4;
    public static final int ORIENTATION_TOP = 5;

    /**
     * 滑动反弹
     */
    private OverScroller mOverScroller;

    /**
     * 滑动方向 0：垂直，1水平
     */
    private int mOrientation = ORIENTATION_HORIZONTAL;

    /**
     * 滑动距离
     */
    private int mDistance = 100;

    /**
     * 滑动反弹分界点 默认 滑动距离/2
     */
    private int mBoundaryPoint = mDistance / 2;
    /**
     * 是否设置分界点
     */
    private boolean isSetBoundaryPoint = false;

    /**
     * 按下时控件坐标
     */
    float viewX, viewY;

    /**
     * 控件当时坐标
     */
    float viewCurrentX, viewCurrentY;

    /**
     * 按下时手指坐标
     */
    float eventX, eventY;

    /**
     * 手指当时坐标
     */
    float eventCurrentX, eventCurrentY;

    /**
     * 滑动监听对象
     */
    private SlideListener mListener;

    public PhoneOperationView(Context context) {
        this(context, null);
    }

    @SuppressLint("NewApi")
    public PhoneOperationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mOverScroller = new OverScroller(context);
    }

    /**
     * 设置滑动方向 默认水平方向
     * 
     * @param orientation
     */
    public void setOrientation(int orientation) {
        this.mOrientation = orientation;
    }

    /**
     * 设置滑动距离 默认100
     * 
     * @param distance
     */
    public void setDistance(int distance) {
        if (distance <= 0)
            return;
        this.mDistance = distance;
        if (!isSetBoundaryPoint)
            setBoundaryPoint(distance / 2);
    }

    /**
     * 设置分界点 默认 滑动距离/2
     * 
     * @param mBoundaryPoint
     */
    public void setBoundaryPoint(int mBoundaryPoint) {
        if (mBoundaryPoint > mDistance)
            return;
        this.mBoundaryPoint = mBoundaryPoint;
        isSetBoundaryPoint = true;
    }

    /**
     * 设置滑动完毕监听
     * 
     * @param listener
     */
    public void setSlideListener(SlideListener listener) {
        mListener = listener;
    }

    @SuppressLint({ "ClickableViewAccessibility", "NewApi" })
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!isEnabled())
            return false;
        IwdsLog.i(this, "Action:" + event.getAction());
        switch (event.getAction()) {
        case MotionEvent.ACTION_DOWN:
            if (mListener != null)
                mListener.down(this);
            viewX = getX();
            viewY = getY();

            eventX = event.getX();
            eventY = event.getY();
            break;
        case MotionEvent.ACTION_MOVE:
            viewCurrentX = getX();
            viewCurrentY = getY();
            IwdsLog.i(this, "viewCurrentX:" + viewCurrentX + ",viewCurrentY:" + viewCurrentY);
            eventCurrentX = event.getX();
            eventCurrentY = event.getY();
            IwdsLog.i(this, "eventCurrentX:" + eventCurrentX + ",eventCurrentY:" + eventCurrentY + ",mDistance" + mDistance);

            float slidingDistanceX = viewCurrentX > viewX ? viewCurrentX - viewX : viewX - viewCurrentX;
            float slidingDistanceY = viewCurrentY > viewY ? viewCurrentY - viewY : viewY - viewCurrentY;

            switch (mOrientation) {
            case ORIENTATION_HORIZONTAL:
            case ORIENTATION_LEFT:
            case ORIENTATION_RIGHT:
                if (slidingDistanceX < mDistance) {
                    if ((mOrientation == ORIENTATION_LEFT && viewCurrentX >= viewX && eventCurrentX >= eventX)
                            || (mOrientation == ORIENTATION_RIGHT && viewCurrentX <= viewX && eventCurrentX <= eventX)) {
                        setX(viewX);
                    } else {
                        float x = (eventCurrentX - eventX) + viewCurrentX;
                        if (mOrientation == ORIENTATION_LEFT) {
                            if (x > viewX) {
                                x = viewX;
                            } else if (x < (viewX - mDistance)) {
                                x = viewX - mDistance;
                            }
                        } else if (mOrientation == ORIENTATION_RIGHT) {
                            if (x < viewX) {
                                x = viewX;
                            } else if (x > viewX + mDistance) {
                                x = viewX + mDistance;
                            }
                        } else {
                            if (x < viewX - mDistance) {
                                x = viewX - mDistance;
                            } else if (x > viewX + mDistance) {
                                x = viewX + mDistance;
                            }
                        }
                        setX(x);
                    }
                } else if (slidingDistanceX == mDistance) {
                    if ((mOrientation == ORIENTATION_LEFT && eventCurrentX >= eventX)
                            || (mOrientation == ORIENTATION_RIGHT && eventCurrentX <= eventX)) {
                        setX((eventCurrentX - eventX) + viewCurrentX);
                    } else {
                        if (viewCurrentX > viewX) {
                            setX(viewX + mDistance);
                        } else {
                            setX(viewX - mDistance);
                        }
                    }
                } else {
                    if (viewCurrentX > viewX) {
                        setX(viewX + mDistance);
                    } else {
                        setX(viewX - mDistance);
                    }
                }
                break;

            case ORIENTATION_VERTICAL:
            case ORIENTATION_TOP:
            case ORIENTATION_BELOW:
                if (slidingDistanceY < mDistance) {
                    if ((mOrientation == ORIENTATION_TOP && viewCurrentY >= viewY && eventCurrentY >= eventY)
                            || (mOrientation == ORIENTATION_BELOW && viewCurrentY <= viewY && eventCurrentY <= eventY)) {
                        setY(viewY);
                    } else {
                        float y = (eventCurrentY - eventY) + viewCurrentY;
                        if (mOrientation == ORIENTATION_TOP) {
                            if (y > viewY) {
                                y = viewY;
                            } else if (y < (viewY - mDistance)) {
                                y = viewY - mDistance;
                            }
                        } else if (mOrientation == ORIENTATION_BELOW) {
                            if (y < viewY) {
                                y = viewY;
                            } else if (y > viewY + mDistance) {
                                y = viewY + mDistance;
                            }
                        } else {
                            if (y < viewY - mDistance) {
                                y = viewY - mDistance;
                            } else if (y > viewY + mDistance) {
                                y = viewY + mDistance;
                            }
                        }
                        setY(y);
                    }
                } else if (slidingDistanceY == mDistance) {
                    if ((mOrientation == ORIENTATION_TOP && eventCurrentY >= eventY)
                            || (mOrientation == ORIENTATION_BELOW && eventCurrentY <= eventY)) {
                        setY((eventCurrentY - eventY) + viewCurrentY);
                    } else {
                        if (viewCurrentY > viewY) {
                            setY(viewY + mDistance);
                        } else {
                            setY(viewY - mDistance);
                        }
                    }
                } else {
                    if (viewCurrentY > viewY) {
                        setY(viewY + mDistance);
                    } else {
                        setY(viewY - mDistance);
                    }
                }
                break;
            default:
                break;
            }
            break;
        case MotionEvent.ACTION_UP:
            viewCurrentX = getX();
            viewCurrentY = getY();

            float upX = viewCurrentX > viewX ? viewCurrentX - viewX : viewX - viewCurrentX;
            float upY = viewCurrentY > viewY ? viewCurrentY - viewY : viewY - viewCurrentY;
            switch (mOrientation) {
            case ORIENTATION_HORIZONTAL:
            case ORIENTATION_LEFT:
            case ORIENTATION_RIGHT:
                if (upX < mBoundaryPoint) {
                    mOverScroller.startScroll((int) getX(), (int) getY(), (int) viewX - (int) getX(), 0);
                    isEnd = false;
                } else {
                    if (getX() > viewX) {
                        mOverScroller.startScroll((int) getX(), (int) getY(), ((int) viewX + mDistance) - (int) getX(), 0);
                    } else {
                        mOverScroller.startScroll((int) getX(), (int) getY(), ((int) viewX - mDistance) - (int) getX(), 0);
                    }
                    setEnabled(false);
                    isEnd = true;
                }
                invalidate();
                break;
            case ORIENTATION_VERTICAL:
            case ORIENTATION_TOP:
            case ORIENTATION_BELOW:
                if (upY < mBoundaryPoint) {
                    mOverScroller.startScroll((int) getX(), (int) getY(), 0, (int) viewY - (int) getY());
                    isEnd = false;
                } else {
                    if (getY() > viewY) {
                        mOverScroller.startScroll((int) getX(), (int) getY(), 0, ((int) viewY + mDistance) - (int) getY());
                    } else {
                        mOverScroller.startScroll((int) getX(), (int) getY(), 0, ((int) viewY - mDistance) - (int) getY());
                    }
                    setEnabled(false);
                    isEnd = true;
                }
                invalidate();
                break;
            default:
                break;
            }
            float x = Math.abs(event.getX() - eventX);
            float y = Math.abs(event.getY() - eventY);
            if (x <= 5 && y <= 5) {
                if (mListener != null)
                    mListener.slideOver(this);
            }
            break;
        default:
            break;
        }
        return true;
    }

    boolean isEnd = false;

    @SuppressLint("NewApi")
    @Override
    public void computeScroll() {
        if (mOverScroller.computeScrollOffset()) {
            setX(mOverScroller.getCurrX());
            setY(mOverScroller.getCurrY());
            invalidate();
        } else {
            if (isEnd) {
                if (mListener != null)
                    mListener.slideOver(this);
            }else{
                if (mListener != null)
                    mListener.up(this);
            }
        }
    }

    /**
     * 滑动完毕监听
     * 
     * @author Ahlin
     * 
     */
    public interface SlideListener {
        void slideOver(View view);

        void down(View view);

        void up(View view);
    }
}
