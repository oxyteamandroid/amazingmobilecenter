package com.ingenic.mobilecenter.view;

import com.ingenic.mobilecenter.R;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;

public class InputView extends LinearLayout implements OnClickListener, View.OnLongClickListener {

    private Button[] mNumbers = new Button[12];
    private OnClickListener mClickListener;

    public InputView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_input, this);
    }

    public InputView(Context context) {
        this(context, null);
    }

    @SuppressLint("DefaultLocale")
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        View v1 = findViewById(R.id.num_line1);
        View v2 = findViewById(R.id.num_line2);
        View v3 = findViewById(R.id.num_line3);
        View v4 = findViewById(R.id.num_line4);
        mNumbers[1] = (Button) v1.findViewById(R.id.key1);
        mNumbers[2] = (Button) v1.findViewById(R.id.key2);
        mNumbers[3] = (Button) v1.findViewById(R.id.key3);

        mNumbers[4] = (Button) v2.findViewById(R.id.key1);
        mNumbers[5] = (Button) v2.findViewById(R.id.key2);
        mNumbers[6] = (Button) v2.findViewById(R.id.key3);

        mNumbers[7] = (Button) v3.findViewById(R.id.key1);
        mNumbers[8] = (Button) v3.findViewById(R.id.key2);
        mNumbers[9] = (Button) v3.findViewById(R.id.key3);

        mNumbers[10] = (Button) v4.findViewById(R.id.key1);
        mNumbers[0] = (Button) v4.findViewById(R.id.key2);
        mNumbers[11] = (Button) v4.findViewById(R.id.key3);
        
        mNumbers[0].setOnClickListener(this);
        mNumbers[0].setOnLongClickListener(this);
        String num = String.format("%d", 0);
        mNumbers[0].setText(num+" +");
        mNumbers[0].setTag(R.id.numbers_key, (char) (0 + '0'));
        
        for (int i = 1; i < 10; i++) {
            mNumbers[i].setOnClickListener(this);
            String number = String.format("%d", i);
            mNumbers[i].setText(number);
            mNumbers[i].setTag(R.id.numbers_key, (char) (i + '0'));
        }
        mNumbers[10].setOnClickListener(this);
        mNumbers[10].setText(R.string.nl);
        mNumbers[10].setTag(R.id.numbers_key, '*');
        mNumbers[11].setOnClickListener(this);
        mNumbers[11].setText(R.string.nr);
        mNumbers[11].setTag(R.id.numbers_key, '#');
    }


    @Override
    public void onClick(View view) {
        char val = (Character) view.getTag(R.id.numbers_key);
        if (val != 0 && mClickListener != null) {
            mClickListener.onClick(val);
        }
    }

    public void setOnClickListener(OnClickListener l) {
        mClickListener = l;
    }

    public interface OnClickListener {
        void onClick(char in);
    }

    @Override
    public boolean onLongClick(View view) {
        if (view.getId() == R.id.key2 && '0' == (Character) view.getTag(R.id.numbers_key)) {
            if (mClickListener != null) {
                mClickListener.onClick('+');
            }
        }
        return true;
    }

}
