/*
 * Copyright (C) 2015 Ingenic Semiconductor
 *
 * TaoZhang(Kevin)<tao.zhang@ingenic.com>
 *
 * Elf/iwds-ui-jar Project
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package com.ingenic.mobilecenter.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.ingenic.iwds.HardwareList;

/**
 * 是一个可以根据视图纵坐标进行缩放和平移处理的容器。
 * <p>
 * 主要用于在滑动AmazingListView系列列表控件时，对Item做缩放和平移处理。
 *
 * @author tZhang
 */
public class MatrixView extends LinearLayout {

    /**
     * 父控件高度
     */
    private float mParentHeight = 0;

    /**
     * 缩放因子
     */
    private float mFullScaleFactor = 1;

    private LayoutParams mParams;

    public MatrixView(Context context) {
        this(context, null);
    }

    public MatrixView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MatrixView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        WindowManager manager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics outMetrics = new DisplayMetrics();
        manager.getDefaultDisplay().getMetrics(outMetrics);
        // 设置默认的父容器高度为屏幕高度
        mParentHeight = outMetrics.heightPixels;
    }

    @SuppressWarnings("deprecation")
    @Override
    protected void dispatchDraw(Canvas canvas) {
        if(!HardwareList.IsCircularScreen()){
            super.dispatchDraw(canvas);
            return;
        }

        float halfOfWidth = getWidth() / 2f;
        float halfOfHeight = mParentHeight / 6f;
        float scale = calcuylateScale(getTop() + halfOfHeight, mParentHeight);

        canvas.save();
        Matrix m = canvas.getMatrix();
        // 以中心点进行缩放
        m.preTranslate(-halfOfWidth, -halfOfHeight);
        m.postScale(scale, scale);
        m.postTranslate(halfOfWidth, halfOfHeight);
        canvas.concat(m);
        super.dispatchDraw(canvas);
        canvas.restore();
    }

    /**
     * 计算缩放因子
     *
     * @param centerY
     *            纵向中心坐标
     * @param parentHeight
     *            父容器高度
     * @return 缩放因子
     */
    private float calcuylateScale(float centerY, float parentHeight) {
        return (1f - 1f / 2f * Math.abs((centerY - parentHeight / 2f))
                / (parentHeight / 2f))
                * mFullScaleFactor;
    }

    /**
     * 设置容器内容
     *
     * @param view
     */
    public void setContentView(View view) {
        if (getChildCount() > 0) {
            removeAllViews();
        }

        if (mParams == null) {
            mParams = new LayoutParams(LayoutParams.MATCH_PARENT,
                    LayoutParams.MATCH_PARENT);
        }

        addView(view, mParams);
    }

    /**
     * 设置父容器高度
     *
     * @param height
     */
    public void setParentHeight(float height) {
        mParentHeight = height;
    }

    /**
     * 设置缩放因子
     *
     * @param fullScaleFactor
     */
    public void setFullScaleFactor(float fullScaleFactor) {
        this.mFullScaleFactor = fullScaleFactor;
    }

}
