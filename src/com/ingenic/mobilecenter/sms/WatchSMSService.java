/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  YuanBanglin(Ahlin) <banglin.yuan@ingenic.com>
 *
 *  Elf/IDWS Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.mobilecenter.sms;

import java.util.ArrayList;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.app.Note;
import com.ingenic.iwds.app.NotificationProxyServiceManager;
import com.ingenic.iwds.common.api.ConnectFailedReason;
import com.ingenic.iwds.common.api.ServiceClient;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.elf.SmsInfo;
import com.ingenic.iwds.datatransactor.elf.SmsTransactionModel;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.mobilecenter.MobileCenterApplication;
import com.ingenic.mobilecenter.dao.SMSDao;
import com.ingenic.mobilecenter.utils.Utils;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Vibrator;

public class WatchSMSService extends Service implements SmsTransactionModel.SmsTransactionCallback, ServiceClient.ConnectionCallbacks {

    /**
     * 本地服务
     */
    private static ServiceClient sServiceClient;

    /**
     * 来电提醒Model
     */
    private static SmsTransactionModel sModel;

    /**
     * dao对象
     */
    private SMSDao mDao;

    /**
     * 通知管理
     */
    private NotificationProxyServiceManager mNotificationManager;

    /**
     * 通道可用监听
     */
    private ArrayList<ChannelAvailableListener> mListeners = new ArrayList<ChannelAvailableListener>();

    private ArrayList<SendSmsResultListener> mSendSmsListeners = new ArrayList<SendSmsResultListener>();

    /**
     * 是否需要短信提醒
     */
    private volatile boolean mIsFront = false;

    /**
     * 是否震动
     */
    private volatile boolean mVibratable = true;

    private static final int MSG_RESET_VIBRATABLE = 0;
    private Handler mHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
            case MSG_RESET_VIBRATABLE:
                mVibratable = true;
                break;
            default:
                break;
            }

            return false;
        }
    });

    @Override
    public IBinder onBind(Intent arg0) {
        return new SmsBinder(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (sServiceClient == null) {
            sServiceClient = new ServiceClient(this, ServiceManagerContext.SERVICE_NOTIFICATION_PROXY, this);
        }
        sServiceClient.connect();

        if (sModel == null) {
            sModel = ((MobileCenterApplication) getApplication()).getSmsModel(this);
        }
        sModel.start();

        IntentFilter filter = new IntentFilter(Utils.INENT_SEND_MESSAGE_REQUEST);
        registerReceiver(mBroadcastReceiver, filter);

        Notification.Builder builder = new Notification.Builder(this);
        Notification notification = builder.build();
        startForeground(9999, notification);

        mDao = SMSDao.getInstance(this);
    }

    @Override
    public void onDestroy() {
        if (sModel != null) {
            sModel.stop();
        }

        if (sServiceClient != null) {
            sServiceClient.disconnect();
        }

        if (mBroadcastReceiver != null)
            unregisterReceiver(mBroadcastReceiver);

        super.onDestroy();
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Utils.INENT_SEND_MESSAGE_REQUEST)) {
                SmsInfo info = intent.getExtras().getParcelable("SmsInfo");
                sendSms(info);
            }
        }
    };

    /**
     * 注册通道是否可用监听
     * 
     * @param listener
     */
    private void registerChannelAvailableListener(ChannelAvailableListener listener) {
        if (mListeners.contains(listener)) {
            IwdsLog.w(this, "This callback is aleady registered.Ignore!");
            return;
        }

        synchronized (mListeners) {
            mListeners.add(listener);
        }
    }

    /**
     * 注册通道是否可用监听
     * 
     * @param listener
     */
    private void unregisterChannelAvailableListener(ChannelAvailableListener listener) {
        if (!mListeners.contains(listener)) {
            return;
        }

        synchronized (mListeners) {
            mListeners.remove(listener);
        }
    }

    /**
     * 注册通道是否可用监听
     * 
     * @param listener
     */
    private void registerSendSmsResultListener(SendSmsResultListener listener) {
        if (mSendSmsListeners.contains(listener)) {
            return;
        }

        synchronized (mSendSmsListeners) {
            mSendSmsListeners.add(listener);
        }
    }

    /**
     * 注册通道是否可用监听
     * 
     * @param listener
     */
    private void unregisterSendSmsResultListener(SendSmsResultListener listener) {
        if (!mSendSmsListeners.contains(listener)) {
            return;
        }

        synchronized (mSendSmsListeners) {
            mSendSmsListeners.remove(listener);
        }
    }

    /**
     * 设置是否短信提醒
     * 
     * @param isFront
     */
    private void setIsFront(boolean isFront) {
        this.mIsFront = isFront;
    }

    /**
     * 清除通知
     */
    private void clearNotifications() {
        if (mNotificationManager != null) {
            Note note = new Note("Clear By PackageName", "");
            mNotificationManager.notify(-1, note);
        }
    }

    /**
     * 发送短信
     * 
     * @param info
     * @return
     */
    private void sendSms(SmsInfo info) {
        if (sModel == null)
            return;
        sModel.send(info);
    }

    @Override
    public void onRequest() {

    }

    @Override
    public void onRequestFailed() {

    }

    @Override
    public void onObjectArrived(SmsInfo info) {
        IwdsLog.d(this, "onObjectArrived:" + info);

        mDao.insert(info);

        for (int i = mListeners.size() - 1; i >= 0; i--) {
            mListeners.get(i).onSmsReceived(info);
        }

        if (!mIsFront && info.getType() == 1) {
            // 播放提示音
            RingtoneManager.getRingtone(getApplicationContext(), RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)).play();

            // 震动一下
            if (mVibratable) {
                Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
                vibrator.vibrate(new long[] { 0, 500, 1000 }, -1);
                mVibratable = false;
                mHandler.sendEmptyMessageDelayed(MSG_RESET_VIBRATABLE, 5000);
            }

            if (mNotificationManager != null) {
                Intent it = new Intent(this, SMSListActivity.class);
                PendingIntent intent = PendingIntent.getActivity(this, 0, it, 0);
                String person = info.getPerson();
                String title = person == null ? info.getAddress() : person;
                Note note = new Note(title, info.getBody(), intent);
                mNotificationManager.notify(info.getId(), note);
            }
        }

    }

    @Override
    public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {

    }

    @Override
    public void onChannelAvailable(boolean isAvailable) {
        for (ChannelAvailableListener listener : mListeners) {
            listener.onChannelAvailable(isAvailable);
        }

        SharedPreferences sp = getSharedPreferences(Utils.PREFERENCE_BOND, Context.MODE_PRIVATE);
        sp.edit().putBoolean(Utils.AVAILABLE_SMS, isAvailable);
    }

    @Override
    public void onSendResult(DataTransactResult result) {
        if (result.getResultCode() != DataTransactResult.RESULT_OK) {
            if (Utils.isAvailableSms(this)) {
                if (result.getTransferedObject() instanceof SmsInfo)
                    sendSms((SmsInfo) result.getTransferedObject());
            }
        }
    }

    @Override
    public void onConnected(ServiceClient serviceClient) {
        mNotificationManager = (NotificationProxyServiceManager) serviceClient.getServiceManagerContext();
    }

    @Override
    public void onDisconnected(ServiceClient serviceClient, boolean unexpected) {
        mNotificationManager = null;
    }

    @Override
    public void onConnectFailed(ServiceClient serviceClient, ConnectFailedReason reason) {

    }

    interface ChannelAvailableListener {
        void onSmsReceived(SmsInfo info);

        void onChannelAvailable(boolean isConnected);
    }

    interface SendSmsResultListener {
        void sendOk(SmsInfo info);

        void sendFailure(SmsInfo info);
    }

    public static class SmsBinder extends Binder {
        private WatchSMSService mService;

        private SmsBinder(WatchSMSService service) {
            mService = service;
        }

        public void registerChannelAvailableListener(ChannelAvailableListener listener) {
            if (mService != null) {
                mService.registerChannelAvailableListener(listener);
            }
        }

        public void unregisterChannelAvailableListener(ChannelAvailableListener listener) {
            if (mService != null) {
                mService.unregisterChannelAvailableListener(listener);
            }
        }

        public void registerSendSmsResultListener(SendSmsResultListener listener) {
            if (mService != null) {
                mService.registerSendSmsResultListener(listener);
            }
        }

        public void unregisterSendSmsResultListener(SendSmsResultListener listener) {
            if (mService != null) {
                mService.unregisterSendSmsResultListener(listener);
            }
        }

        public void setIsFront(boolean isFront) {
            if (mService != null) {
                mService.setIsFront(isFront);
            }
        }

        public void clearNotifications() {
            if (mService != null) {
                mService.clearNotifications();
            }
        }

        public void sendSms(SmsInfo info) {
            if (mService != null) {
                mService.sendSms(info);
            }
        }
    }
}
