/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  YuanBanglin(Ahlin) <banglin.yuan@ingenic.com>
 *
 *  Elf/IDWS Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.mobilecenter.sms;

import com.ingenic.iwds.app.AmazingDialog;
import com.ingenic.iwds.datatransactor.elf.SmsInfo;
import com.ingenic.iwds.widget.AmazingListView;
import com.ingenic.mobilecenter.R;
import com.ingenic.mobilecenter.call.TelephoneActivity;
import com.ingenic.mobilecenter.sms.SmsDetailActivity.SmsDetail;
import com.ingenic.mobilecenter.utils.Utils;

import android.app.Activity;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

public class ReplySMSFragment extends Fragment implements View.OnClickListener {
    public ReplySMSFragment() {
    }

    /**
     * 快捷短信ListView
     */
    private ListView mListView;

    /**
     * 圆屏短信Listview
     */
    private AmazingListView mAmazingListView;

    /**
     * 表情回复短信按钮
     */
    private ImageButton mExpressionImageButton;

    /**
     * 文本回复短信按钮
     */
    private ImageButton mTextSMSImageButton;

    /**
     * 快捷短信Adapter
     */
    private ArrayAdapter<String> mAdapter;

    /**
     * 所属Activity
     */
    private Activity mActivity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
        if (!Utils.isAvailable(mActivity)) {
            showIsAvailableDialog();
        }
        if (mAdapter == null) {
            if (Utils.isRound()) {
                mAdapter = new ArrayAdapter<String>(activity, R.layout.item_reply_sms_round, android.R.id.text1);
            } else {
                mAdapter = new ArrayAdapter<String>(activity, R.layout.item_reply_sms, android.R.id.text1);
            }
        }
        configSms();
    }

    private AmazingDialog isAvailableDialog = null;

    private void showIsAvailableDialog() {

        if (isAvailableDialog == null) {
            isAvailableDialog = new AmazingDialog(mActivity).setRightScrollEnable(false);
            isAvailableDialog.setCancelable(false);
            isAvailableDialog.setContent(R.string.connect_disconnect);
            isAvailableDialog.setPositiveButton(R.drawable.ic_cancel, new OnClickListener() {

                @Override
                public void onClick(View v) {
                    isAvailableDialog.dismiss();
                    isAvailableDialog = null;
                    mActivity.onBackPressed();
                }
            });
        }
        isAvailableDialog.show();
    }

    /**
     * 获取快捷短信
     */
    private void configSms() {
        mAdapter.clear();
        SharedPreferences sp = getActivity().getSharedPreferences(Utils.PREFERENCE_BOND, 0);
        for (int i = 0; i < 4; i++) {
            String key = "sms_" + i;
            if (sp.contains(key)) {
                String sms = sp.getString(key, null);
                if (sms != null) {
                    mAdapter.add(sms);
                }
            }
        }
        if (Utils.isRound())
            mAdapter.add("");
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = null;
        if (Utils.isRound()) {
            view = inflater.inflate(R.layout.fragment_reply_sms_round, container, false);
        } else {
            view = inflater.inflate(R.layout.fragment_reply_sms, container, false);
        }
        initViews(view);
        return view;
    }

    /**
     * 初始化控件
     * 
     * @param view
     */
    private void initViews(View view) {
        if (Utils.isRound()) {
            mAmazingListView = (AmazingListView) view.findViewById(R.id.reply_sms_list);
            mAmazingListView.setDividerHeight(1);
            mAmazingListView.setAdapter(mAdapter);
            mAmazingListView.setSelector(R.drawable.text_fragment_reply_sms_item);
            mAmazingListView.setOnItemClickListener(new com.ingenic.iwds.widget.AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(com.ingenic.iwds.widget.AdapterView<?> parent, View view, int position, long id) {
                    String sms = (String) parent.getItemAtPosition(position);
                    if (mActivity instanceof TelephoneActivity) {
                        TelephoneActivity activity = (TelephoneActivity) mActivity;
                        receiveSMS(activity, sms, activity.number);
                    } else if (mActivity instanceof SmsReplyActivity) {// 短信回复
                        SmsReplyActivity activity = (SmsReplyActivity) mActivity;
                        SmsInfo info = new SmsInfo();
                        info.setBody(sms);
                        info.setAddress(activity.getAddress());
                        info.setDate(System.currentTimeMillis());
                        SmsDetail.sendSms(info);
                    }
                    mActivity.finish();
                }
            });
            final LinearLayout layout = (LinearLayout) view.findViewById(R.id.buttons);
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) layout.getLayoutParams();
            params.height = Utils.sScreenHeight / 3;
        } else {
            mListView = (ListView) view.findViewById(R.id.reply_sms_list);
            mListView.setSelector(R.drawable.text_fragment_reply_sms_item);
            mListView.setAdapter(mAdapter);
            mListView.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String sms = (String) parent.getItemAtPosition(position);
                    if (mActivity instanceof TelephoneActivity) {// 来电快捷短信回复
                        TelephoneActivity TActivity = (TelephoneActivity) mActivity;
                        receiveSMS(TActivity, sms, TActivity.number);
                    } else if (mActivity instanceof SmsReplyActivity) {// 短信回复
                        SmsReplyActivity SRActivity = (SmsReplyActivity) mActivity;
                        SmsInfo info = new SmsInfo();
                        info.setBody(sms);
                        info.setAddress(SRActivity.getAddress());
                        info.setDate(System.currentTimeMillis());
                        SmsDetail.sendSms(info);
                    }
                    mActivity.finish();
                }
            });
        }
        mExpressionImageButton = (ImageButton) view.findViewById(R.id.reply_sms_expression);
        mExpressionImageButton.setOnClickListener(this);
        mTextSMSImageButton = (ImageButton) view.findViewById(R.id.reply_sms_text);
        mTextSMSImageButton.setOnClickListener(this);
    }

    /**
     * 短信回复
     * 
     * @param sms
     * @param number
     */
    private void receiveSMS(TelephoneActivity activity, String sms, String number) {
        if (activity != null) {
            activity.receiveSMS(sms, number);
            activity.onBackPressed();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.reply_sms_expression:
            SmsReplyActivity activity = (SmsReplyActivity) mActivity;
            EmojiconGridFragment emojiFragment = new EmojiconGridFragment();
            activity.showFragment(emojiFragment, "emojicon_grid");
            break;
        case R.id.reply_sms_text:
            SmsReplyActivity SRActivity = (SmsReplyActivity) mActivity;
            VoiceReplySmsFragment voiceFragment = new VoiceReplySmsFragment();
            SRActivity.showFragment(voiceFragment, "voice_reply");
            break;
        default:
            break;
        }
    }

}
