package com.ingenic.mobilecenter.sms;

import com.ingenic.iwds.common.api.ConnectFailedReason;
import com.ingenic.iwds.common.api.ServiceClient;
import com.ingenic.iwds.common.api.ServiceClient.ConnectionCallbacks;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.smartspeech.RemoteSpeechConstant;
import com.ingenic.iwds.smartspeech.RemoteSpeechErrorCode;
import com.ingenic.iwds.smartspeech.RemoteSpeechRecognizer;
import com.ingenic.iwds.smartspeech.RemoteSpeechRecognizer.RemoteRecognizerListener;
import com.ingenic.iwds.smartspeech.RemoteSpeechServiceManager;
import com.ingenic.iwds.smartspeech.RemoteStatusListener;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.mobilecenter.R;
import com.ingenic.mobilecenter.utils.Utils;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class VoiceReplySmsFragment extends Fragment implements View.OnClickListener, RemoteRecognizerListener, RemoteStatusListener,
        ConnectionCallbacks {

    /**
     * 录音动画控件
     */
    private ImageView mVoiceAnimImageView;

    /**
     * 录音结果控件
     */
    private TextView mVoiceTextTextView;

    /**
     * 取消控件
     */
    private Button mVoiceCancelButton;

    /**
     * 发送按钮父控件
     */
    private FrameLayout sendFrameLayout;

    /**
     * 发送控件
     */
    private Button mVoiceSendButton;

    /**
     * 远程语音识别对象
     */
    private RemoteSpeechRecognizer mRecognizer;

    /**
     * 服务端
     */
    private ServiceClient mClient;

    /**
     * 远程语音服务管理对象
     */
    private RemoteSpeechServiceManager mService;

    /**
     * 是否取消录音
     */
    private boolean isCancel = false;

    @Override
    public void onAttach(Activity activity) {
        IwdsLog.d(this, "onAttach");
        super.onAttach(activity);

        mClient = new ServiceClient(activity, ServiceManagerContext.SERVICE_REMOTE_SPEECH, this);
        mClient.connect();
    }

    @Override
    public void onResume() {
        IwdsLog.d(this, "onResume");
        super.onResume();
    }

    @Override
    public void onConnected(ServiceClient serviceClient) {
        IwdsLog.d(this, "ServiceClient onConnected");
        mRecognizer = RemoteSpeechRecognizer.getInstance();
        mRecognizer.setListener(this);

        mService = (RemoteSpeechServiceManager) serviceClient.getServiceManagerContext();

        mService.registerRemoteStatusListener(this);

    }

    @Override
    public void onDisconnected(ServiceClient serviceClient, boolean unexpected) {
        IwdsLog.d(this, "ServiceClient onDisconnected");
        if (!isCancel)
            mService.requestCancelRecognize(mRecognizer);

        mService.unregisterRemoteStatusListener(this);
    }

    @Override
    public void onConnectFailed(ServiceClient serviceClient, ConnectFailedReason reason) {
        IwdsLog.d(this, "ServiceClient onConnectFailed");
        getActivity().finish();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_voice_reply_sms, container, false);
        initViews(view);
        return view;
    }

    private void initViews(View view) {
        mVoiceAnimImageView = (ImageView) view.findViewById(R.id.voice_animation);
        mVoiceTextTextView = (TextView) view.findViewById(R.id.voice_text);
        mVoiceTextTextView.setText(Utils.formatTime(time));
        mHandler.sendEmptyMessageDelayed(MAG_0, 1000);
        mVoiceCancelButton = (Button) view.findViewById(R.id.voice_cancel);
        mVoiceCancelButton.setOnClickListener(this);
        sendFrameLayout = (FrameLayout) view.findViewById(R.id.voice_send_framelayout);
        mVoiceSendButton = (Button) view.findViewById(R.id.voice_send);
        mVoiceSendButton.setOnClickListener(this);

    }

    private static final int MAG_0 = 0;
    private static final int MAG_1 = 1;
    private static final int MAG_2 = 2;

    private int time = 0;
    private Handler mHandler = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
            case MAG_0:
                time++;
                mVoiceTextTextView.setText(Utils.formatTime(time));
                mHandler.sendEmptyMessageDelayed(MAG_0, 1000);
                break;
            case MAG_1:
                if (mHandler.hasMessages(MAG_0))
                    mHandler.removeMessages(MAG_0);
                mVoiceAnimImageView.setVisibility(View.GONE);
                sendFrameLayout.setVisibility(View.VISIBLE);
                mVoiceTextTextView.setText((String) msg.obj);
                break;
            case MAG_2:

                break;
            default:
                break;
            }
            return false;
        }
    });

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.voice_cancel:
            if (!isCancel)
                mService.requestCancelRecognize(mRecognizer);
            getActivity().finish();
            break;
        case R.id.voice_send:

            break;
        default:
            break;
        }
    }

    /**
     * 设置语音识别参数
     */
    private void setParameters() {
        mRecognizer.clearParameter();
        mRecognizer.setParameter(RemoteSpeechConstant.ENGINE_TYPE, "cloud");
        mRecognizer.setParameter(RemoteSpeechConstant.LANGUAGE, "zh_cn");
        mRecognizer.setParameter(RemoteSpeechConstant.DOMAIN, "iat");
        mRecognizer.setParameter(RemoteSpeechConstant.ACCENT, "mandarin");
        mRecognizer.setParameter(RemoteSpeechConstant.VAD_BOS, "4000");
        mRecognizer.setParameter(RemoteSpeechConstant.VAD_EOS, "1000");
    }

    /**
     * 录音结果
     */
    private String mSmsResult = new String();

    private String str = "";

    @Override
    public void onListeningStatus(boolean isListening) {

    }

    @Override
    public void onBeginOfSpeech() {

    }

    @Override
    public void onEndOfSpeech() {

    }

    @Override
    public void onCancel() {
        IwdsLog.d(this, "onCancel");
        isCancel = true;
    }

    @Override
    public void onError(int errorCode) {
        IwdsLog.d(this, "onError:" + errorCode);
        if (errorCode != 0)
            getActivity().finish();
    }

    @Override
    public void onResult(String result, boolean isLast) {
        IwdsLog.d(this, "onResult:" + result + isLast);
        str += result;
        if (isLast) {
            mSmsResult = str;
            mHandler.obtainMessage(MAG_1, mSmsResult).sendToTarget();
            str = "";
        }
    }

    @Override
    public void onVolumeChanged(int volume) {

    }

    @Override
    public void onAvailable(int errorCode) {
        IwdsLog.d(this, "onAvailable" + errorCode);
        if (errorCode != RemoteSpeechErrorCode.SUCCESS) {
            getActivity().finish();
        } else {
            setParameters();
            mService.requestStartRecognize(mRecognizer);
            isCancel = false;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (!isCancel)
            mService.requestCancelRecognize(mRecognizer);

        mService.unregisterRemoteStatusListener(this);
    }

}
