package com.ingenic.mobilecenter.sms;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ingenic.iwds.app.AmazingDialog;
import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.iwds.datatransactor.elf.SmsInfo;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.iwds.widget.AdapterView;
import com.ingenic.iwds.widget.AmazingSwipeListView;
import com.ingenic.iwds.widget.AmazingToast;
import com.ingenic.mobilecenter.MobileCenterApplication;
import com.ingenic.mobilecenter.R;
import com.ingenic.mobilecenter.TimeChangeListener;
import com.ingenic.mobilecenter.dao.SMSDao;
import com.ingenic.mobilecenter.sms.WatchSMSService.ChannelAvailableListener;
import com.ingenic.mobilecenter.sms.WatchSMSService.SmsBinder;
import com.ingenic.mobilecenter.utils.Utils;

public class SMSListActivity extends RightScrollActivity implements ServiceConnection, ChannelAvailableListener, AdapterView.OnItemClickListener,
        AmazingSwipeListView.OnItemDeleteListener, TimeChangeListener {

    /**
     * 列表
     */
    private AmazingSwipeListView mListView;

    /**
     * SMS服务代理对象
     */
    private SmsBinder mBinder;

    /**
     * 访问数据层对象 (DAO)
     */
    private SMSDao mDao;

    /**
     * 适配器
     */
    private SmsAddressAdapter mAdapter;

    /**
     * 删除Dialog
     */
    private AmazingDialog mDeleteDialog;

    /**
     * 返回控件
     */
    private TextView mBackTextView;

    /**
     * 时间控件
     */
    private TextView mTimeTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Utils.isRound()) {
            setContentView(R.layout.activity_smss_round);
        } else {
            setContentView(R.layout.activity_smss);
        }

        ((MobileCenterApplication) getApplication()).registerListener(this);

        bindService(new Intent(this, WatchSMSService.class), this, BIND_AUTO_CREATE);

        mDao = SMSDao.getInstance(this);

        initViews();

        loadSms();

        mDeleteDialog = new AmazingDialog(this).setNegativeButton(AmazingDialog.USE_NORMAL_DRAWABLE, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDeleteDialog.dismiss();
            }
        });
    }

    private LinearLayout headerLayout;

    private void initViews() {
        mListView = (AmazingSwipeListView) findViewById(R.id.sms_list);
        mListView.setEmptyView(findViewById(android.R.id.empty));
        if (Utils.isRound()) {
            headerLayout = (LinearLayout) findViewById(R.id.header);
            headerLayout.getLayoutParams().height = Utils.sScreenHeight / 3;
            mListView.setDivider(new ColorDrawable(getResources().getColor(R.color.color_grey_text)));
            mListView.setDividerHeight(1);
            mListView.setSelector(R.drawable.btn_activity_main_round);
            mAdapter = new SmsAddressAdapter(this, R.layout.item_sms_round);

            TextView headerView = new TextView(this);
            mListView.addHeaderView(headerView);
            TextView footerView = new TextView(this);
            mListView.addFooterView(footerView);
        } else {
            mListView.setDividerHeight(10);
            mListView.setSelector(R.drawable.btn_round_angle);
            mAdapter = new SmsAddressAdapter(this, R.layout.item_sms);
        }
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(this);
        mListView.setOnItemDeleteListener(this);

        mBackTextView = (TextView) findViewById(R.id.sms_list_back);
        mBackTextView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mTimeTextView = (TextView) findViewById(R.id.sms_list_time);
        mTimeTextView.setText(Utils.getTime());
    }

    /**
     * 加载SMS
     */
    private void loadSms() {
        mAdapter.clear();
        List<SmsAddress> addresses = mDao.getNumbers();
        if (addresses != null) {
            mAdapter.addAll(addresses);
        }
        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        unbindService(this);

        if (mBinder != null) {
            mBinder.setIsFront(false);
            mBinder.unregisterChannelAvailableListener(this);
            mBinder = null;
        }

        super.onDestroy();
    }

    /**
     * 绑定服务 连接
     */
    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        mBinder = (SmsBinder) service;
        mBinder.clearNotifications();
        mBinder.setIsFront(true);
        mBinder.registerChannelAvailableListener(this);
    }

    /**
     * 绑定服务 断开
     */
    @Override
    public void onServiceDisconnected(ComponentName name) {
        if (mBinder != null) {
            mBinder.setIsFront(false);
            mBinder.unregisterChannelAvailableListener(this);
            mBinder = null;
        }
    }

    /**
     * 接收到SMS
     */
    @Override
    public void onSmsReceived(SmsInfo info) {
        loadSms();
    }

    /**
     * 通道可用改变
     */
    @Override
    public void onChannelAvailable(boolean isConnected) {
    }

    /**
     * 适配器
     * 
     * @author yuanbanglin
     * 
     */
    private static class SmsAddressAdapter extends ArrayAdapter<SmsAddress> {

        private Context mContext;
        private int mResource;

        public SmsAddressAdapter(Context context, int resource) {
            super(context, resource);
            mContext = context;
            mResource = resource;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(mResource, parent, false);

                holder = new ViewHolder();
                holder.address = (TextView) convertView.findViewById(R.id.item_sms_address);
                holder.sms = (TextView) convertView.findViewById(R.id.item_sms);
                holder.date = (TextView) convertView.findViewById(R.id.item_sms_date);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            SmsAddress address = getItem(position);
            final String addr = address.getAddress();

            holder.address.setText(address.name != null ? address.name : addr);
            holder.sms.setText(address.lastSms);
            holder.sms.setTextColor(address.hasUnread ? mContext.getResources().getColor(android.R.color.holo_blue_light) : mContext.getResources()
                    .getColor(R.color.color_grey_text));

            long time = address.date;
            if (Utils.isYesterday(time)) {
                holder.date.setText(R.string.yesterday);
            } else {
                String template = Utils.isToday(time) ? "HH:mm" : "MM-dd";
                Date date = new Date(time);
                SimpleDateFormat format = new SimpleDateFormat(template, Locale.ENGLISH);
                holder.date.setText(format.format(date));
            }

            return convertView;
        }

        private static class ViewHolder {
            TextView address;
            TextView sms;
            TextView date;
        }
    }

    /**
     * item点击
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        getRightScrollView().disableRightScroll();

        SmsAddress address = mAdapter.getItem(position);
        Intent it = new Intent(this, SmsDetailActivity.class);
        it.putExtra("address", address);
        startActivity(it);

        mDao.readSms(address.getAddress());
        loadSms();
        parent.setSelection(position);
    }

    /**
     * ListView删除item
     */
    @Override
    public void onDelete(View view, int position) {
        if (mAdapter != null && mAdapter.getCount() > position && position >= 0) {
            final SmsAddress smsAddress = mAdapter.getItem(position);
            final String address = smsAddress.getAddress();
            final String addressName = smsAddress.name != null ? smsAddress.name : address;

            mDeleteDialog.setContent(String.format(getResources().getString(R.string.delete_tip), addressName))
                    .setPositiveButton(AmazingDialog.USE_NORMAL_DRAWABLE, new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            int result = mDao.deleteByAddress(address);

                            if (-1 != result) {
                                IwdsLog.d(SMSListActivity.this, "Delete Item info = " + addressName);
                                mAdapter.remove(smsAddress);
                                mAdapter.notifyDataSetChanged();
                                AmazingToast.showToast(getApplicationContext(), R.string.delete_success, AmazingToast.LENGTH_SHORT,
                                        AmazingToast.BOTTOM_CENTER);
                            } else {
                                IwdsLog.d(SMSListActivity.this, "Delete failed!");
                                AmazingToast.showToast(getApplicationContext(), R.string.delete_failed, AmazingToast.LENGTH_SHORT,
                                        AmazingToast.BOTTOM_CENTER);
                            }
                            mDeleteDialog.dismiss();
                        }
                    }).show();
        } else {
            IwdsLog.d(SMSListActivity.this, "Delete failed!");
            AmazingToast.showToast(getApplicationContext(), R.string.delete_failed, AmazingToast.LENGTH_SHORT, AmazingToast.BOTTOM_CENTER);
        }
    }

    /**
     * 时间改变回调(单位: 分钟)
     */
    @Override
    public void onTimeChange() {
        if (mTimeTextView != null)
            mTimeTextView.setText(Utils.getTime());
    }
}
