package com.ingenic.mobilecenter.sms;

import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.iwds.datatransactor.elf.SmsInfo;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.mobilecenter.MobileCenterApplication;
import com.ingenic.mobilecenter.R;
import com.ingenic.mobilecenter.TimeChangeListener;
import com.ingenic.mobilecenter.sms.EmojiconGridFragment.OnEmojiconClickedListener;
import com.ingenic.mobilecenter.utils.Utils;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;
import android.widget.TextView;

public class SmsReplyActivity extends RightScrollActivity implements TimeChangeListener, OnEmojiconClickedListener {

    /**
     * 时间控件
     */
    private TextView mTimeTextView;

    /**
     * 对方号码
     */
    private String mAddress;

    /**
     * 获取对方号码
     * 
     * @return 对方号码
     */
    public String getAddress() {
        return mAddress;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Utils.isRound()) {
            setContentView(R.layout.activity_sms_reply_round);
        } else {
            setContentView(R.layout.activity_sms_reply);
            initViews();
            ((MobileCenterApplication) getApplication()).registerListener(this);
        }

        mAddress = getIntent().getStringExtra(Utils.SMS_NUMBER);

        Intent it = new Intent(this, WatchSMSService.class);
        bindService(it, mWatchSmsConn, BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mWatchSmsConn);
    }

    /**
     * 初始化控件
     */
    private void initViews() {
        mTimeTextView = (TextView) findViewById(R.id.sms_reply_time);
        mTimeTextView.setText(Utils.getTime());
    }

    /**
     * 初始化Fragment
     */
    private void initFragment() {
        Fragment mFragment = new ReplySMSFragment();
        showFragment(mFragment, null);
    }

    public void showFragment(Fragment fragment, String tag) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        if (TextUtils.isEmpty(tag)) {
            tag = "sms_reply";
        }
        ft.replace(R.id.sms_reply_fragment, fragment, tag);
        ft.commitAllowingStateLoss();
    }

    /**
     * 短信服务代理对象
     */
    public WatchSMSService.SmsBinder smsBinder;

    /**
     * 短信服务连接对象
     */
    private ServiceConnection mWatchSmsConn = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            IwdsLog.i(this, "onServiceDisconnected WatchSmsService");
            if (smsBinder != null) {
                smsBinder = null;
            }
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder binder) {
            IwdsLog.i(this, "onServiceConnected WatchSmsService");
            smsBinder = (WatchSMSService.SmsBinder) binder;
            initFragment();
        }
    };

    /**
     * 时间改变回调
     */
    @Override
    public void onTimeChange() {
        if (!Utils.isRound())
            if (mTimeTextView != null)
                mTimeTextView.setText(Utils.getTime());
    }

    @Override
    public void onEmojiconClicked(Emojicon emojicon) {
        // 发送表情
        if (smsBinder != null) {
            SmsInfo info = new SmsInfo();
            if (TextUtils.isEmpty(mAddress))
                return;
            info.setAddress(mAddress);
            info.setBody(emojicon.getEmoji());
            info.setDate(System.currentTimeMillis());
            info.setProtocol(0);
            info.setType(2);
            smsBinder.sendSms(info);
            finish();
        }
    }
}
