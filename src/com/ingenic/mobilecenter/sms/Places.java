/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  YuanBanglin(Ahlin) <banglin.yuan@ingenic.com>
 *
 *  Elf/IDWS Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.mobilecenter.sms;

/**
 * 
 * @author Ahlin
 *
 */
public class Places {
    public static final Emojicon[] DATA = new Emojicon[] { 
            Emojicon.fromCodePoint(0x1f604),
            Emojicon.fromCodePoint(0x1f60a),
            Emojicon.fromChar((char) 0x263a), 
            Emojicon.fromCodePoint(0x1f609), 
            Emojicon.fromCodePoint(0x1f60d), 
            Emojicon.fromCodePoint(0x1f618),
            Emojicon.fromCodePoint(0x1f61c), 
            Emojicon.fromCodePoint(0x1f633),
            Emojicon.fromCodePoint(0x1f601), 
            Emojicon.fromCodePoint(0x1f614),
            Emojicon.fromCodePoint(0x1f60c), 
            Emojicon.fromCodePoint(0x1f612), 
            Emojicon.fromCodePoint(0x1f602), 
            Emojicon.fromCodePoint(0x1f62d),
            Emojicon.fromCodePoint(0x1f629), 
            Emojicon.fromCodePoint(0x1f60b), 
            Emojicon.fromCodePoint(0x1f60e), 
            Emojicon.fromCodePoint(0x1f634),
            Emojicon.fromCodePoint(0x1f60f), 
            Emojicon.fromCodePoint(0x1f611), 
            Emojicon.fromCodePoint(0x1f648), 
            Emojicon.fromCodePoint(0x1f440),
            Emojicon.fromCodePoint(0x1f44d), 
            Emojicon.fromCodePoint(0x1f44c), 
            Emojicon.fromChar((char) 0x270c), 
            Emojicon.fromCodePoint(0x1f44f),
            Emojicon.fromCodePoint(0x1f481), 
            Emojicon.fromChar((char) 0x2764), 
            Emojicon.fromCodePoint(0x1f495), 
            Emojicon.fromCodePoint(0x1f3b6), };
}
