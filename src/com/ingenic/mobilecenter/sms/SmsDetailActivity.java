/*
 *  Copyright (C) 2015 Ingenic Semiconductor
 *
 *  YuanBanglin(Ahlin) <banglin.yuan@ingenic.com>
 *
 *  Elf/IDWS Project
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by the
 *  Free Software Foundation; either version 2 of the License, or (at your
 *  option) any later version.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */
package com.ingenic.mobilecenter.sms;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ingenic.iwds.app.AmazingDialog;
import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.iwds.datatransactor.elf.SmsInfo;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.mobilecenter.MobileCenterApplication;
import com.ingenic.mobilecenter.R;
import com.ingenic.mobilecenter.TimeChangeListener;
import com.ingenic.mobilecenter.call.CallFargment;
import com.ingenic.mobilecenter.call.TelephoneActivity;
import com.ingenic.mobilecenter.dao.SMSDao;
import com.ingenic.mobilecenter.sms.WatchSMSService.ChannelAvailableListener;
import com.ingenic.mobilecenter.sms.WatchSMSService.SendSmsResultListener;
import com.ingenic.mobilecenter.sms.WatchSMSService.SmsBinder;
import com.ingenic.mobilecenter.utils.Utils;
import com.ingenic.mobilecenter.view.EmojiconTextView;

public class SmsDetailActivity extends RightScrollActivity implements ServiceConnection, ChannelAvailableListener, View.OnClickListener,
        SendSmsResultListener, TimeChangeListener {

    public static class SmsDetail {
        private static SmsDetailActivity mActivity;

        public static void sendSms(SmsInfo info) {
            mActivity.sendSms(info);
        }
    }

    /**
     * 适配器
     */
    private SmsAdapter mAdapter;

    /**
     * 数据访问对象
     */
    private SMSDao mDao;

    /**
     * SMS服务代理对象
     */
    private WatchSMSService.SmsBinder mBinder;

    /**
     * 对方号码
     */
    private String mAddress;

    /**
     * 列表控件
     */
    private ListView mListView;

    /**
     * 返回控件
     */
    private TextView mBackTextView;

    /**
     * 呼叫控件
     */
    private ImageView mDialImageView;

    /**
     * 回复控件
     */
    private Button mReplyImageView;
    
    /**
     * 时间控件
     */
    private TextView mTimeTextView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SmsDetail.mActivity = this;
        if (Utils.isRound()) {
            setContentView(R.layout.activity_sms_detail_round);
        } else {
            setContentView(R.layout.activity_sms_detail);
        }
        SmsAddress address = (SmsAddress) getIntent().getSerializableExtra("address");
        if (address == null) {
            finish();
            return;
        }

        mAddress = address.getAddress();
        mDao = SMSDao.getInstance(this);

        mAdapter = new SmsAdapter(this, R.layout.item_sms_detail);
        mAdapter.addAll(mDao.getSmssWithNumber(mAddress));
        
        initViews();
        
        ((MobileCenterApplication) getApplication()).registerListener(this);
        
        mBackTextView.setText((address.name == null || address.name.isEmpty()) ? address.getAddress() : address.name);

        bindService(new Intent(this, WatchSMSService.class), this, BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {
        unbindService(this);

        if (mBinder != null) {
            mBinder.unregisterChannelAvailableListener(this);
            mBinder.unregisterSendSmsResultListener(this);
            mBinder = null;
        }

        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mBackTextView != null)
            mBackTextView.setEnabled(true);
        if (mDialImageView != null)
            mDialImageView.setEnabled(true);
        if (mReplyImageView != null)
            mReplyImageView.setEnabled(true);
    }

    /**
     * 初始化控件
     */
    private void initViews() {
        mListView = (ListView) findViewById(android.R.id.list);
        mListView.setAdapter(mAdapter);
        
        if(Utils.isRound()){
            mTimeTextView = (TextView)findViewById(R.id.sms_detail_time);
            mTimeTextView.setText(Utils.getTime());
        }

        TextView headerView = new TextView(this);
        headerView.setText(R.string.sms_or_mms);
        headerView.setTextColor(getResources().getColor(R.color.color_grey_text));
        headerView.setTextSize(15);
        headerView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        headerView.setGravity(Gravity.CENTER_HORIZONTAL);

        mListView.addHeaderView(headerView);
        mListView.setSelection(mAdapter.getCount() - 1);// 显示最后一个item

        mBackTextView = (TextView) findViewById(R.id.sms_detail_back);
        mBackTextView.setOnClickListener(this);

        mDialImageView = (ImageView) findViewById(R.id.sms_detail_dial);
        mDialImageView.setOnClickListener(this);

        mReplyImageView = (Button) findViewById(R.id.sms_detail_reply);
        mReplyImageView.setOnClickListener(this);
    }

    private static class SmsAdapter extends ArrayAdapter<SmsInfo> {

        private Context mContext;
        private int mResource;

        public SmsAdapter(Context context, int resource) {
            super(context, resource);
            mContext = context;
            mResource = resource;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(mResource, parent, false);

                holder = new ViewHolder();
                holder.sms = (EmojiconTextView) convertView.findViewById(R.id.sms);
                holder.sms.setUseSystemDefault(false);
                holder.date = (TextView) convertView.findViewById(R.id.date);
                holder.dayLayout = convertView.findViewById(R.id.day_layout);
                holder.day = (TextView) convertView.findViewById(R.id.day);
                holder.smsLayout = (LinearLayout) convertView.findViewById(R.id.sms_layout);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            SmsInfo info = getItem(position);
            long time = info.getDate();

            if (position > 0) {
                SmsInfo last = getItem(position - 1);
                if (Utils.less1Min(time, last.getDate())) {
                    holder.dayLayout.setVisibility(View.GONE);
                } else {
                    holder.dayLayout.setVisibility(View.VISIBLE);
                }
            } else {
                holder.dayLayout.setVisibility(View.VISIBLE);
            }

            if (info.getType() == 1) {
                holder.smsLayout.setGravity(Gravity.NO_GRAVITY);
                holder.sms.setBackgroundResource(R.drawable.bg_sms_received);
            } else {
                holder.smsLayout.setGravity(Gravity.RIGHT);
                holder.sms.setBackgroundResource(R.drawable.bg_sms_send);
            }
            holder.sms.setText(info.getBody());

            Date date = new Date(time);
            SimpleDateFormat format = new SimpleDateFormat(" HH:mm", Locale.getDefault());
            holder.date.setText(format.format(date));

            if (Utils.isToday(time)) {
                holder.day.setText(R.string.today);
            } else if (Utils.isYesterday(time)) {
                holder.day.setText(R.string.yesterday);
            } else {
                format = new SimpleDateFormat("MM-dd", Locale.ENGLISH);
                holder.day.setText(format.format(date));
            }

            return convertView;
        }

        private static class ViewHolder {
            LinearLayout smsLayout;
            View dayLayout;
            EmojiconTextView sms;
            TextView date;
            TextView day;
        }
    }

    /**
     * 发送短信
     * 
     * @param info
     */
    private void sendSms(SmsInfo info) {
        mBinder.sendSms(info);
        mAdapter.add(info);
        mAdapter.notifyDataSetChanged();
        mListView.setSelection(mAdapter.getCount() - 1);
    }

    @Override
    public void onSmsReceived(SmsInfo info) {
        IwdsLog.i(this, "onSmsReceived:" + info);
        if (info.getAddress().equals(mAddress)) {
            for (int i = 0; i < mAdapter.getCount(); i++) {
                SmsInfo smsInfo = mAdapter.getItem(i);
                if (smsInfo.getBody().equals(info.getBody())&&smsInfo.getType() == 0) {
                    mAdapter.getItem(i).setType(info.getType());
                    break;
                } else {
                    if (i == mAdapter.getCount() - 1) {
                        IwdsLog.i(this, "mAdapter.getCount():" + mAdapter.getCount());
                        mAdapter.add(info);
                        break;
                    }
                }
            }
            mAdapter.notifyDataSetChanged();
            mDao.readSms(mAddress);

            mListView.setSelection(mAdapter.getCount() - 1);
        } else {
            IwdsLog.w(this, "The message is not this address send.Ignore...");
        }
    }

    @Override
    public void onChannelAvailable(boolean isConnected) {
    }

    @Override
    public void sendOk(SmsInfo info) {
    }

    @Override
    public void sendFailure(SmsInfo info) {
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        mBinder = (SmsBinder) service;
        mBinder.registerChannelAvailableListener(this);
        mBinder.registerSendSmsResultListener(this);
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        if (mBinder != null) {
            mBinder.unregisterChannelAvailableListener(this);
            mBinder.unregisterSendSmsResultListener(this);
            mBinder = null;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.sms_detail_back:
            mBackTextView.setEnabled(false);
            onBackPressed();
            break;
        case R.id.sms_detail_dial:
            mDialImageView.setEnabled(false);
            if (Utils.isAvailable(this)) {
                Intent it = new Intent(SmsDetailActivity.this, TelephoneActivity.class);
                it.setAction(Utils.ACTION_OUT_CALL);
                it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                it.putExtra(Utils.TELEPHONE_FRAGMENT, CallFargment.class.getName());
                it.putExtra(Utils.TELEPHONE_TAG, "call");
                it.putExtra(Utils.TELEPHONE_NUMBER, mAddress);
                startActivity(it);
            } else {
                showBtDisconnectedDialog();
            }
            break;
        case R.id.sms_detail_reply:
            mReplyImageView.setEnabled(false);
            Intent intent = new Intent(SmsDetailActivity.this, SmsReplyActivity.class);
            intent.putExtra(Utils.SMS_NUMBER, mAddress);
            startActivity(intent);
            break;
        default:
            break;
        }
    }

    private AmazingDialog disconnected = null;

    private void showBtDisconnectedDialog() {

        if (disconnected == null) {
            disconnected = new AmazingDialog(this).setRightScrollEnable(false);
            disconnected.setCancelable(false);
            disconnected.setContent(R.string.bt_disconnect);
            disconnected.setPositiveButton(R.drawable.ic_cancel, new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    mDialImageView.setEnabled(true);
                    disconnected.dismiss();
                    disconnected = null;
                }
            });
        }
        disconnected.show();
    }

    @Override
    public void onTimeChange() {
        if(mTimeTextView != null && Utils.isRound())
            mTimeTextView.setText(Utils.getTime());
    }

}