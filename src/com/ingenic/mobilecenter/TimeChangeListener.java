package com.ingenic.mobilecenter;

public interface TimeChangeListener {
    void onTimeChange();
}
